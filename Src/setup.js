import React, { Component } from "react";
import { View } from "react-native";
import RootNavigation from "./Navigation/RootNavigation";
import * as Linking from 'expo-linking';


// LogBox.ignoreLogs([
//   "Animated: `useNativeDriver` was not specified. This is a required option and must be explicitly set to `true` or `false`",
// ]);

class Root extends Component {
  render() {

    const prefix = Linking.createURL('');

    return (
      <>
        <View style={{ flex: 1 }}>
          <RootNavigation />
        </View>
      </>
    );
  }
}
export default Root;
