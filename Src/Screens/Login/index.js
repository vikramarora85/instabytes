import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground,
  Modal,
  StatusBar,
  Linking,
  Platform
} from "react-native";
import Styles from "./Styles";
import Images from "../../Styles/Images";
import GlobalStyles from "../../Styles/globalStyles";
import * as Constants from "../../AllConstants";
import FloatingLabel from "../../Components/FloatingLabel";
import { connect } from "react-redux";
import { startLoginUser } from "../../actions/userAction";
import * as Google from 'expo-google-app-auth';
import * as Facebook from 'expo-facebook';
import Axios from 'axios'
import { Entypo } from '@expo/vector-icons'
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ActivityIndicator } from "react-native";
import colors from "../../Styles/Colors";
import HideWithKeyboard from 'react-native-hide-with-keyboard';
import Toast from "../../Components/Toast";
import { updateExpoToken } from "../../Utils/update_expo_token";
import * as AppleAuthentication from 'expo-apple-authentication';

class Login extends Component {
  constructor(props) {
    super(props);
    this.phoneInput = React.createRef();
    this.state = {
      email: "",
      emailError: "",
      password: "",
      passError: "",
      showgradient: false,
      forgotPassModal: false,
      forgotEmail: "",
      loading: false,
      googleLoading: false,
      fbLogin: false,
      passEyeIconVisible: true,
      fpLoading: false
    };
  }

  // async componentDidMount(){
  //   try {
  //     await AsyncStorage.setItem('getStarted', 'done')
  //   } catch (e) {
  //     console.log(e)
  //   }
  // }

  getStartedPress = () => {
    this.setState({ showgradient: !this.state.showgradient });
    this.setTime();
  };

  setTime = () => {
    this.setState({ loading: false })
    setTimeout(() => {
      this.props.navigation.navigate("LearningCategories");
    }, 10);
  };

  handleSubmit = () => {
    let reg = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
    let isValid = reg.test(this.state.email);
    let regPass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/;
    let isValidPass = regPass.test(this.state.password);

    if (!isValid) {
      this.setState({ emailError: "Not a valid email" });
    } else if (!isValidPass) {
      this.setState({ passError: "Enter Valid Password" });
    } else {
      this.setState({ emailError: "", passError: "", loading: true });
      const formData = {
        email: this.state.email,
        password: this.state.password,
      };
      const redirect = () => {
        return this.getStartedPress();
      };

      const onError = () => {
        return this.setState({ loading: false })
      }

      this.props.dispatch(startLoginUser(formData, redirect, onError));
    }
  };

  handleGoogleLogin = () => {
    this.setState({ googleLoading: true })
    const config = {
      iosClientId: `678626351526-q6k64i8ouhq93r3ee0583jm6hnk0k7r9.apps.googleusercontent.com`,
      androidStandaloneAppClientId: '637174475769-mlcgb5uli2s8liosk6teb7pqv98jqf1a.apps.googleusercontent.com',
      androidClientId: `678626351526-f4k7e87nj2a13hi63fr3n27qvngpijdv.apps.googleusercontent.com`,
      scopes: ['profile', 'email']
    }
    Google.logInAsync(config)
      .then((response) => {
        const { type, user } = response
        console.log(response)
        if (type === 'success') {
          this.update({ access_token: response.accessToken, provider: "google" })
        } else {
          this.setState({ googleLoading: false })
        }
      })
      .catch((err) => {
        console.log(err)
        this.setState({ googleLoading: false })
        Toast('An error occured ,Check your network and try again')
      })

  }


  handleFacebookLogin = async () => {
    this.setState({ fbLogin: true })
    try {
      await Facebook.initializeAsync({
        appId: '345570947302726',
      });
      const val = await Facebook.logInWithReadPermissionsAsync({
        permissions: ['public_profile', 'email'],
      });
      console.log(val)
      if (val.type === 'success') {
        this.update({ access_token: val.token, provider: "facebook" })
      } else {
        this.setState({ fbLogin: false })
        // type === 'cancel'
      }
    } catch ({ message }) {
      console.log(message)
      this.setState({ fbLogin: false })
      Toast(`Facebook Login Error: ${message}`);
    }
  }

  update = (value) => {
    fetch(`http://instabytes.in/api/auth/social/${value.provider}`, {
      method: "POST",
      headers: {
        "accept": "application/json",
        "content-type": "application/json"
      },
      body: JSON.stringify({
        access_token: value.access_token
      })
    }).then((res) => res.json())
      .then((json) => {
        this.setState({ fbLogin: false, googleLoading: false })
        if (json.status == "1") {
          this.storeUser(json.data)
        } else {
          Toast(json.message)
        }
      }).catch((err) => {
        console.log(err)
        this.setState({ fbLogin: false, googleLoading: false })
      })
  }

  storeUser = async (val) => {
    const token = JSON.stringify(val.api_token)
    const data = JSON.stringify(val)
    try {
      updateExpoToken(val.api_token)
      await AsyncStorage.setItem('token', token)
      await AsyncStorage.setItem('user', data)
      this.setTime()
    } catch (err) {
      Toast("An error occured please try again")
      console.log(err)
    }
  }

  handleForgot = async () => {
    let reg = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(this.state.forgotEmail) === false) {
      Toast('Enter a valid email');
      return;
    }
    this.setState({ fpLoading: true })
    const res = await Axios.post('http://instabytes.in/api/auth/forgot', {
      email: this.state.forgotEmail
    });
    this.setState({ fpLoading: false })
    if (res.data.status == "1") {
      this.setState({ forgotPassModal: false })
      this.props.navigation.navigate('Forgot', { data: res.data })
      return;
    }
    Toast(res.data.message);
    console.log(res.data)
  }

  render() {
    const { email, password, showgradient, forgotEmail, loading, googleLoading, fbLogin, passEyeIconVisible, fpLoading } = this.state;
    return (
      <>
        <StatusBar backgroundColor={colors.main} />
        <ImageBackground
          source={Images.loginFrame}
          style={Styles.mainWrapperBackground}
        >
          <View style={Styles.mainWrapper}>
            <Image source={Images.Logo} style={Styles.logoStyle} />
            <Text style={[GlobalStyles.boldText, Styles.WelcomeText]}>
              {Constants.WELCOME_BACK}
            </Text>
            <Text style={[GlobalStyles.lightText, Styles.wlcomeDescription]}>
              {Constants.LOGIN_DESCRIPTION}
            </Text>
          </View>
          <View style={Styles.contentWrapper}>
            <ScrollView>
              <View style={Styles.inputMainWrapper}>
                <FloatingLabel
                  value={email}
                  label={"Email"}
                  autoCapitalize="none"
                  onChangeText={(value) => {
                    this.setState({ email: value });
                  }}
                />
                <Text style={{ color: "red" }}>{this.state.emailError}</Text>

                <FloatingLabel
                  value={password}
                  label={"Password"}
                  eyeIcon={true}
                  onEyePress={() => this.setState({ passEyeIconVisible: !passEyeIconVisible })}
                  secureTextEntry={passEyeIconVisible}
                  autoCapitalize="none"
                  onChangeText={(value) => {
                    this.setState({ password: value });
                  }}
                />

                <Text style={{ color: "red" }}>{this.state.passError}</Text>
                <Text onPress={() => this.setState({ forgotPassModal: true })} style={{ alignSelf: 'flex-end', marginRight: -10 }}>Forgot Password?</Text>

                {showgradient ? (
                  <TouchableOpacity
                  // onPress={() => { this.props.navigation.navigate("LearningCategories") }}
                  >
                    <Text style={[GlobalStyles.boldText, Styles.buttonText]}>
                      {Constants.LOGIN}
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity
                    disabled={loading}
                    style={Styles.buttonContainer}
                    onPress={this.handleSubmit}
                  >
                    {loading ? <ActivityIndicator color='white' size='small' /> : <Text style={[GlobalStyles.boldText, Styles.buttonText]}>
                      {Constants.LOGIN}
                    </Text>}
                  </TouchableOpacity>
                )}

                <View style={Styles.seperatorContainer}>
                  <View style={Styles.seperatorWrapper} />
                  <Text style={[GlobalStyles.lightText, Styles.textContainer]}>
                    {Constants.OR}
                  </Text>
                  <View style={Styles.textwrapper} />
                </View>

                <Text style={[GlobalStyles.lightText, Styles.loginText1]}>
                  {"Login with"}
                </Text>

                <View style={Styles.socialContainer}>
                  {/* Google */}
                  <TouchableOpacity disabled={googleLoading} style={Styles.googleButton} onPress={this.handleGoogleLogin}>
                    {googleLoading ? <ActivityIndicator color={colors.main} size='small' /> : <View style={Styles.innerContainer}>
                      <Image
                        source={Images.Google}
                        style={Styles.socialImage}
                      />
                      <Text style={[GlobalStyles.boldText, Styles.socialText1]}>
                        {Constants.GOOGLE}
                      </Text>
                    </View>}
                  </TouchableOpacity>
                  {/* FaceBook */}
                  <TouchableOpacity disabled={fbLogin} style={Styles.facebookButton} onPress={this.handleFacebookLogin}>
                    {fbLogin ? <ActivityIndicator color='white' size='small' /> : <View style={Styles.innerContainer}>
                      <Image
                        source={Images.Facebook}
                        style={Styles.socialImage}
                      />
                      <Text style={[GlobalStyles.boldText, Styles.socialText]}>
                        {Constants.FACEBOOK}
                      </Text>
                    </View>}
                  </TouchableOpacity>
                </View>

                {Platform.OS == 'ios' && <AppleAuthentication.AppleAuthenticationButton
                  buttonType={AppleAuthentication.AppleAuthenticationButtonType.SIGN_IN}
                  buttonStyle={AppleAuthentication.AppleAuthenticationButtonStyle.BLACK}
                  cornerRadius={5}
                  style={{ height: 44, borderRadius: 12, marginTop: 20 }}
                  onPress={async () => {
                    try {
                      const credential = await AppleAuthentication.signInAsync({
                        requestedScopes: [
                          AppleAuthentication.AppleAuthenticationScope.FULL_NAME,
                          AppleAuthentication.AppleAuthenticationScope.EMAIL,
                        ],
                      });
                      console.log(credential);
                      this.update({ provider: 'apple', access_token: credential.identityToken })
                      // signed in
                    } catch (e) {
                      console.log(e)
                      if (e.code === 'ERR_CANCELED') {
                        // handle that the user canceled the sign-in flow
                      } else {
                        // handle other errors
                      }
                    }
                  }}
                />}

                <View style={Styles.bottomWrapper}>
                  <Text
                    style={[GlobalStyles.lightText, Styles.alreadyAccountText]}
                  >
                    {Constants.DONT_ACCOUNT}
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("Signup");
                    }}
                  >
                    <Text
                      style={[GlobalStyles.lightText, Styles.loginTextBottom]}
                    >
                      {Constants.SIGNUP}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </View>
        </ImageBackground>
        <Modal animationType='slide' transparent style={{ backgroundColor: 'black', opacity: 0.3 }} onRequestClose={() => this.setState({ forgotPassModal: false })} visible={this.state.forgotPassModal}>
          <View style={{ width: '100%', height: '100%', backgroundColor: 'rgba(0, 0, 0, 0.5)' }}>
            <View style={{ position: "absolute", bottom: 0, left: 0, right: 0, height: '40%', backgroundColor: "white", borderTopLeftRadius: 20, borderTopRightRadius: 20, zIndex: 2, justifyContent: "center", alignItems: "center" }}>
              <Entypo onPress={() => this.setState({ forgotPassModal: false })} style={{ position: "absolute", top: 10, right: 10 }} name='cross' color='black' size={25} />
              <HideWithKeyboard style={{ position: "absolute", top: 30, }}>
                <Text style={{ textAlign: "center", fontSize: 22, fontFamily: 'DMSans_700Bold' }}>Forgot Password</Text>
              </HideWithKeyboard>
              <FloatingLabel
                value={forgotEmail}
                style={{ paddingTop: 5 }}
                label={"Email"}
                autoCapitalize="none"
                onChangeText={(value) => {
                  this.setState({ forgotEmail: value });
                }}
              />
              <TouchableOpacity
                style={Styles.buttonContainer}
                onPress={this.handleForgot}
                disabled={fpLoading}
              >
                {fpLoading ? <ActivityIndicator color='white' size='small' /> : <Text style={[GlobalStyles.boldText, Styles.buttonText]}>
                  {'Submit'}
                </Text>}
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </>
    );
  }
}
export default connect()(Login);
