import React, { Component, createRef } from "react";
import { View, Image, TouchableOpacity, Text ,ActivityIndicator, SafeAreaView} from "react-native";
import Styles from "./Styles";
import BackHeader from "../../Components/BackHeader";
import Colors from "../../Styles/Colors";

import { Video } from "expo-av";
import Images from "../../Styles/Images";
import Swiper from "react-native-swiper/src";
import { connect } from "react-redux";
import { startGetSlide ,startAddFavSlider,startDeleteFavSlider} from "../../actions/userAction";
import { IMAGE_BASE_URL } from "../../AllConstants";
import GlobalStyles from "../../Styles/globalStyles";
import AsyncStorage from "@react-native-async-storage/async-storage";

class ResourceSliderFvrts extends Component {
  constructor(props) {
    super(props);
    this.video = createRef(null);
    this.state = {
      selectedIndex:null,
      favorite:null,

      status: null,
      loader:true
    };
  }

  async componentDidMount() {
    await this.props.dispatch(startGetSlide());
    console.log(this.props.navigation.state.params.subcategory_id);
    this.setState({loader:false})
  }

  handleIndexChange = async (index, value) => {
    const userId = await AsyncStorage.getItem("userId");
    const categoryId1 = await AsyncStorage.getItem("categoryId");
    const categoryId = JSON.parse(categoryId1);
    const subcategoryId=this.props.navigation.state.params.subcategory_id
    const formData={
      postId:value.id,
      user_id:userId, 
      subcategory_id:subcategoryId, 
      category_id:categoryId
    }
    const postId={
      category_id:categoryId, 
      subcategory_id:subcategoryId, 
      post_id:value.id
    }
   
    let allData = this.props.user.resourceSlider;
    allData[index].favorite = !allData[index].favorite;

    if (value.favorite) {
      this.setState({ favorite: true });
      this.props.dispatch(startAddFavSlider(formData));
    } else {
      this.setState({ favorite: false });
      this.props.dispatch(startDeleteFavSlider(postId));
    }

    
  };

  handlePageChange = (page) => {
    this.setState({
      selectedIndex: page,
    });
  };

  renderPagination = (index, total) => {
    return (
      <View style={Styles.paginationStyle}>
        <Text style={{ color: "grey" }}>
          <Text style={Styles.paginationText}>{index + 1}</Text>/{total}
        </Text>
      </View>
    );
  };

  render() {
    const { resourceSlider } = this.props.user;
    return (
      <>
        <View style={Styles.safeViewStyle}>
          <BackHeader
            navigation={this.props.navigation}
            headerName={this.props?.navigation?.getParam('data').subcategory_name}
            screen={"ResourceSubCategory"}
            nextShow={true}
          />
          {
            this.state.loader ? <ActivityIndicator  color={Colors.appHeaderColor} style={Styles.activityIndicator}/>:
          
          <View style={Styles.container}>
            <Swiper
              autoPlay={false}
              showsButtons={false}
              dotColor={"#00000000"}
              activeDotColor={"#00000000"}
              pagination={this.renderPagination}
              onIndexChanged={(index) => {
                this.handlePageChange(index);
              }}
            >
              {resourceSlider?.length > 0 &&
                resourceSlider.map((value, index) => {
                  const images = ["jpg", "gif", "png"];
                  const videos = ["mp4", "3gp", "ogg"];
                  const extension = value.file_type;
                  if (images.includes(extension)) {
                    return (
                      <View style={Styles.container}>
                        <Image
                          resizeMode="cover"
                          style={Styles.cover}
                          source={{ uri: IMAGE_BASE_URL + value.slide }}
                        />
                        <TouchableOpacity
                          onPress={() => {
                            this.handleIndexChange(index, value);
                          }}
                          style={Styles.close}
                        >
                          <Image
                            source={Images.heart}
                            style={{
                              width: 23,
                              height: 20,
                              tintColor: value.favorite ? "#F33636" : "#BDBDBD",
                            }}
                          />
                        </TouchableOpacity>
                      </View>
                    );
                  } else if (videos.includes(extension)) {
                    return (
                      <View style={Styles.container}>
                        {/* <Video
                          ref={this.video}
                          style={[
                            Styles.video,
                            { position: "absolute", top: 0 },
                          ]}
                          resizeMode={"contain"}
                          source={{ uri: IMAGE_BASE_URL + value.slide }}
                          useNativeControls
                          isLooping
                          onPlaybackStatusUpdate={(status) => {
                            this.setState({ status: status });
                          }}
                        /> */}
                        <TouchableOpacity
                          onPress={() => {
                            this.handleIndexChange(index, value);
                          }}
                          style={Styles.close}
                        >
                          <Image
                            source={Images.heart}
                            style={{
                              width: 23,
                              height: 20,
                              tintColor: value.favourite?.length>0 ? "#F33636" : "#BDBDBD",
                            }}
                          />
                        </TouchableOpacity>
                      </View>
                    );
                  }
                })}
            </Swiper>
            {resourceSlider?.length > 0 && (
              <View style={[GlobalStyles.mediumText, Styles.paginationStyle]}>
                <Text style={[GlobalStyles.mediumText, Styles.paginationText]}>
                  {this.state.selectedIndex + 1}/{resourceSlider?.length}
                </Text>
              </View>
            )}
          </View>
  }
        </View>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps)(ResourceSliderFvrts);
