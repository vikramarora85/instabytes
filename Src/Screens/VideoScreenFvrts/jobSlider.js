import React, { Component, createRef } from "react";
import { View, Image,Text, TouchableOpacity,ActivityIndicator } from "react-native";
import Styles from "./Styles";
import BackHeader from "../../Components/BackHeader";
import Images from "../../Styles/Images";
import { Video } from "expo-av";
import Colors from "../../Styles/Colors";
import Swiper from "react-native-swiper/src";
import Carousel from "react-native-banner-carousel";
import { connect } from "react-redux";
import { startGetSlide,startAddFavJobSlider,startDeleteFavJobSlider } from "../../actions/userAction";
import { IMAGE_BASE_URL } from "../../AllConstants";
import GlobalStyles from "../../Styles/globalStyles";
import AsyncStorage from "@react-native-async-storage/async-storage";


class JobSliderFvrts extends Component {
  constructor(props) {
    super(props);
    this.video = createRef(null);
    this.state = {
      selectedIndex: null,
      favorite:null,
      status: null,
      loader:true
    };
  }

  async componentDidMount() {
    await this.props.dispatch(startGetSlide());
    this.setState({loader:false})
  }

  handleIndexChange = async (index,value) => {
    const userId = await AsyncStorage.getItem("userId");
    const categoryId1 = await AsyncStorage.getItem("categoryId");
    const categoryId = JSON.parse(categoryId1);
    const jobsubcategoryId=this.props.navigation.state.params.job_subcategory_id
    const formData={
      job_id:categoryId,
      customer_id:userId, 
      job_subcategory_id:jobsubcategoryId, 
      postId:value.id,
    }
    const postId={
      job_id:categoryId, 
      job_subcategory_id:jobsubcategoryId, 
      post_id:value.id
    }
    let allData = this.props.user.jobSlider;
    allData[index].favorite = !allData[index].favorite;

    if (value.favorite) {
      this.setState({ favorite: true });
      this.props.dispatch(startAddFavJobSlider(formData));
    } else {
      this.setState({ favorite: false });
      this.props.dispatch(startDeleteFavJobSlider(postId));
    }
  
  };
  handlePageChange = (page) => {
    this.setState({
      selectedIndex: page,
    });
  };
  renderPagination = (index, total) => {
    return (
      <View style={Styles.paginationStyle}>
        <Text style={{ color: "grey" }}>
          <Text style={Styles.paginationText}>{index + 1}</Text>/{total}
        </Text>
      </View>
    );
  };
  handlePageChange = (page) => {
    this.setState({
      selectedIndex: page,
    });
  };
  renderPagination = (index, total) => {
    return (
      <View style={Styles.paginationStyle}>
        <Text style={{ color: "grey" }}>
          <Text style={Styles.paginationText}>{index + 1}</Text>/{total}
        </Text>
      </View>
    );
  };

  render() {
   
    const { jobSlider } = this.props.user;
    return (
      <>
        <View style={Styles.safeViewStyle}>
          <BackHeader
            navigation={this.props.navigation}
            headerName={this.props.navigation.getParam('data').job_subname}
            screen={"JobSubCategory"}
            nextShow={true}
          />
          {
            this.state.loader ? <ActivityIndicator color={Colors.appHeaderColor} style={Styles.activityIndicator}/>:
          
          <View style={Styles.container}>
             <Swiper
              autoPlay={false}
              showsButtons={false}
              dotColor={"#00000000"}
              activeDotColor={"#00000000"}
              pagination={this.renderPagination}
              onIndexChanged={(index) => {
                this.handlePageChange(index);
              }}
            >
              {jobSlider.length > 0 &&
                jobSlider.map((value, index) => {
                  console.log(IMAGE_BASE_URL + value.job_slides)
                  const images = ["jpg", "gif", "png", "PNG"];
                  const videos = ["mp4", "3gp", "ogg"];
                  const extension = value.file_type;
                  if (images.includes(extension)) {
                    return (
                      <View style={Styles.container}>
                        <Image
                          resizeMode="cover"
                          style={Styles.cover}
                          source={{ uri: IMAGE_BASE_URL + value.job_slides }}
                        />
                        <TouchableOpacity
                          onPress={() => {
                            this.handleIndexChange(index, value);
                          }}
                          style={Styles.close}
                        >
                          <Image
                            source={Images.heart}
                            style={{
                              width: 23,
                              height: 20,
                              tintColor: value.favorite ? "#F33636" : "#BDBDBD",
                            }}
                          />
                        </TouchableOpacity>
                      </View>
                    );
                  } else if (videos.includes(extension)) {
                    return (
                      <View style={Styles.container}>
                        <Video
                          ref={this.video}
                          style={[
                            Styles.video,
                            { position: "absolute", top: 0 },
                          ]}
                          resizeMode={"contain"}
                          source={{ uri: IMAGE_BASE_URL + value.job_slides }}
                          useNativeControls
                          isLooping
                          onPlaybackStatusUpdate={(status) => {
                            this.setState({ status: status });
                          }}
                        />
                        <TouchableOpacity
                          onPress={() => {
                            this.handleIndexChange(index, value);
                          }}
                          style={Styles.close}
                        >
                          <Image
                            source={Images.heart}
                            style={{
                              width: 23,
                              height: 20,
                              tintColor: value.favourite?.length>0 ? "#F33636" : "#BDBDBD",
                            }}
                          />
                        </TouchableOpacity>
                      </View>
                    );
                  }
                })}
            </Swiper>
            {jobSlider.length > 0 && (
              <View style={[GlobalStyles.mediumText, Styles.paginationStyle]}>
                <Text style={[GlobalStyles.mediumText, Styles.paginationText]}>
                  {this.state.selectedIndex + 1}/{jobSlider.length}
                </Text>
              </View>
            )}
          </View>
          }
        </View>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps)(JobSliderFvrts);
