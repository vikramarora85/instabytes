import { StyleSheet, Dimensions, Platform } from "react-native";
import Colors from "../../Styles/Colors";

const widthScreen = Dimensions.get("window").width;
const heightScreen = Dimensions.get("window").height;

const styles = StyleSheet.create({
  safeViewStyle1: {
    backgroundColor: Colors.appHeaderColor,
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
 },
  mainContainer: {
    marginBottom: 20,
  },
  safeViewStyle: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
  },
  /*container: {
    width: widthScreen,
  },*/
  video: {
    alignSelf: "center",
    width: widthScreen,
    height: heightScreen / 1.2,
  },
  buttons: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },

  /* Style for Header */
  headerWrapper: {
    width: widthScreen,
    height: Platform.OS === "ios" ? 110 : 90,
    alignItems: "center",
  },
  mainWrapper: {
    flexDirection: "row",
    width: "90%",
    alignSelf: "center",
    marginTop: Platform.OS === "ios" ? 60 : 40,
    justifyContent: "space-between",
  },
  mainWrapper1: {
    flexDirection: "row",
    width: "90%",
    alignSelf: "center",
    marginTop: Platform.OS === "ios" ? 60 : 40,
  },
  innerWrapper: {
    flexDirection: "row",
    alignItems: "center",
  },
  menuStyle: {
    marginTop: Platform.OS === "ios" ? 0 : 3,
    width: 25,
    height: 25,
    tintColor: Colors.White,
  },
  headerTitle: {
    fontSize: 20,
    color: Colors.White,
    marginLeft: 15,
    marginTop: 3,
  },
  emailWrapper: {
    marginTop: -7,
    flexDirection: "row",
    borderWidth: 1,
    borderColor: Colors.White,
    borderRadius: 50,
    backgroundColor: Colors.White,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    width: "80%",
    marginLeft: 15,
  },
  emailInput: {
    marginLeft: 20,
    flex: 1,
    fontSize: 17,
    // margin: 10
  },
  inputImageLock: {
    width: 22,
    height: 25,
    marginRight: 20,
  },
  container: {
    width: widthScreen,
    height: heightScreen / 1.2,
  },
  cover: {
    flex: 1,
  },
  close: {
    margin: 5,
    position: "absolute",
    top: 10,
    right: 10,
    width: 40,
    height: 40,
    borderRadius: 40,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
  },
  paginationStyle: {
    position: "absolute",
    bottom: -30,
    alignSelf: "center",
    backgroundColor: Colors.appHeaderColor,
    borderRadius: 50,
    paddingHorizontal: 10,
  },
  paginationText: {
    color: Colors.White,
    fontSize: 18,
    letterSpacing: 2,
  },
});
export default styles;
