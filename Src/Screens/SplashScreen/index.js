import React, { Component } from 'react';
import {
  Image,
  View
} from 'react-native';
import Styles from './Styles';
import Images from '../../Styles/Images';
import colors from '../../Styles/Colors';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Linking from 'expo-linking';

class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.moved = false;
  }
  async componentDidMount() {

    //this.props.navigation.navigate('resourceSlide', {id:25, slide:5})

    setTimeout(async () => {
      if (!this.moved) {
        const is_linking = await AsyncStorage.getItem('is_linking')
        if (!is_linking) {
          const val = await AsyncStorage.getItem('getStarted');
          if (val) {
            const Token = await AsyncStorage.getItem("token");
            if (Token !== null) {
              this.props.navigation.navigate("LearningCategories");
            } else {
              this.props.navigation.navigate("Login");
            }
          } else {
            this.props.navigation.navigate("GetStarted");
          }
        } else {
          this.set_linking()
        }
      }
    }, 3000);
  }

  set_linking = async () => {
    await AsyncStorage.removeItem("is_linking")
  }

  render() {

    return (
      <View style={{ height: "100%", width: "100%", justifyContent: 'center', alignItems: "center", backgroundColor: colors.main }}>
        <Image source={require('../../../assets/Icons/iconSplash.png')} style={{ height: 166, width: 166 }} />
      </View>
    );
  }
}
export default SplashScreen;