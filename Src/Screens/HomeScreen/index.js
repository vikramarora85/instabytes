import React, { Component } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground,
  FlatList

} from 'react-native';
import Styles from './Styles';
import Images from '../../Styles/Images';
import HeaderHome from '../../Components/HomeHeaderList';
import GlobalStyles from '../../Styles/globalStyles'

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      premiumData: [
        {
          name: 'Accounting'
        },
        {
          name: 'Business'
        },
        {
          name: 'Business'
        },
        {
          name: 'Business'
        },
      ],
    };
  }
  // LearningCategories
  render() {
    const { premiumData, freeData, selectedIndex } = this.state
    return (
      <>
        <View style={Styles.safeViewStyle}>
          <HeaderHome
            navigation={this.props.navigation}
            headerName={'Hello!'}
            headerName1={'Prasanna'}
            isSearch={true}
          />
          <ScrollView showsHorizontalScrollIndicator={false}>
            <View style={Styles.headerContentWrapper}>

              <Text style={[GlobalStyles.boldText, Styles.headerText]}>{'Welcome to Instabyte!'}</Text>
              <Text style={[GlobalStyles.lightText, Styles.headerTextDescription]}>{'choose the topics you are intrested in'}</Text>

              <View style={Styles.headerContentWrapper1}>
                {
                  premiumData.length > 0 && premiumData.map((value) => {
                    return (
                      <>
                        <ImageBackground
                          source={Images.premiumThumbnail}
                          imageStyle={{ borderRadius: 15 }}
                          style={Styles.headerPremiumContentInner}>
                          <View style={Styles.innerPremium}>
                            <Text style={[GlobalStyles.boldText, Styles.textCategory]}>{value.name}</Text>
                            <TouchableOpacity style={Styles.buttonContainer} onPress={() => {this.props.navigation.navigate('LearningCategories')}}>
                              <Text style={[GlobalStyles.mediumText, Styles.buttonText]}>{'Select'}</Text>
                            </TouchableOpacity>
                          </View>
                        </ImageBackground>
                      </>
                    )
                  })
                }
              </View>
            </View>
          </ScrollView>
        </View>
      </>
    );
  }
}
export default HomeScreen;
