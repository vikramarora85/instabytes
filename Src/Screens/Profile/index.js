import React, { Component } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Image,
  Platform
} from 'react-native';
import Styles from './Styles'
import Images from '../../Styles/Images';
import Colors from '../../Styles/Colors';
import * as Constants from '../../AllConstants';
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from 'axios';
import * as ImagePicker from 'expo-image-picker';
import { ImageBackground } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons'
import Toast from '../../Components/Toast';
import { Picker } from '@react-native-picker/picker';
import * as Updates from 'expo-updates';
import { connect } from 'react-redux'
import { setProfile } from '../../actions/userAction';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showgradient: false,
      name: '',
      email: '',
      phone: '',
      gender: '',
      country: '',
      professional_domain: '',
      profile_pic: ''
    };
  }

  async componentDidMount() {
    const user = await AsyncStorage.getItem('user')
    this.setState({ ...JSON.parse(user) })

  }

  changeImage = async () => {
    if (Platform.OS !== 'web') {
      const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      } else {
        this.pickImage()
      }
    }
  }

  pickImage = async () => {

    let res = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: Platform.OS == 'android' ? true : false,
      aspect: [4, 3],
      quality: 1,
    });

    if (!res.cancelled) {
      const token = await AsyncStorage.getItem("token")
      let formData = new FormData();
      //const path = await this.normalizePath(res.uri);
      formData.append("profile_pic", {
        uri: res.uri,
        type: "image/JPEG",
        name: 'profile_image.jpg',
      })
      fetch('http://instabytes.in/api/auth/update_profile_pic', {
        method: "POST",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': `Bearer ${JSON.parse(token)}`
        },
        body: formData
      }).then((res) => res.json())
        .then((json) => {
          if (json.status == "1") {
            this.fetchProfile()
          } else {
            console.log(json)
          }
        }).catch((err) => console.log(err))
    }
  };


  getStartedPress = async () => {
    this.setState({ showgradient: !this.state.showgradient })

    const val = this.state;

    delete val.profile_pic

    const res = await axios.post('http://instabytes.in/api/auth/update_profile', {
      ...val
    });

    if (res.data.status == "1") {
      try {
        this.props.setProfile(res.data.data)
        await AsyncStorage.setItem('user', JSON.stringify(res.data.data))
        this.props.navigation.navigate('LearningCategories')
      }
      catch (e) {
        console.log(e)
      }
    } else {
      Toast(res.data.message)
    }
    console.log(res.data)
  }

  fetchProfile = async () => {
    const token = await AsyncStorage.getItem("token")
    fetch('http://instabytes.in/api/profile', {
      method: "GET",
      headers: {
        "accept": "application/json",
        "content-type": "application/json",
        "Authorization": `Bearer ${JSON.parse(token)}`
      }
    }).then((res) => res.json())
      .then((json) => {
        if (json.status == "1") {
          this.storeData(json.data)
        } else {
          console.log(json)
        }
      }).catch((err) => {
        console.log(err)
      })

  }

  storeData = async (val) => {

    this.props.setProfile(val)
    const data = JSON.stringify(val)

    try {
      await AsyncStorage.setItem('user', data)
      this.refresh()
    } catch (err) {
      console.log(err)
    }
  }

  refresh = async () => {
    const user = await AsyncStorage.getItem('user')
    this.setState({ ...JSON.parse(user) })
  }

  setTime = () => {
    setTimeout(() => {
      this.props.navigation.navigate('LearningCategories')
    }, 50);
  }

  render() {
    const { showgradient, name, email, phone, gender, country, professional_domain, profile_pic } = this.state
    return (
      <>
        <SafeAreaView style={Styles.safeViewStyle}>
          <View style={Styles.mainWrapper}>
            <View style={Styles.headerWrapper}>

              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('ViewProfile');
                }}
              >
                <Image source={Images.back} style={Styles.menuStyle} />
              </TouchableOpacity>

              <Text style={Styles.headerTitle}>{'Profile'}</Text>
              <Text style={[Styles.headerTitle, { color: Colors.White }]}>{'Edit'}</Text>
            </View>

            <TouchableOpacity onPress={this.changeImage} style={{ flexDirection: 'row', justifyContent: 'center', width: 130, height: 130, borderRadius: 130, alignSelf: "center", marginTop: 20 }}>
              <View style={{ position: "absolute", top: 0, right: 0, left: 0, bottom: 0, borderRadius: 130, justifyContent: "center", alignItems: "center", zIndex: 1, backgroundColor: "rgba(0, 0, 0, 0.4)" }}>
                <Ionicons onPress={this.changeImage} name='camera-outline' color='white' size={40} />
              </View>
              <Image source={profile_pic?.length != 0 ? { uri: profile_pic } : Images.Avatar} style={{
                width: 130,
                height: 130,
                borderRadius: 130,
                alignSelf: 'center',
              }} />
            </TouchableOpacity>

          </View>
          <ScrollView>
            <View style={Styles.headerContainer}>
              {/* Profile */}
              {/* {
                profileFocus &&
                <Text style={Styles.textInputHeading}>{'Profile'}</Text>
              } */}
              <View style={Styles.emailWrapper}>
                <TextInput
                  style={Styles.emailInput}
                  value={name}
                  placeholder={'Name'}
                  // onFocus={this.focusName}
                  // onBlur={this.focusName}
                  placeholderTextColor={Colors.textInputColor}
                  autoCapitalize='none'
                  onChangeText={(value) => {
                    this.setState({ name: value })
                  }}
                />
              </View>

              <View style={Styles.emailWrapper}>
                <TextInput
                  style={Styles.emailInput}
                  value={email}
                  placeholder={'Email'}
                  // onFocus={this.focusName}
                  // onBlur={this.focusName}
                  placeholderTextColor={Colors.textInputColor}
                  autoCapitalize='none'
                  onChangeText={(value) => {
                    this.setState({ email: value })
                  }}
                />
              </View>

              <View style={Styles.emailWrapper}>
                <TextInput
                  style={Styles.emailInput}
                  value={phone}
                  maxLength={10}
                  keyboardType={'phone-pad'}
                  placeholder={'Phone'}
                  placeholderTextColor={Colors.textInputColor}
                  autoCapitalize='none'
                  onChangeText={(value) => {
                    this.setState({ phone: value })
                  }}
                />

              </View>
              {Platform.OS == 'android' ?
                <View style={{ ...Styles.emailWrapper, justifyContent: 'center', alignItems: 'center' }}>
                  <Picker
                    style={{ width: "100%", height: "96%" }}
                    selectedValue={gender}
                    mode='dropdown'
                    onValueChange={(itemValue, itemIndex) =>
                      this.setState({ gender: itemValue })
                    }>
                    <Picker.Item label="Male" value="Male" />
                    <Picker.Item label="Female" value="Female" />
                  </Picker>
                </View> :
                <View style={{ ...Styles.emailWrapper }}>
                  <TextInput
                    style={Styles.emailInput}
                    value={gender}
                    placeholder={'Gender'}
                    placeholderTextColor={Colors.textInputColor}
                    autoCapitalize='none'
                    onChangeText={(value) => {
                      this.setState({ gender: value })
                    }}
                  />
                </View>}
              <View style={Styles.emailWrapper}>
                <TextInput
                  style={Styles.emailInput}
                  value={country}
                  placeholder={'Country code'}
                  // onFocus={this.focusName}
                  // onBlur={this.focusName}
                  placeholderTextColor={Colors.textInputColor}
                  autoCapitalize='none'
                  onChangeText={(value) => {
                    this.setState({ country: value })
                  }}
                />
              </View>
              <View style={Styles.emailWrapper}>
                <TextInput
                  style={Styles.emailInput}
                  value={professional_domain}
                  placeholder={'Professional Domain'}
                  // onFocus={this.focusName}
                  // onBlur={this.focusName}
                  placeholderTextColor={Colors.textInputColor}
                  autoCapitalize='none'
                  onChangeText={(value) => {
                    this.setState({ professional_domain: value })
                  }}
                />
              </View>
            </View>
            {
              showgradient ?
                <TouchableOpacity
                // onPress={() => { this.props.navigation.navigate("LearningCategories") }}
                >
                  <View
                    style={Styles.buttonContainerGradient}
                  >
                    <Text style={Styles.buttonText}>{'Save'}</Text>
                  </View>
                </TouchableOpacity>
                :
                <TouchableOpacity style={Styles.buttonContainer}
                  onPress={this.getStartedPress}
                >
                  <Text style={Styles.buttonText}>{'Save'}</Text>
                </TouchableOpacity>
            }
          </ScrollView>



        </SafeAreaView>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user.profile,
  };
};

const mapDispatchToProps = (dispatch) => ({
  setProfile: (data) => dispatch(setProfile(data)),
  dispatch
});


export default connect(mapStateToProps, mapDispatchToProps)(Profile);
