import React, { Component } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground,
  Platform
} from 'react-native';
import Styles from './Styles';
import Images from '../../Styles/Images';
import { startGetPlans } from '../../actions/userAction'
import { connect } from 'react-redux';
import Entypo from '@expo/vector-icons/Entypo'
import colors from '../../Styles/Colors';
import GlobalStyles from '../../Styles/globalStyles';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Header from '../../Components/Header';


class Subscription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      showgradient: false,
      isModalVisible: true,
      isSubscribed: false
    };
  }

  componentDidMount() {
    this.fetchUser()
    this.props.dispatch(startGetPlans())
  }
  getStartedPress = () => {
    this.setState({ showgradient: !this.state.showgradient })
    this.setTime()
  }

  setTime = () => {
    setTimeout(() => {
      this.props.navigate('LearningCategories')
    }, 50);
  }

  fetchUser = async () => {
    const res = await AsyncStorage.getItem("user")
    const data = JSON.parse(res)
    if (data.is_subscribed == "1") {
      this.setState({ isSubscribed: true })
    } else {
      this.setState({ isSubscribed: false })
    }
  }

  valueChange1 = () => {
    this.props.navigation.navigate("SelectPayment", { type: "subscription" })
  }

  render() {
    const { isSubscribed } = this.state
    return (
      <>
        <View style={Styles.safeViewStyle1}>
          <View style={{ height: Platform.OS == 'ios' ? 110 : 90, position: "absolute", top: 0, left: 0, right: 0, zIndex: 1, backgroundColor: colors.main, }}>
            <View style={Styles.mainWrapperHeader}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Images.back} style={Styles.menuStyle} />
              </TouchableOpacity>
              <Text style={[GlobalStyles.boldText, Styles.headerTitle]}>{'Subscription'}</Text>
            </View>
          </View>
          {!isSubscribed ? <View style={Styles.mainWrapper}>
            <View style={Styles.courseDescription}>
              <Text style={Styles.headingText}>
                {'Bytes Subscription'}
              </Text>
              <Text style={Styles.headingdescription}>
                {'Upgrade to Premium Membership and increase your options'}
              </Text>
              <TouchableOpacity style={Styles.buyContainer} onPress={this.valueChange1}>
                <Text style={Styles.buttonText}>{'$99 Upgrade to Premium'}</Text>
              </TouchableOpacity>
            </View>
          </View> :
            <View style={{ width: "100%" }}>
              <Text style={Styles.headingText}>Bytes Subscriber</Text>
              <Text style={{ fontFamily: "DMSans_400Regular", fontSize: 16, marginTop: 10, textAlign: "center" }}>Enjoy your Premium Membership</Text>
              <View style={{ height: 50, width: '76%', alignSelf: "center", marginTop: 30, alignItems: "center", justifyContent: "center", borderRadius: 25, backgroundColor: '#42BE65' }}>
                <Text style={{ fontFamily: "DMSans_700Bold", color: "white", fontSize: 16 }}>Already a member</Text>
              </View>
            </View>}
        </View>
        <TouchableOpacity
          style={Styles.buttonContainer}
          onPress={() => this.props.navigation.goBack(null)}
        >
          <Text style={[GlobalStyles.mediumText, Styles.buttonText]}>
            {"Back to Home"}
          </Text>
        </TouchableOpacity>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user
  };
}
export default connect(mapStateToProps)(Subscription);