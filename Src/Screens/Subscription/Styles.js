import { StyleSheet, Dimensions, Platform } from 'react-native';
import colors from '../../Styles/Colors';
import Colors from '../../Styles/Colors';
const widthScreen = Dimensions.get('window').width;
const styles = StyleSheet.create({

  safeViewStyle1: {
    height:"100%",
    width:"100%",
    backgroundColor: 'white',
    justifyContent:"center",
    alignItems:"center",
  },

  mainWrapperHeader: {
    flexDirection: 'row',
    width: '90%',
    alignSelf: 'center',
    zIndex:2,
    marginTop: Platform.OS == 'ios' ? 60 : 40,
  },

  headerTitle: {
    fontSize: 20,
    fontWeight: '600',
    color: Colors.White,
    marginLeft: 10,
  },

  safeViewStyle: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent:"center",
    alignItems:"center"
  },
  menuStyle: {
    marginTop: Platform.OS == 'ios' ? 0 : 0,
    width: 25,
    height: 25,
    tintColor:"white"
  },
  mainWrapper: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  bottomWrapper: {
    flex: 0.15,
  },
  buttonContainer: {
    width: widthScreen / 1.2,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.appHeaderColor,
    alignSelf: 'center',
    borderRadius: 25,
    position:'absolute',
    bottom:50
  },
  buttonText: {
    color: Colors.White,
    fontSize: 20,
    fontWeight: '600'
  },
  buttonContainerGradient: {
    width: widthScreen / 1.2,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    alignSelf: 'center',
    borderRadius: 25,
    marginBottom: 10,
    backgroundColor:colors.main
  },
  courseDescription: {
    width: widthScreen / 1.1,
    alignSelf: 'center',
    borderColor: Colors.ok,
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor:'white'
  },
  headingText: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 25,
    color: Colors.primary,
    fontFamily:"DMSerifDisplay_400Regular"
  },
  headingdescription: {
    marginTop: 20,
    textAlign: 'center',
    fontWeight: '400',
    fontSize: 18
  },
  headingdescription1: {
    marginTop: 20,
    // textAlign: 'center',
    fontWeight: '400',
    fontSize: 18,
    marginHorizontal: 15,
    textAlign:'center'
  },
  headingPrice: {
    marginTop: 20,
    textAlign: 'center',
    fontWeight: '500',
    fontSize: 22,
    color: Colors.primary
  },
  buyPrice: {
    marginTop: 20,
    textAlign: 'center',
    fontWeight: '400',
    fontSize: 14
  },
  buyContainer: {
    width: widthScreen / 1.2,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#42BE65',
    marginTop: 20,
    alignSelf: 'center',
    borderRadius: 25,
    marginBottom: 40
  },
  cancelButton: {
    width: '40%',
    height: 60,
    borderColor: Colors.appHeaderColor,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.White,
    marginTop: 20,
    alignSelf: 'flex-end',
    borderRadius: 25,
    marginBottom: 20,
    marginRight: 15
  },
  cancelButton1: {
    width: '34%',
    height: 40,
    borderColor: Colors.appHeaderColor,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.White,
    marginTop: 20,
    alignSelf: 'flex-end',
    borderRadius: 25,
    marginBottom: 20,
    marginRight: 15
  },
  cancelButton2: {
    width: '34%',
    height: 40,
    borderColor: Colors.appHeaderColor,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.main,
    marginTop: 20,
    alignSelf: 'flex-end',
    borderRadius: 25,
    marginBottom: 20,
    marginRight: 15
  },
  buttonText1: {
    color: Colors.ok,
    fontSize: 14,
    fontWeight: '700'
  },
  buttonText2: {
    color: 'white',
    fontSize: 14,
    fontWeight: '700'
  },
  modelContainer: {
    width: '100%',
    // height: 200,
    backgroundColor: 'white',
    borderRadius: 20,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.5,
  },
});
export default styles;