import React, { Component, createRef } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground,
  Animated,
  FlatList,
  Dimensions,
  ActivityIndicator
} from 'react-native';
import Images from '../../Styles/Images';
import BackHeader from '../../Components/BackHeader';
import Styles from './Styles';
import GlobalStyles from '../../Styles/globalStyles';
import { startGetFavResuorceSubcategory, startDeleteFavSubcategory, startGetFavSubcategory, setFavResourceSubcategory } from '../../actions/userAction'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { IMAGE_BASE_URL } from '../../AllConstants';
import colors from '../../Styles/Colors';

const height = Dimensions.get("window").height;

class FavResourceSubcategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      fadeAnim: new Animated.Value(1),
      animatedIndex: null,
      loader:true
    }
  }
  async componentDidMount() {
    const catId = this.props.navigation.state.params.catId
    await this.props.dispatch(startGetFavResuorceSubcategory(catId))
    this.setState({
      loader:false
    })
  }

  handleClick = (value) => {
    this.props.navigation.navigate('FavResourceSlide', { data: value })

  }

  fadeOut = () => {
    Animated.timing(this.state.fadeAnim, {
      toValue: 0,
      duration: 1000,
      useNativeDriver: false
    }).start();
  }


  handleIndexChange = (index) => {

    this.setState({ selectedIndex: index })
  }
  getStartedPress = () => {
    this.setTime()
  }
  setTime = () => {
    setTimeout(() => {
      this.props.navigation.navigate('LearningCategories')
    }, 50);
  }

  animate = async (index, value) => {
    this.setState({
      animatedIndex: index
    });
    this.fadeOut();
    this.removeResFavorite(index, value)
  }
  removeResFavorite = async (index, value) => {

    const formdata = {
      category_id: value.category_id,
      subcategory_id: value.subcategory_id
    }

    await this.props.dispatch(startDeleteFavSubcategory(formdata))
    const data = this.props.user.favResSubcat
    setTimeout(() => {
      data.splice(index, 1)
      this.props.dispatch(setFavResourceSubcategory(data))
      this.setState({
        animatedIndex: null
      });
    }, 1000)
  }
  render() {
    const { favResSubcat } = this.props.user

    if (this.state.loader) {
      return (
        <View style={{ height: "100%", width: "100%", alignItems: "center", justifyContent: "center" }}>
          <View style={{width:"100%", position:"absolute", top:0}}>
            <BackHeader
              navigation={this.props.navigation}
              headerName={this.props.navigation?.getParam("data")?.jocategory_name}
              screen={"Favourites"}
              nextShow={false}
            />
          </View>
          <ActivityIndicator color={colors.main} size={'small'} />
        </View>
      )
    }

    return (
      <>
        <View style={Styles.safeViewStyle}>
          <BackHeader
            navigation={this.props.navigation}
            headerName={this.props.navigation?.getParam('data')?.category_name || 'Sub Category'}
            screen={'Favourites'}
            nextShow={false}
          />

          <ScrollView>
            <View style={Styles.headerContainer}>
              {
                favResSubcat.length > 0 ? favResSubcat.map((value, index) => {
                  return (
                    <>
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('FavResourceSlide', { data: value })}>
                        <Animated.View style={{ ...Styles.listWrapper, opacity: this.state.animatedIndex === index ? this.state.fadeAnim : 1 }}>
                          <View style={Styles.innerList}>
                            <ImageBackground
                              source={{ uri: IMAGE_BASE_URL + value.subcategory_image }}
                              imageStyle={{ borderRadius: 12 }}
                              style={Styles.headerContentInner}>
                              <View style={{ height: "100%", width: "100%", alignItems: "center", justifyContent: "center", backgroundColor: "rgba(0, 0, 0, 0.2)", borderRadius:12 }}>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate('FavResourceSlide', { data: value }) }}>
                                  <Image source={Images.playIcon} style={Styles.playIconStyle} />
                                </TouchableOpacity>
                              </View>
                            </ImageBackground>
                            <Text style={[GlobalStyles.mediumText, Styles.titleText]}>
                              {value.subcategory_name}
                            </Text>
                          </View>
                          <TouchableOpacity onPress={() => { this.animate(index, value) }}>
                            <Image source={Images.closeIcon} style={{
                              width: 25, height: 25, marginTop: 15, marginRight: 15,
                            }} />
                          </TouchableOpacity>
                        </Animated.View>
                      </TouchableOpacity>
                    </>
                  )
                }) : <Text style={[GlobalStyles.boldText, { color:colors.main, textAlign:"center", marginTop: height * 0.35}]}>No Data</Text>
              }
            </View>
          </ScrollView>
        </View>
        <TouchableOpacity style={Styles.buttonContainer} onPress={this.getStartedPress}>
          <Text style={[GlobalStyles.boldText, Styles.buttonText]}>{'Back to Home'}</Text>
        </TouchableOpacity>
      </>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user

  }

}
export default connect(mapStateToProps)(FavResourceSubcategory);