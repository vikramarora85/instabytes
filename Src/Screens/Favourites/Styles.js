import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../../Styles/Colors';

const widthScreen = Dimensions.get("window").width;
const heightScreen = Dimensions.get("window").height;

const styles = StyleSheet.create({
  safeViewStyle: {
    height:heightScreen,
    width:widthScreen,
  },
  safeViewStyle1: {
    backgroundColor: Colors.appHeaderColor
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
 },
  segmentMainContainer: {
    // paddingVertical: 30,
    justifyContent: "center",
    alignItems: "center",
  },
  segmentWrapper: {
    justifyContent: "center",
    alignItems: "center",
    width: '100%',
  },
  gratitudeText: {
    marginTop: 15,
    fontSize: 24,
    width: widthScreen / 1.2,
    alignSelf: 'center',
    textAlign: 'center',
    color: Colors.black
  },
  description: {
    marginTop: 10,
    fontSize: 16,
    width: widthScreen / 1.2,
    alignSelf: 'center',
    textAlign: 'center',
    color: "#6C6C6C"
  },
  buttonContainerGradient: {
    width: widthScreen / 1.2,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 25,
    marginBottom: 20
  },
  buttonContainer: {
    width: widthScreen / 1.2,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.appHeaderColor,
    alignSelf: 'center',
    borderRadius: 25,
    position:'absolute',
    bottom:30
  },
  buttonText: {
    color: Colors.White,
    fontSize: 16,
    fontWeight: '600'
  },
  headerContainer: {
    width: widthScreen / 1.2,
    alignSelf: "center",
    marginTop: 20,
    marginBottom: 100,
  },
  listWrapper: {
    width: widthScreen / 1.1,
    alignSelf: 'center',
    backgroundColor: Colors.White,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: Colors.appHeaderColor,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    marginTop: 20,

    elevation: 3,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  innerList: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerContentInner: {
    width: 120,
    height: 120,
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:"transparent"
  },
  playIconStyle: {
    width: 40,
    height: 40
  },
  innerPremium: {
    width: "90%",
    alignSelf: "center",
    marginTop: 20,
  },
  playIconStyle1: {
    alignSelf: "center",
    width: 45,
    height: 45,
  },
  playIconStyle2: {
    alignSelf: "flex-end",
    width: 30,
    height: 30,
  },
  titleText: {
    fontSize: 14,
    color: Colors.ok,
    marginLeft: 10,
  },
  paginationStyle: {
    position: "absolute",
    bottom: 20,
    alignSelf: "center",
    backgroundColor: Colors.appHeaderColor,
    borderRadius: 50,
    paddingHorizontal: 10,
  },
  paginationText: {
    color: Colors.White,
    fontSize: 18,
    letterSpacing: 2,
  },
  close: {
    width: 40,
    height: 40,
    borderRadius: 40,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
  },
  video: {
    alignSelf: "center",
    width: widthScreen,
    height: heightScreen / 1.2,
  },
});
export default styles;
