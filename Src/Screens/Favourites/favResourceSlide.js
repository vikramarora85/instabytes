import React, { Component } from 'react';
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Image,
  FlatList,
  ActivityIndicator
} from 'react-native';
import { Video } from "expo-av";
import MaterialCommunityIcons from '@expo/vector-icons/MaterialCommunityIcons'
import Images from '../../Styles/Images';
import BackHeader from '../../Components/BackHeader';
import Styles from './Styles';
import Colors from '../../Styles/Colors';
import GlobalStyles from '../../Styles/globalStyles';
import { startDeleteFavSlider } from '../../actions/userAction'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import { IMAGE_BASE_URL } from '../../AllConstants';
import colors from '../../Styles/Colors';
import Toast from '../../Components/Toast';

const width = Dimensions.get('window').width;
const images = ["jpg", "gif", "png", "PNG", "jpeg", "JPEG", "JPG", "webp", "WEBP", "webP"];
const videos = ["mp4", "3gp", "ogg"];

class FavResourceSlide extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      data: [],
      loader: false,
      loading: true
    }
    this.videoRefs = [];
  }

  async componentDidMount() {
    //console.log(this.props.navigation?.getParam('data')?.subcategory_id)
    const token = await AsyncStorage.getItem("token");

    fetch(`http://instabytes.in/api/favourite-slidelist/${this.props.navigation?.getParam('data')?.subcategory_id}`, {
      method: "GET",
      headers: {
        accept: 'application/json',
        'content-type': "application/json",
        "Authorization": `Bearer ${JSON.parse(token)}`,
      },
    }).then((res) => res.json())
      .then((json) => {
        console.log(json)
        if (json.status == "1") {
          this.setState({
            data: json.data,
            loading: false
          })
        } else {
          this.setState({
            loading: false
          })
        }
      }).catch((err) => console.log(err))

  }

  handleClick = () => {
    this.props.navigation.navigate('FavJobSlide')
  }


  handleIndexChange = (index) => {
    this.setState({ selectedIndex: index })
  }

  getStartedPress = () => {
    this.setTime()
  }

  setTime = () => {
    setTimeout(() => {
      this.props.navigation.navigate('LearningCategories')
    }, 50);
  }

  removeResourceFavorite = async (index, value) => {
    const formData = {
      category_id: value.category_id,
      subcategory_id: value.subcategory_id,
      post_id: value.slide_id
    }
    await this.props.dispatch(startDeleteFavSlider(formData))

    const newData = this.state.data.filter((item) => item.slide_id != value.slide_id)
    this.setState({ data: newData })
  }

  handlePageChange = (page) => {
    this.setState({
      selectedIndex: page,
    });
  };

  renderPagination = (index, total) => {
    return (
      <View style={Styles.paginationStyle}>
        <Text style={{ color: "grey" }}>
          <Text style={Styles.paginationText}>{index + 1}</Text>/{total}
        </Text>
      </View>
    );
  };

  setRef = (ref) => {
    this.videoRefs.push(ref);
  };

  onViewableItemsChanged = props => {
    const changed = props.changed;
    changed.forEach(item => {
      const video = this.videoRefs[item.index];
      if (video) {
        if (!item.isViewable && videos.includes(item.item.file_type)) {
          console.log('I ranß')
          video.pauseAsync();
        }
      }
    })
  };


  render() {
    const { data, loading } = this.state;
    //console.log(this.videoRefs)
    /*if (loading) {
      return (
        <View style={{ height: "100%", width: "100%", justifyContent: "center", alignItems: "center" }}>
          <ActivityIndicator color={colors.main} size='small' />
        </View>
      )
    }*/

    return (
      <>
        <View style={{ ...Styles.safeViewStyle }}>
          <BackHeader
            navigation={this.props.navigation}
            headerName={this.props.navigation?.getParam('data')?.subcategory_name}
            screen={'FavResourceSubcategory'}
            nextShow={false}
          />
          {
            loading
              ? (<View style={{ height: "80%", width: "100%", alignItems: "center", justifyContent: "center" }}>
                <ActivityIndicator color={colors.main} size='large' />
              </View>)
              : this.state.data?.length > 0 ?
              <FlatList
                data={data}
                horizontal
                onViewableItemsChanged={this.onViewableItemsChanged}
                viewabilityConfig={{
                  viewAreaCoveragePercentThreshold: 90
                }}
                pagingEnabled
                keyExtractor={item => item?.slide_id}
                renderItem={({ item, index }) => {
                  return (
                    <View style={{ height: "100%", width: width, }}>
                      <View style={{ position: "absolute", top: 10, right: 10, width: 110, zIndex: 5, flexDirection: 'row', alignItems: 'center', }}>
                        <TouchableOpacity
                          onPress={() => {
                            this.removeResourceFavorite(index, item);
                          }}
                          style={{ ...Styles.close, marginRight: 10 }}
                        >
                          <Image
                            source={Images.closeIcon}
                            style={{
                              width: 23,
                              height: 20,
                              tintColor: item.favorite ? "#F33636" : "#BDBDBD",
                            }}
                          />
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => {
                            item.link?.length != 0 ? this.props.navigation.navigate('WebViewScreen', { link: item.link }) : Toast('No Link provided for this slide')
                          }}
                          style={Styles.close}
                        >
                          <MaterialCommunityIcons name='web' size={25} color={colors.main} />
                        </TouchableOpacity>
                      </View>
                      {
                        images.includes(item.file_type) ?
                          <View style={{ height: '100%', width: '100%' }}>
                            <Image
                              ref={this.setRef}
                              resizeMode="cover"
                              style={{ height: "100%", width: "100%" }}
                              source={{ uri: IMAGE_BASE_URL + item.slide }} />
                            <View style={[GlobalStyles.mediumText, Styles.paginationStyle]}>
                              <Text style={[GlobalStyles.mediumText, Styles.paginationText]}>
                                {index + 1}/{data.length}
                              </Text>
                            </View>
                          </View> :
                          <View style={{ height: '100%', width: '100%', }}>
                            {videos.includes(item.file_type) ?
                              <View style={{ height: '100%', width: '100%', }}>
                                <Video
                                  ref={this.setRef}
                                  style={[
                                    Styles.video,
                                    { position: "absolute", top: 0 },
                                  ]}
                                  resizeMode={"contain"}
                                  source={{ uri: IMAGE_BASE_URL + item.slide }}
                                  useNativeControls
                                  isLooping
                                  onPlaybackStatusUpdate={(status) => {
                                    this.setState({ status: status });
                                  }}
                                /></View> : null}
                            <View style={[GlobalStyles.mediumText, Styles.paginationStyle]}>
                              <Text style={[GlobalStyles.mediumText, Styles.paginationText]}>
                                {index + 1}/{data.length}
                              </Text>
                            </View>
                            {/* <View style={[GlobalStyles.mediumText, Styles.paginationStyle]}>
                              <Text style={[GlobalStyles.mediumText, Styles.paginationText]}>
                                {index + 1}/{jobSlider.length}
                              </Text>
                            </View> */}
                          </View>
                      }
                    </View>
                  )
                }}
              />
             : (
              <View style={{width: '100%', height: '80%', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={[GlobalStyles.boldText, {fontSize: 16, color: Colors.primary}]}>No Data</Text>
              </View>
            )
          }
        </View>
      </>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user

  }

}
export default connect(mapStateToProps)(FavResourceSlide);
