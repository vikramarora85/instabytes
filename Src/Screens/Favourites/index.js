import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Animated,
  ImageBackground,
  ActivityIndicator,
  Modal,
  Dimensions
} from "react-native";
import Images from "../../Styles/Images";
import BackHeader from "../../Components/BackHeader";
import SegmentedControlTab from "react-native-segmented-control-tab";
import Styles from "./Styles";
import Colors from "../../Styles/Colors";
import GlobalStyles from "../../Styles/globalStyles";
import { startGetFav, startDeleteFav, startRemoveFavJobCategory, startRemoveFavCategory, setFreeResoure, setFavResource, setFavJobs, startFetchData } from "../../actions/userAction";
import { connect } from "react-redux";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { IMAGE_BASE_URL } from "../../AllConstants";
import colors from "../../Styles/Colors";
import Header from "../../Components/Header";
import BuyCourse from "../BuyCourse";
import Subscription from "../SubscriptionModal";

const height = Dimensions.get("window").height;

class Favourites extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      showGradient: false,
      loader: true,
      user: {},
      fadeAnimRes: new Animated.Value(1),
      fadeAnimJob: new Animated.Value(1),
      animatedIndexJob: null,
      animatedIndexRes: null,
      modal_visible: false,
      modal_visible_job: false,
      visible_data: {},
    };
  }

  async componentDidMount() {
    await this.props.dispatch(startGetFav());
    this.fetchData()
    this.setState({ loader: false });

  }

  fetchData = async () => {
    const val = await AsyncStorage.getItem('user')
    const value = JSON.parse(val)
    this.setState({
      user: value
    })
  }

  setModalFalse = () => {
    this.setState({
      modal_visible: false,
      visible_data: {}
    })
  }

  setModalJobFalse = () => {
    this.setState({
      modal_visible_job: false,
      visible_data: {}
    })
  }

  navigate = (val) =>{
    this.setState({
      modal_visible_job:false,
      visible_data:{}
    })
    this.props.navigation.navigate(val,{data:{type: 'subscription'}})
  }

  fadeOutRes = () => {
    Animated.timing(this.state.fadeAnimRes, {
      toValue: 0,
      duration: 1000,
      useNativeDriver: false
    }).start();
  }

  fadeOutJob = () => {
    Animated.timing(this.state.fadeAnimJob, {
      toValue: 0,
      duration: 1000,
      useNativeDriver: false
    }).start();
  }

  animateRes = async (index, value) => {
    this.setState({
      animatedIndexRes: index
    });
    this.fadeOutRes();
    this.removeResourceFav(index, value)
  }

  animateJob = async (index, value) => {
    this.setState({
      animatedIndexJob: index
    });
    this.fadeOutJob();
    this.removeJobFavorite(index, value)
  }

  handleClick = (value) => {
    if (this.state.selectedIndex === 0) {
      if (value.price_type =="1" && value.purchasedId == null) {
        this.setState({ visible_data: value, modal_visible: true })
      } else {
        this.props.navigation.navigate('FavResourceSubcategory', {
          catId: value.category_id,
          data: value
        })
      }

    } else {
      if (value.subscription_type == "1" && this.state.user?.is_subscribed == "0") {
        this.setState({ visible_data: value, modal_visible_job: true })
      } else {
        this.props.navigation.navigate('FavJobSubcategory', {
          jobId: value.jobcategory_id,
          data: value
        })
      }
    }
  };

  handleIndexChange = (index) => {
    this.setState({ selectedIndex: index });
  };
  getStartedPress = () => {
    // this.setState({ showGradient: !this.state.showGradient })
    this.setTime();
  };
  setTime = () => {
    setTimeout(() => {
      this.props.navigation.navigate("LearningCategories");
    }, 50);
  };
  removeResourceFav = async (index, value) => {

    console.log(value)


    const formData = {
      category_id: value.category_id,
    };

    const data = this.props?.user?.favResource?.filter((item) => item.category_id !== value.category_id)

    await this.props.dispatch(startRemoveFavCategory(formData));
    await this.props.dispatch(startFetchData())
    setTimeout(() => {
      this.props.dispatch(setFavResource(data))
      this.setState({
        animatedIndexRes: null
      });
    }, 1000)
  };


  removeJobFavorite = async (index, value) => {

    AsyncStorage.setItem("FavouriteId", JSON.stringify(value.favorite_id));
    const data = this.props.user.favJobs.filter((item) => item.jobcategory_id !== value.jobcategory_id)

    const categoryId = {
      job_id: value.jobcategory_id
    }

    await this.props.dispatch(startRemoveFavJobCategory(categoryId));
    await this.props.dispatch(startFetchData())
    await this.props.dispatch(setFavResource(data))
    setTimeout(() => {
      this.props.dispatch(setFavJobs(data))
      this.setState({
        animatedIndexJob: null
      });
    }, 1000)
    // let favoriteList = this.props.user.favJobs.filter(data => {
    //   return data.id !== value.favorite_id;
    // });
    // this.setState({freeData:favoriteList})
  };
  render() {
    const { selectedIndex } = this.state;
    const { favResource, favJobs } = this.props.user;
    console.log(favResource)
    return (
      <>
        <View style={Styles.safeViewStyle}>
          <BackHeader
            navigation={this.props.navigation}
            headerName={"Favourites"}
            screen={"LearningCategories"}
            nextShow={false}
          />
          {this.state.loader ? (
            <ActivityIndicator color={Colors.appHeaderColor} style={Styles.activityIndicator} />
          ) : (
            <>
              <View style={{flex:1}}>
                <View style={Styles.segmentMainContainer}>
                  <View style={Styles.segmentWrapper}>
                    {selectedIndex === 1 ? (
                      <SegmentedControlTab
                        values={["Favourite Resource", "Favourite Jobs"]}
                        selectedIndex={this.state.selectedIndex}
                        onTabPress={this.handleIndexChange}
                        borderWidth={0}
                        borderLeftWidth={0}
                        borderLeftColor={Colors.White}
                        borderRightColor={Colors.White}
                        activeTabStyle={{
                          backgroundColor: "transparent",
                          borderBottomWidth: 4,
                          borderWidth: 0,
                          borderRightColor: Colors.White,
                          borderRightWidth: 0,
                          borderLeftColor: Colors.White,
                          borderBottomColor: colors.main,
                          borderColor: Colors.White,
                        }}
                        activeTabTextStyle={{
                          color: Colors.appHeaderColor,
                          fontSize: 17,
                          fontWeight: "600",
                          marginVertical: 5,
                          fontFamily: GlobalStyles.mediumText.fontFamily,
                        }}
                        tabStyle={{
                          backgroundColor: "transparent",
                          borderBottomColor: "#454647",
                          borderBottomWidth: 1,
                          borderRightColor: Colors.White,
                          borderLeftColor: Colors.White,
                          borderLeftWidth: 0,
                          borderWidth: 0,
                          borderColor: Colors.White,
                        }}
                        tabTextStyle={{
                          color: Colors.ok,
                          fontSize: 17,
                          fontWeight: "600",
                          marginVertical: 5,
                          fontFamily: GlobalStyles.mediumText.fontFamily,
                        }}
                      />
                    ) : (
                      <SegmentedControlTab
                        values={["Favourite Resource", "Favourite Jobs"]}
                        selectedIndex={this.state.selectedIndex}
                        onTabPress={this.handleIndexChange}
                        borderWidth={0}
                        borderLeftWidth={0}
                        borderLeftColor={Colors.White}
                        borderRightColor={Colors.White}
                        activeTabStyle={{
                          backgroundColor: "transparent",
                          borderBottomWidth: 4,
                          borderWidth: 0,
                          borderRightColor: Colors.White,
                          borderRightWidth: 0,
                          borderLeftColor: Colors.White,
                          borderBottomColor: colors.main,
                          borderColor: Colors.White,
                        }}
                        activeTabTextStyle={{
                          color: Colors.appHeaderColor,
                          fontSize: 18,
                          fontWeight: "600",
                          marginVertical: 5,
                          fontFamily: GlobalStyles.mediumText.fontFamily,
                        }}
                        tabStyle={{
                          backgroundColor: "transparent",
                          borderBottomColor: "#454647",
                          borderBottomWidth: 1,
                          borderRightColor: Colors.White,
                          borderLeftColor: Colors.White,
                          borderLeftWidth: 0,
                          borderWidth: 0,
                          borderColor: Colors.White,
                        }}
                        tabTextStyle={{
                          color: Colors.ok,
                          fontSize: 16,
                          fontWeight: "600",
                          marginVertical: 5,
                          fontFamily: GlobalStyles.mediumText.fontFamily,
                        }}
                      />
                    )}
                  </View>
                </View>
                <View style={{flex:1}}>
                <ScrollView>
                  {selectedIndex === 0 ? (
                    <View style={Styles.headerContainer}>
                      {favResource?.length > 0 ?
                        favResource.map((value, index) => {
                          return (
                            <>
                              <TouchableOpacity
                                onPress={() => {
                                  if (value.price_type === "1") {
                                    if (value.purchasedId) {
                                      this.setState({ visible_data: value, modal_visible: true });
                                    } else {
                                      this.handleClick(value);
                                    }
                                  } else {
                                    this.handleClick(value);
                                  }
                                }}
                              >
                                <Animated.View style={{ ...Styles.listWrapper, opacity: this.state.animatedIndexRes === index ? this.state.animatedIndexRes : 1 }}>
                                  <View style={Styles.innerList}>
                                    <ImageBackground
                                      source={{ uri: IMAGE_BASE_URL + value.category_image }}
                                      imageStyle={{ borderRadius: 12 }}
                                      style={Styles.headerContentInner}
                                    >
                                      <View style={{ height: "100%", width: "100%", alignItems: "center", justifyContent: "center", backgroundColor: "rgba(0, 0, 0, 0.2)", borderRadius:12 }}>
                                      {value.price_type === "1" && value.purchasedId ? <TouchableOpacity
                                        onPress={() => {
                                          this.setState({ visible_data: value, modal_visible: true })
                                        }}
                                      >
                                        <Image
                                          source={Images.lockPremium}
                                          style={Styles.playIconStyle1}
                                        />
                                      </TouchableOpacity> : null}
                                      </View>
                                    </ImageBackground>
                                    <Text
                                      style={[
                                        GlobalStyles.mediumText,
                                        Styles.titleText,
                                      ]}
                                    >
                                      {value.name}
                                    </Text>
                                  </View>
                                  <TouchableOpacity
                                    onPress={() => {
                                      this.animateRes(index, value);
                                    }}
                                  >
                                    <Image
                                      source={Images.closeIcon}
                                      style={{
                                        width: 25,
                                        height: 25,
                                        marginTop: 15,
                                        marginRight: 15,
                                      }}
                                    />
                                  </TouchableOpacity>
                                </Animated.View>
                              </TouchableOpacity>
                            </>
                          );
                        }) : <Text style={[GlobalStyles.boldText, { textAlign: "center", marginTop: height * 0.30, color:colors.main }]}>No Data</Text>}
                    </View>
                  ) : (
                    <View style={Styles.headerContainer}>
                      {favJobs?.length > 0 ?
                        favJobs.map((value, index) => {
                          return (
                            <>
                              <TouchableOpacity
                                onPress={() => {
                                  this.handleClick(value);
                                }}
                              >
                                <Animated.View style={{ ...Styles.listWrapper, opacity: this.state.animatedIndexJob === index ? this.state.animatedIndexJob : 1 }}>
                                  <View style={Styles.innerList}>
                                    <ImageBackground
                                      source={{ uri: IMAGE_BASE_URL + value.jobcategory_image }}
                                      imageStyle={{ borderRadius: 12 }}
                                      style={Styles.headerContentInner}
                                    >
                                      <View style={{ height: "100%", width: "100%", alignItems: "center", justifyContent: "center", backgroundColor: "rgba(0, 0, 0, 0.2)", borderRadius:12 }}>
                                      {value.subscription_type == "1" ? this.state.user?.is_subscribed == "0" ? <TouchableOpacity
                                        onPress={() => {
                                          this.setState({ visible_data: value, modal_visible_job: true })
                                        }}
                                      >
                                        <Image
                                          source={Images.lockPremium}
                                          style={Styles.playIconStyle1}
                                        />
                                      </TouchableOpacity> : null : null}
                                      </View>
                                    </ImageBackground>
                                    <Text
                                      style={[
                                        GlobalStyles.mediumText,
                                        Styles.titleText,
                                      ]}
                                    >
                                      {value.jocategory_name}
                                    </Text>
                                  </View>
                                  <TouchableOpacity
                                    onPress={() => {
                                      this.animateJob(index, value);
                                    }}
                                  >
                                    <Image
                                      source={Images.closeIcon}
                                      style={{
                                        width: 25,
                                        height: 25,
                                        marginTop: 15,
                                        marginRight: 15,
                                      }}
                                    />
                                  </TouchableOpacity>
                                </Animated.View>
                              </TouchableOpacity>
                            </>
                          );
                        }) : <Text style={[GlobalStyles.boldText, { textAlign: "center", marginTop: height * 0.30, color:colors.main }]}>No Data</Text>}
                    </View>
                  )}

                </ScrollView>
                </View>
              </View>
              <Modal onRequestClose={() => this.setModalFalse} transparent animationType='fade' visible={this.state.modal_visible}>
                <View style={{ height: '100%', width: "100%", backgroundColor: 'rgba(0,0,0,0.5)', }}>
                  <BuyCourse navigation={this.props.navigation} onCancel={this.setModalFalse} data={this.state.visible_data} />
                </View>
              </Modal>
              <Modal onRequestClose={() => this.setModalJobFalse} transparent animationType='fade' visible={this.state.modal_visible_job}>
                <View style={{ height: '100%', width: "100%", backgroundColor: 'rgba(0,0,0,0.5)', }}>
                  <Subscription navigate={this.navigate} onCancel={this.setModalJobFalse} data={this.state.visible_data} />
                </View>
              </Modal>
            </>
          )}
        </View>
        <TouchableOpacity
          style={Styles.buttonContainer}
          onPress={this.getStartedPress}
        >
          <Text style={[GlobalStyles.boldText, Styles.buttonText]}>
            {"Back to Home"}
          </Text>
        </TouchableOpacity>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps)(Favourites);
