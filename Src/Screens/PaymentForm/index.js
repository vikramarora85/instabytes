import React, { Component, createRef } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Image,
  TextInput,
  ImageBackground, ActivityIndicator
} from 'react-native';
import Styles from './Styles';
import Images from '../../Styles/Images';
import Colors from '../../Styles/Colors';
import * as Constants from '../../AllConstants';
import BackHeader from '../../Components/BackHeader';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from '../../Components/Toast';
import { setProfile, startFetchData } from '../../actions/userAction';
import { connect } from 'react-redux';
import GlobalStyles from "../../Styles/globalStyles";
class PaymentForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      cardNumber: '',
      showgradient: false,
      cardName: '',
      cardExpiryMonth: '',
      cardCSV: '',
      cardExpiryYear: '',
      loading: false
    };
    this.monthRef = null;
    this.yearRef = null;
  }

  getStartedPress = async () => {
    this.cnfrmpayment()
  }

  cnfrmpayment = async () => {
    this.setState({loading: true});
    const res = await AsyncStorage.getItem('token');
    const token = JSON.parse(res)


    const data = this.props?.navigation?.getParam('data')

    if (data.type == "subscription") {
      fetch('http://instabytes.in/api/subscribepayment', {
        method: "POST",
        headers: {
          "accept": "application/json",
          "content-type": "application/json",
          "Authorization": `Bearer ${token}`
        },
        body: JSON.stringify({
          "card-number": this.state.cardNumber,
          "expiry-month":this.state.cardExpiryMonth,
          "expiry-year": this.state.cardExpiryYear,
          "cvv": this.state.cardCSV,
          "total": "100",
          "subscription_id": "1",
          "currency": "USD",
        })
      }).then((res) => res.json())
        .then((json) => {
          console.log(json)
          this.setState({loading: false});
          if (json.status == '1') {
            Toast('Payment Successfull');
            this.props.setData({...this.props.user, is_subscribed:"1"})
            this.setTime()
          } else {
            Toast(json.message)
          }
        })
    } else {
      fetch('http://instabytes.in/api/purchasecategory', {
        method: "POST",
        headers: {
          "accept": "application/json",
          "content-type": "application/json",
          "Authorization": `Bearer ${token}`
        },
        body: JSON.stringify({
          "card-number": this.state.cardNumber,
          "expiry-month": this.state.cardExpiryMonth,
          "expiry-year": this.state.cardExpiryYear,
          "cvv": this.state.cardCSV,
          "total": data.data.price,
          "category_id": data.id,
          "currency": "USD",
        })
      }).then((res) => res.json())
        .then((json) => {
          this.setState({loading: false});
          console.log(json)
          if (json.status == '1') {
            Toast('Payment Successfull');
            this.setTime()
          } else {
            Toast(json.message)
          }
        })
    }
  }

  focusFncMnth = (value) => {
    if (value.length == 2) {
      this.yearRef.focus()
    }
  }

  focusFncYear = (value) => {
    if (value.length == 0) {
      this.monthRef.focus()
    }
  }


  setTime = async () => {
    await this.props.dispatch(startFetchData());
    setTimeout(() => {
      this.props.navigation.navigate('Gratitude')
    }, 50);
  }
  render() {
    const { showgradient, cardNumber, cardName, cardExpiryMonth, cardExpiryYear, cardCSV } = this.state
    return (
      <>
        <View style={Styles.safeViewStyle}>
          <BackHeader
            navigation={this.props.navigation}
            headerName={'Payment Methods'}
            screen={'SelectPayment'}
            nextShow={false}
          />
          <ScrollView>
            <View style={Styles.mainWrapper}>
              <View style={Styles.mainContentWrapper}>
                {/* HEading */}
                <View style={{ flexDirection: 'row', marginTop: 30, }}>
                  <Image source={Images.credit_card} style={{
                    width: 40,
                    height: 40
                  }} />
                  <Text style={Styles.headingText}>
                    {'Add Credit / Debit card'}
                  </Text>
                </View>
                {/* Ends */}

                <Text style={Styles.textInputHeading}>{'Card Holder’s Name'}</Text>

                <View style={Styles.emailWrapper}>
                  <TextInput
                    style={Styles.emailInput}
                    value={cardName}
                    placeholder={"Name"}
                    placeholderTextColor={Colors.ok}
                    autoCapitalize='none'
                    onChangeText={(value) => {
                      this.setState({ cardName: value })
                    }}
                  />
                </View>


                <Text style={Styles.textInputHeading}>{'Card Number'}</Text>

                <View style={Styles.emailWrapper}>
                  <TextInput
                    style={Styles.emailInput}
                    value={cardNumber}
                    placeholder={'Card Number'}
                    placeholderTextColor={Colors.ok}
                    autoCapitalize='none'
                    onChangeText={(value) => {
                      this.setState({ cardNumber: value })
                    }}
                  />
                  <Image source={Images.master} style={Styles.inputImageLock} />
                </View>



                <View style={{ flexDirection: 'row', }}>
                  <View>
                    <Text style={Styles.textInputHeading}>{'Expiry Date'}</Text>
                    <View style={Styles.expWrapper}>
                      <TextInput
                        ref={(ref) => this.monthRef = ref}
                        style={Styles.emailInput}
                        value={cardExpiryMonth}
                        maxLength={2}
                        textAlign={'center'}
                        keyboardType={'phone-pad'}
                        placeholder={'MM'}
                        placeholderTextColor={Colors.ok}
                        autoCapitalize='none'
                        onChangeText={(value) => {
                          this.focusFncMnth(value)
                          this.setState({ cardExpiryMonth: value })
                        }}
                      />
                      <Text>/</Text>
                      <TextInput
                        ref={(ref) => this.yearRef = ref}
                        style={Styles.emailInputTwo}
                        value={cardExpiryYear}
                        maxLength={4}
                        textAlign={'center'}
                        keyboardType={'phone-pad'}
                        placeholder={'YY'}
                        placeholderTextColor={Colors.ok}
                        autoCapitalize='none'
                        onChangeText={(value) => {
                          this.focusFncYear(value)
                          this.setState({ cardExpiryYear: value })
                        }}
                      />
                    </View>
                  </View>
                  <View>
                    <Text style={Styles.textInputHeading}>{'CVV'}</Text>
                    <View style={Styles.emailWrapper1}>
                      <TextInput
                        style={Styles.emailInput}
                        value={cardCSV}
                        placeholder={'CVV'}
                        placeholderTextColor={Colors.ok}
                        autoCapitalize='none'
                        onChangeText={(value) => {
                          this.setState({ cardCSV: value })
                        }}
                      />
                      <Image source={Images.information} style={Styles.inputImageLock} />
                    </View>
                  </View>
                </View>

              </View>
            </View>
          </ScrollView>
          <View style={Styles.bottomWrapper}>
            <TouchableOpacity
              style={Styles.buttonContainer}
              onPress={this.getStartedPress}>
              {this.state.loading ? <ActivityIndicator color='white' size='small' /> : <Text style={Styles.buttonText}>{'Pay'}</Text>}
            </TouchableOpacity>
          </View>
        </View>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};

const mapDispatchToProps = (dispatch) => ({
  setData: (data) => dispatch(setProfile(data)),
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(PaymentForm);
