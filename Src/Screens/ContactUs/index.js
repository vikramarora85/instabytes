import React, { Component, createRef } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Image,
  TextInput,
  ImageBackground, ActivityIndicator
} from 'react-native';
import Styles from './Styles';
import Images from '../../Styles/Images';
import Colors from '../../Styles/Colors';
import * as Constants from '../../AllConstants';
import BackHeader from '../../Components/BackHeader';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from '../../Components/Toast';
import GlobalStyles from "../../Styles/globalStyles";


class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: '',
      profileFocus: false,
      passwordFocus: false,
      password: '',
      settingFocus: false,
      setting: '',
      privacyFocus: false,
      privacy: '',
      subscriptionFocus: false,
      subscription: '',
      showgradient: false,
      loader: false,
    };
  }
  getStartedPress = async () => {
    const val = await AsyncStorage.getItem('token')
    const token = JSON.parse(val);
    const reg =  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if(this.state.profile.length < 2 ){
      Toast("Enter a valid name")
      return;
    }


    if(reg.test(this.state.password) == false){
      Toast("Enter a valid email")
      return;
    }

    if(this.state.subscription.length != 10 ){
      Toast("Enter a valid 10 digit phone number")
      return;
    }


    if(this.state.privacy.length < 10 ){
      Toast("Enter a valid message")
      return;
    }

    this.setState({loader: true});
    fetch(`http://instabytes.in/api/contact-us`,{
      method:"POST",
      headers:{
        "accept":"application/json",
        "content-type":"application/json",
        "Authorization":`Bearer ${token}`
      },
      body:JSON.stringify({
        name: this.state.profile,
        phone: this.state.subscription,
        email:this.state.password,
        message:this.state.privacy
      })
    }).then((res)=> res.json())
      .then((json)=>{
        this.setState({loader: false});
        if(json.status == "1"){
          Toast(json.message)
          this.setTime()
        } else {
          Toast("An error occured, Please try again.")
        }
      }).catch((err)=>{
      Toast("An error occured, Please try again.")
      console.log(err)
    })
  }
  setTime = () => {
    setTimeout(() => {
      this.props.navigation.navigate('LearningCategories')
    }, 50);
  }
  focusPassword = () => {
    this.setState({
      passwordFocus: !this.state.passwordFocus,
    })
  }
  focusName = () => {
    this.setState({
      profileFocus: !this.state.profileFocus,
    })
  }
  focusSetting = () => {
    this.setState({
      settingFocus: !this.state.settingFocus,
    })
  }
  focusPrivacy = () => {
    this.setState({
      privacyFocus: !this.state.privacyFocus,
    })
  }
  focusSubscription = () => {
    this.setState({
      subscriptionFocus: !this.state.subscriptionFocus,
    })
  }
  render() {
    const { profile, profileFocus, password, passwordFocus, showgradient,
      setting, settingFocus, privacy, privacyFocus, subscriptionFocus, subscription
    } = this.state
    return (
      <>
        <View style={Styles.safeViewStyle}>
          <BackHeader
            navigation={this.props.navigation}
            headerName={'Contact us'}
            screen={'LearningCategories'}
            nextShow={false}
          />
          <ScrollView>
            <View style={Styles.headerContainer}>
              {/* Profile */}
              {/* {
                profileFocus &&
                <Text style={Styles.textInputHeading}>{'Name'}</Text>
              } */}
              <View style={//profileFocus ? Styles.emailWrapper2 :
                Styles.emailWrapper}>
                <Image source={Images.settingUser} style={Styles.inputImageLock} />
                <TextInput
                  style={Styles.emailInput}
                  value={profile}
                  placeholder={'Name'}
                  onFocus={this.focusName}
                  onBlur={this.focusName}
                  placeholderTextColor={Colors.textInputColor}
                  autoCapitalize='none'
                  onChangeText={(value) => {
                    this.setState({ profile: value })
                  }}
                />
              </View>



              {/* {
                passwordFocus &&
                <Text style={Styles.textInputHeading}>{'Email'}</Text>
              } */}
              <View style={//passwordFocus ? Styles.emailWrapper2 :
                Styles.emailWrapper}>
                <Image source={Images.mail} style={Styles.inputImageLock} />
                <TextInput
                  style={Styles.emailInput}
                  value={password}
                  placeholder={'Email'}
                  onFocus={this.focusPassword}
                  onBlur={this.focusPassword}
                  placeholderTextColor={Colors.textInputColor}
                  autoCapitalize='none'
                  onChangeText={(value) => {
                    this.setState({ password: value })
                  }}
                />
              </View>



              {/* {
                subscriptionFocus &&
                <Text style={Styles.textInputHeading}>{'Phone'}</Text>
              } */}
              <View style={//subscriptionFocus ? Styles.emailWrapper2 :
                Styles.emailWrapper}>
                <Image source={Images.phone_call} style={Styles.inputImageLock} />
                <TextInput
                  style={Styles.emailInput}
                  value={subscription}
                  placeholder={'Phone'}
                  keyboardType='phone-pad'
                  onFocus={this.focusSubscription}
                  onBlur={this.focusSubscription}
                  placeholderTextColor={Colors.textInputColor}
                  autoCapitalize='none'
                  onChangeText={(value) => {
                    this.setState({ subscription: value })
                  }}
                />
              </View>


              {/* {
                privacyFocus &&
                <Text style={Styles.textInputHeading}>{'Message'}</Text>
              } */}
              <View style={//privacyFocus ? Styles.emailWrapper4 :
                Styles.emailWrapper1}>
                <Image source={Images.messageEdit} style={Styles.inputImageLock1} />
                <TextInput
                  style={Styles.emailInput1}
                  value={privacy}
                  numberOfLines={4}
                  multiline={true}
                  placeholder={'Message'}
                  onFocus={this.focusPrivacy}
                  onBlur={this.focusPrivacy}
                  placeholderTextColor={Colors.textInputColor}
                  autoCapitalize='none'
                  onChangeText={(value) => {
                    this.setState({ privacy: value })
                  }}
                />
              </View>

            </View>
          </ScrollView>
          <TouchableOpacity
            style={Styles.buttonContainer}
            onPress={this.getStartedPress}>
            {this.state.loader ? <ActivityIndicator color='white' size='small' /> : <Text style={[GlobalStyles.boldText, Styles.buttonText]}>{'Send'}</Text>}
          </TouchableOpacity>
        </View>
      </>
    );
  }
}
export default ContactUs;
