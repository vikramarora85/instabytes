import React, { Component, createRef } from "react";
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Image,
  TextInput,
  ImageBackground,
} from "react-native";
import Styles from "./Styles";
import Images from "../../Styles/Images";
import Colors from "../../Styles/Colors";
import * as Constants from "../../AllConstants";
import BackHeader from "../../Components/BackHeader";
import { startChangePassword } from "../../actions/userAction";
import { connect } from "react-redux";
import { ActivityIndicator } from "react-native";
import Feather from '@expo/vector-icons/Feather'
import Header from "../../Components/Header";

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: "",
      profileErr: "",
      profileFocus: false,
      passwordFocus: false,
      password: "",
      passError: "",
      settingFocus: false,
      setting: "",
      privacyFocus: false,
      privacy: "",
      subscriptionFocus: false,
      subscription: "",
      subscriptionErr: "",
      showgradient: false,
      secureTextEntry1: true,
      secureTextEntry2: true,
      secureTextEntry3: true,
      loading: false
    };
  }
  handleSubmit = () => {
    let regPass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/;
    let isValidPass = regPass.test(this.state.password);

    if (this.state.profile === "") {
      this.setState({ profileErr: "Enter 4 Digit OTP" });
    } else if (!isValidPass) {
      this.setState({
        passError:
          "Password should contain one  uppercase, lowercase, number and symbol",
      });
    } else if (this.state.password !== this.state.subscription) {
      this.setState({ subscriptionErr: "Pasword doesnt match" });
    } else {
      this.setState({ profileErr: "", passError: "", subscriptionErr: "", loading: true });
      const formData = {
        old_password: this.state.profile,
        new_password: this.state.password,
        confirm_password: this.state.subscription,
      };
      console.log(formData);

      const onError = () => {
        return this.setState({ loading: false })
      }
      const redirect = () => {
        return this.getStartedPress();
      };
      this.props.dispatch(startChangePassword(formData, redirect, onError));
    }
  };

  getStartedPress = () => {
    this.setState({ showgradient: !this.state.showgradient });
    this.setTime();
  };

  setTime = () => {
    this.setState({ loading: false })
    setTimeout(() => {
      this.props.navigation.navigate("LearningCategories");
    }, 50);
  };

  focusPassword = () => {
    this.setState({
      passwordFocus: !this.state.passwordFocus,
    });
  };

  focusName = () => {
    this.setState({
      profileFocus: !this.state.profileFocus,
    });
  };

  toggleEye1 = () => {
    this.setState({ secureTextEntry1: !this.state.secureTextEntry1 })
  }

  toggleEye2 = () => {
    this.setState({ secureTextEntry2: !this.state.secureTextEntry2 })
  }

  toggleEye3 = () => {
    this.setState({ secureTextEntry3: !this.state.secureTextEntry3 })
  }

  focusSetting = () => {
    this.setState({
      settingFocus: !this.state.settingFocus,
    });
  };

  focusPrivacy = () => {
    this.setState({
      privacyFocus: !this.state.privacyFocus,
    });
  };

  focusSubscription = () => {
    this.setState({
      subscriptionFocus: !this.state.subscriptionFocus,
    });
  };

  render() {
    const {
      profile,
      profileFocus,
      password,
      passwordFocus,
      subscriptionFocus,
      subscription,
      secureTextEntry1,
      secureTextEntry2,
      secureTextEntry3,
      loading
    } = this.state;
    return (
      <>
        <View style={Styles.safeViewStyle}>
          <BackHeader
            navigation={this.props.navigation}
            headerName={"Change Password"}
            screen={"LearningCategories"}
            nextShow={false}
          />
          <ScrollView>
            <View style={Styles.headerContainer}>
              <Text style={Styles.headingText}>{"Create New Password"}</Text>
              {/* Profile */}
              {/* {profileFocus && (
                <Text style={Styles.textInputHeading}>
                  {"Current Password"}
                </Text>
              )} */}
              <View
                style={
                 // profileFocus ? Styles.emailWrapper1 :
                  Styles.emailWrapper
                }
              >
                <Image source={Images.lock} style={Styles.inputImageLock} />
                <TextInput
                  style={Styles.emailInput}
                  value={profile}
                  placeholder={"Current Password"}
                  secureTextEntry={secureTextEntry1}
                  onFocus={this.focusName}
                  onBlur={this.focusName}
                  placeholderTextColor={Colors.textInputColor}
                  autoCapitalize="none"
                  onChangeText={(value) => {
                    this.setState({ profile: value });
                  }}
                />
                <View style={Styles.eyeIcon}>
                  {secureTextEntry1 ? <Feather onPress={this.toggleEye1} name='eye' color='black' size={16} /> : <Feather onPress={this.toggleEye1} name='eye-off' color='black' size={16} />}
                </View>
              </View>
              <Text style={{ color: "red" }}>{this.state.profileErr}</Text>

              {/* {passwordFocus && (
                <Text style={Styles.textInputHeading}>{"New Password"}</Text>
              )} */}
              <View
                style={
                  //passwordFocus ? Styles.emailWrapper1 :
                   Styles.emailWrapper
                }
              >
                <Image source={Images.lock} style={Styles.inputImageLock} />
                <TextInput
                  style={Styles.emailInput}
                  value={password}
                  secureTextEntry={secureTextEntry2}
                  placeholder={"New Password"}
                  onFocus={this.focusPassword}
                  onBlur={this.focusPassword}
                  placeholderTextColor={Colors.textInputColor}
                  autoCapitalize="none"
                  onChangeText={(value) => {
                    this.setState({ password: value });
                  }}
                />
                <View style={Styles.eyeIcon}>
                  {secureTextEntry2 ? <Feather onPress={this.toggleEye2} name='eye' color='black' size={16} /> : <Feather onPress={this.toggleEye1} name='eye-off' color='black' size={16} />}
                </View>
              </View>
              <Text style={{ color: "red" }}>{this.state.passError}</Text>

              {/* {subscriptionFocus && (
                <Text style={Styles.textInputHeading}>
                  {"Confirm Password"}
                </Text>
              )} */}
              <View
                style={
                  // subscriptionFocus ? Styles.emailWrapper1 :
                   Styles.emailWrapper
                }
              >
                <Image source={Images.lock} style={Styles.inputImageLock} />
                <TextInput
                  style={Styles.emailInput}
                  value={subscription}
                  secureTextEntry={secureTextEntry3}
                  placeholder={"Confirm Password"}
                  onFocus={this.focusSubscription}
                  onBlur={this.focusSubscription}
                  placeholderTextColor={Colors.textInputColor}
                  autoCapitalize="none"
                  onChangeText={(value) => {
                    this.setState({ subscription: value });
                  }}
                />
                <View style={Styles.eyeIcon}>
                  {secureTextEntry3 ? <Feather onPress={this.toggleEye3} name='eye' color='black' size={16} /> : <Feather onPress={this.toggleEye1} name='eye-off' color='black' size={16} />}
                </View>
              </View>
              <Text style={{ color: "red" }}>{this.state.subscriptionErr}</Text>
            </View>
          </ScrollView>
          <TouchableOpacity
            style={Styles.buttonContainer}
            onPress={this.handleSubmit}
            disabled={loading}
          >
            {loading ? <ActivityIndicator color='white' size='small' /> : <Text style={Styles.buttonText}>{"Save"}</Text>}
          </TouchableOpacity>
        </View>
      </>
    );
  }
}
export default connect()(ChangePassword);
