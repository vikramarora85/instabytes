import React, { Component, createRef } from "react";
import {
    View,
    Text,
    SafeAreaView,
    TouchableOpacity,
    ScrollView,
    Image,
    TextInput,
    ImageBackground,
} from "react-native";
import Styles from "./Styles";
import Images from "../../Styles/Images";
import Colors from "../../Styles/Colors";
import * as Constants from "../../AllConstants";
import BackHeader from "../../Components/BackHeader";
import { startChangePassword } from "../../actions/userAction";
import { connect } from "react-redux";
import axios from "axios";
import Feather from '@expo/vector-icons/Feather'
import { ActivityIndicator } from "react-native";
import Toast from "../../Components/Toast";

class ForgotPass extends Component {
    constructor(props) {
        super(props);
        this.state = {
            profile: "",
            profileErr: "",
            profileFocus: false,
            passwordFocus: false,
            password: "",
            passError: "",
            settingFocus: false,
            setting: "",
            privacyFocus: false,
            privacy: "",
            subscriptionFocus: false,
            subscription: "",
            subscriptionErr: "",
            showgradient: false,
            secureTextEntry1: true,
            secureTextEntry2: true,
            loading: false
        };
    }
    handleSubmit = async () => {
        let regPass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/;
        let isValidPass = regPass.test(this.state.password);

        if (this.state.profile === "") {
            this.setState({ profileErr: "Enter Your old password" });
        } else if (!isValidPass) {
            this.setState({
                passError:
                    "Password should contain one  uppercase, lowercase, number and symbol",
            });
        } else if (this.state.password !== this.state.subscription) {
            this.setState({ subscriptionErr: "Pasword doesnt match" });
        } else {
            this.setState({ profileErr: "", passError: "", subscriptionErr: "", loading: true });
            const formData = {
                token: this.state.profile,
                password: this.state.password,
            };

            const res = await axios.post('http://instabytes.in/api/auth/reset', formData)

            this.setState({ loading: false })

            if (res.data.status == "1") {
                this.props.navigation.navigate("Login");
                Toast("Password changed successfully.")
                return;
            }
            Toast(res.data.message)

        }
    };

    getStartedPress = () => {
        this.setState({ showgradient: !this.state.showgradient });
        this.setTime();
    };

    setTime = () => {
        setTimeout(() => {
            this.props.navigation.navigate("LearningCategories");
        }, 50);
    };

    focusPassword = () => {
        this.setState({
            passwordFocus: !this.state.passwordFocus,
        });
    };

    focusName = () => {
        this.setState({
            profileFocus: !this.state.profileFocus,
        });
    };

    focusSetting = () => {
        this.setState({
            settingFocus: !this.state.settingFocus,
        });
    };

    focusPrivacy = () => {
        this.setState({
            privacyFocus: !this.state.privacyFocus,
        });
    };

    focusSubscription = () => {
        this.setState({
            subscriptionFocus: !this.state.subscriptionFocus,
        });
    };

    toggleEye1 = () => {
        this.setState({ secureTextEntry1: !this.state.secureTextEntry1 })
    }

    toggleEye2 = () => {
        this.setState({ secureTextEntry2: !this.state.secureTextEntry2 })
    }

    render() {
        const {
            profile,
            profileFocus,
            password,
            passwordFocus,
            showgradient,
            setting,
            settingFocus,
            privacy,
            privacyFocus,
            subscriptionFocus,
            subscription,
            secureTextEntry1,
            secureTextEntry2,
            loading
        } = this.state;
        return (
            <>
                <View style={Styles.safeViewStyle}>
                    <BackHeader
                        navigation={this.props.navigation}
                        headerName={"Reset Password"}
                        screen={"Login"}
                        nextShow={false}
                    />
                    <ScrollView>
                        <View style={Styles.headerContainer}>
                            <Text style={Styles.headingText}>{"Create New Password"}</Text>
                            {/* Profile */}
                            {profileFocus && (
                                <Text style={Styles.textInputHeading}>
                                    {"4 Digit OTP"}
                                </Text>
                            )}
                            <View
                                style={
                                    profileFocus ? Styles.emailWrapper1 : Styles.emailWrapper
                                }
                            >
                                <Image source={Images.lock} style={Styles.inputImageLock} />
                                <TextInput
                                    style={Styles.emailInput}
                                    value={profile}
                                    placeholder={"4 Digit OTP"}
                                    onFocus={this.focusName}
                                    onBlur={this.focusName}
                                    placeholderTextColor={Colors.textInputColor}
                                    maxLength={4}
                                    keyboardType={'phone-pad'}
                                    autoCapitalize="none"
                                    onChangeText={(value) => {
                                        this.setState({ profile: value });
                                    }}
                                />
                            </View>
                            <Text style={{ color: "red" }}>{this.state.profileErr}</Text>

                            {passwordFocus && (
                                <Text style={Styles.textInputHeading}>{"New Password"}</Text>
                            )}
                            <View
                                style={
                                    passwordFocus ? Styles.emailWrapper1 : Styles.emailWrapper
                                }
                            >
                                <Image source={Images.lock} style={Styles.inputImageLock} />
                                <TextInput
                                    style={Styles.emailInput}
                                    value={password}
                                    secureTextEntry={secureTextEntry1}
                                    placeholder={"New Password"}
                                    onFocus={this.focusPassword}
                                    onBlur={this.focusPassword}
                                    placeholderTextColor={Colors.textInputColor}
                                    autoCapitalize="none"
                                    onChangeText={(value) => {
                                        this.setState({ password: value });
                                    }}
                                />
                                <View style={Styles.eyeIcon}>
                                    {secureTextEntry1 ? <Feather onPress={this.toggleEye1} name='eye' color='black' size={16} /> : <Feather onPress={this.toggleEye1} name='eye-off' color='black' size={16} />}
                                </View>
                            </View>
                            <Text style={{ color: "red" }}>{this.state.passError}</Text>

                            {subscriptionFocus && (
                                <Text style={Styles.textInputHeading}>
                                    {"Confirm Password"}
                                </Text>
                            )}
                            <View
                                style={
                                    subscriptionFocus ? Styles.emailWrapper1 : Styles.emailWrapper
                                }
                            >
                                <Image source={Images.lock} style={Styles.inputImageLock} />
                                <TextInput
                                    style={Styles.emailInput}
                                    value={subscription}
                                    secureTextEntry={secureTextEntry2}
                                    placeholder={"Confirm Password"}
                                    onFocus={this.focusSubscription}
                                    onBlur={this.focusSubscription}
                                    placeholderTextColor={Colors.textInputColor}
                                    autoCapitalize="none"
                                    onChangeText={(value) => {
                                        this.setState({ subscription: value });
                                    }}
                                />
                                <View style={Styles.eyeIcon}>
                                    {secureTextEntry2 ? <Feather onPress={this.toggleEye2} name='eye' color='black' size={16} /> : <Feather onPress={this.toggleEye2} name='eye-off' color='black' size={16} />}
                                </View>
                            </View>
                            <Text style={{ color: "red" }}>{this.state.subscriptionErr}</Text>
                        </View>

                        {showgradient ? (
                            <TouchableOpacity
                            // onPress={() => { this.props.navigation.navigate("LearningCategories") }}
                            >
                                <View
                                    style={Styles.buttonContainerGradient}
                                >
                                    <Text style={Styles.buttonText}>{"Submit"}</Text>
                                </View>
                            </TouchableOpacity>
                        ) : (
                            <TouchableOpacity
                                style={Styles.buttonContainer}
                                onPress={this.handleSubmit}
                                disabled={loading}
                            >
                                {loading ? <ActivityIndicator color='white' size='small' /> : <Text style={Styles.buttonText}>{"Submit"}</Text>}
                            </TouchableOpacity>
                        )}
                    </ScrollView>
                </View>
            </>
        );
    }
}
export default ForgotPass;
