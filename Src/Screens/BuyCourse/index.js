import React, { Component } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground,
  Platform,
  Alert
} from 'react-native';
import Styles from './Styles';
import GlobalStyles from '../../Styles/globalStyles';
//import Modal from 'react-native-modal';
import Header from '../../Components/Header';
import Images from "../../Styles/Images";
import RNIap, {
  InAppPurchase,
  PurchaseError,
  SubscriptionPurchase,
  acknowledgePurchaseAndroid,
  consumePurchaseAndroid,
  finishTransaction,
  finishTransactionIOS,
  purchaseErrorListener,
  purchaseUpdatedListener,
} from 'react-native-iap';
// App Bundle > com.dooboolab.test

const itemSkus = Platform.select({
  ios: [
    'com.cooni.point1000',
    'com.cooni.point5000', // dooboolab
  ],
  android: [
    'android.test.purchased',
    'android.test.canceled',
    'android.test.refunded',
    'android.test.item_unavailable',
    // 'point_1000', '5000_point', // dooboolab
  ],
});

const itemSubs = Platform.select({
  ios: [
    'com.cooni.point1000',
    'com.cooni.point5000', // dooboolab
  ],
  android: [
    'test.sub1', // subscription
  ],
});

let purchaseUpdateSubscription;
let purchaseErrorSubscription;
class BuyCourse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      showgradient: false,
      isModalVisible: false,
	    productList: [],
      receipt: '',
      availableItemsMessage: '',
    };
  }
     async componentDidMount(): void {
    try {
      await RNIap.initConnection();
      if (Platform.OS === 'android') {
        await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
      } else {
        await RNIap.clearTransactionIOS();
      }
    } catch (err) {
      console.warn(err.code, err.message);
    }

    purchaseUpdateSubscription = purchaseUpdatedListener(
      async (purchase: InAppPurchase | SubscriptionPurchase) => {
        console.info('purchase', purchase);
        const receipt = purchase.transactionReceipt
          ? purchase.transactionReceipt
          : purchase.originalJson;
        console.info(receipt);
        if (receipt) {
          try {
            const ackResult = await finishTransaction(purchase);
            console.info('ackResult', ackResult);
          } catch (ackErr) {
            console.warn('ackErr', ackErr);
          }

          this.setState({receipt}, () => this.goNext());
        }
      },
    );

    purchaseErrorSubscription = purchaseErrorListener(
      (error: PurchaseError) => {
        console.log('purchaseErrorListener', error);
        Alert.alert('purchase error', JSON.stringify(error));
      },
    );
	this.getItems();
  }
  componentWillUnmount(): void {
    if (purchaseUpdateSubscription) {
      purchaseUpdateSubscription.remove();
      purchaseUpdateSubscription = null;
    }
    if (purchaseErrorSubscription) {
      purchaseErrorSubscription.remove();
      purchaseErrorSubscription = null;
    }
    RNIap.endConnection();
  }

  goNext = (): void => {
    Alert.alert('Receipt', this.state.receipt);
  };

 getItems = async (): void => {
    try {
      const products = await RNIap.getProducts(itemSkus);
      // const products = await RNIap.getSubscriptions(itemSkus);
      console.log('Products', products);
      this.setState({productList: products});
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  getSubscriptions = async (): void => {
    try {
      const products = await RNIap.getSubscriptions(itemSubs);
      console.log('Products', products);
      this.setState({productList: products});
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  getAvailablePurchases = async (): void => {
    try {
      console.info(
        'Get available purchases (non-consumable or unconsumed consumable)',
      );
      const purchases = await RNIap.getAvailablePurchases();
      console.info('Available purchases :: ', purchases);
      if (purchases && purchases.length > 0) {
        this.setState({
          availableItemsMessage: `Got ${purchases.length} items.`,
          receipt: purchases[0].transactionReceipt,
        });
      }
    } catch (err) {
      console.warn(err.code, err.message);
      Alert.alert(err.message);
     
    }
  };

  // Version 3 apis
  requestPurchase = async (sku): void => {
    try {
      RNIap.requestPurchase(sku);
    } catch (err) {
      console.warn(err.code, err.message);
    }
  };

  requestSubscription = async (sku): void => {
    try {
      RNIap.requestSubscription(sku);
    } catch (err) {
      Alert.alert(err.message);
      
    }
  };
 
  getStartedPress = () => {
    this.setState({ showgradient: !this.state.showgradient })
    this.setTime()
  }
  setTime = () => {
    setTimeout(() => {
      this.props.navigation.navigate('LearningCategories')
    }, 10);
  }
  valueChange = () => {
    this.setState({ isModalVisible: true })
  }
  valueChange1 = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
    this.props.onCancel();
    this.props.navigation.navigate("SelectPayment", { data: {type: 'resource', id:this.props.data.id, data:this.props?.data}})
  }
  render() {
    const { showgradient, isModalVisible ,productList} = this.state;
	console.log(productList)
    return (
      <>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <View style={Styles.mainWrapper}>
            <View style={Styles.courseDescription}>
              <Text style={[GlobalStyles.boldText, Styles.headingText]}>
                {this.props?.data?.name}
              </Text>
              <Text style={[GlobalStyles.lightText, Styles.headingdescription]}>
                {'Buy Course at'}
              </Text>
              <Text style={[GlobalStyles.mediumText, Styles.headingPrice]}>
                ${this.props?.data?.price}
              </Text>
              <TouchableOpacity style={Styles.buyContainer}
                onPress={this.valueChange1}
				 onPress={() => this.requestSubscription(this.state.productList[2].productId)}
              >
                <Text style={[GlobalStyles.mediumText, Styles.buttonText]}>{'Buy Now'}</Text>
              </TouchableOpacity>
              <Text style={[GlobalStyles.lightText, Styles.buyPrice]}>
                {'Purchase Valid till 6 months'}
              </Text>

              <TouchableOpacity style={Styles.cancelButton}
                onPress={() => { this.props.onCancel() }}
              >
                <Text style={[GlobalStyles.lightText, Styles.buttonText1]}>{'Cancel'}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </>
    );
  }
}
export default BuyCourse;
