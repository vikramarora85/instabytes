import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../Styles/Colors';
import Colors from '../../Styles/Colors';
const widthScreen = Dimensions.get('window').width;
const styles = StyleSheet.create({
  safeViewStyle1: {
    backgroundColor: 'white'
  },
  safeViewStyle: {
    flex: 1,
    backgroundColor: 'transparent'
  },
  mainWrapper: {
    flex: 0.95,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  bottomWrapper: {
    flex: 0.05,
  },
  buttonContainer: {
    width: widthScreen / 1.2,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.appHeaderColor,
    marginTop: 20,
    alignSelf: 'center',
    borderRadius: 25,
    marginBottom: 10
  },
  buttonText: {
    color: Colors.White,
    fontSize: 20,
  },
  buttonContainerGradient: {
    width: widthScreen / 1.2,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    alignSelf: 'center',
    borderRadius: 25,
    marginBottom: 10
  },
  courseDescription: {
    width: widthScreen / 1.1,
    alignSelf: 'center',
    borderColor: Colors.ok,
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor:'white'
  },
  headingText: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 20,
    color: Colors.primary
  },
  headingdescription: {
    marginTop: 20,
    textAlign: 'center',
    fontWeight: '400',
    fontSize: 16
  },
  headingdescription1: {
    marginTop: 20,
    textAlign: 'center',
    fontWeight: '400',
    fontSize: 18,
    width: '80%',
    alignSelf: 'center'
  },
  headingPrice: {
    marginTop: 20,
    textAlign: 'center',
    fontWeight: '500',
    fontSize: 22,
    color: Colors.primary
  },
  buyPrice: {
    marginTop: 20,
    textAlign: 'center',
    fontWeight: '400',
    fontSize: 14
  },
  buyContainer: {
    width: widthScreen / 1.4,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#42BE65',
    marginTop: 20,
    alignSelf: 'center',
    borderRadius: 25
  },
  cancelButton: {
    width: '40%',
    height: 40,
    borderColor: Colors.appHeaderColor,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.White,
    marginTop: 20,
    alignSelf: 'flex-end',
    borderRadius: 25,
    marginBottom: 20,
    marginRight: 15
  },
  cancelButton1:{
    width: '40%',
    height: 45,
    borderColor: Colors.appHeaderColor,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.White,
    marginTop: 20,
    alignSelf: 'center',
    borderRadius: 25,
    marginBottom: 20,
  },
  cancelButton2:{
    width: '40%',
    height: 45,
    borderColor: Colors.appHeaderColor,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.main,
    marginTop: 20,
    alignSelf: 'center',
    borderRadius: 25,
    marginBottom: 20,
  },
  buttonText1: {
    color: Colors.ok,
    fontSize: 16,
  },
  buttonText2: {
    color: 'white',
    fontSize: 16,
  },
  modelContainer: {
    width: '85%',
    // height: 200,
    backgroundColor: 'white',
    borderRadius: 20,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.5,
  },
  headerContentInner: {
    width: 130,
    height: 180,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    alignSelf: 'center',
  },
  playIconStyle: {
    width: 30,
    height: 30
  },
  textCategory1:{
    fontSize: 16,
    textAlign: 'center',
    color: Colors.White,
    position: 'absolute',
    bottom: 0,
    marginBottom: 10,
  }
});
export default styles;
