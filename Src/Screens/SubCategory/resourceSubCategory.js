import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Dimensions,
  ActivityIndicator
} from "react-native";
import Styles from "./Styles";
import Images from "../../Styles/Images";
import Colors from "../../Styles/Colors";
import GlobalStyles from "../../Styles/globalStyles";
import BackHeader from "../../Components/BackHeader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { connect } from "react-redux";
import {
  startAddFavSubCategory,
  startSubCategory,
  startDeleteFavSubcategory,
  setResourceSubCategory
} from "../../actions/userAction";
import { BaseImageBackground } from "../../Components/ImageUrl";
import { sub } from "react-native-reanimated";
import Header from "../../Components/Header";
import Toast from "../../Components/Toast";
import colors from "../../Styles/Colors";

const height = Dimensions.get('window').height

class ResourceSubCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: false,
      data:[],
      loader: true
    };
  }

  async componentDidMount() {
    await this.props.dispatch(startSubCategory(this.props.navigation?.getParam('data')?.id));
    this.setSubCat()
  }

  setSubCat = () => {
    this.setState({ data: this.props.user.subCategory, loader:false })
  }

  handleIdChange = async (value) => {
    console.log(this.props.navigation?.getParam('data'))
    try {
      await AsyncStorage.setItem("subCategoryId", value.id.toString());
    } catch (e) {
      console.log(e)
    }
    // if(this.props.navigation.state.params.index===0){
    this.props.navigation.navigate("ResourceSlider", {
      subcategory_id: value.id,
      data: value,
      shareable: this.props.navigation?.getParam('data')?.sharable == "1" ? true : false
    });
  };

  handleIndexChange = async (index, value) => {
    const userId = await AsyncStorage.getItem("userId");
    /*const categoryId1 = await AsyncStorage.getItem("categoryId");*/
    const categoryId = this.props.navigation?.getParam('data')?.id
    let allData = this.props.user.subCategory;

    const subcategoryId = {
      category_id: categoryId,
      subcategory_id: value.id
    };
    const formData = {
      subcategory_id: value.id,
      user_id: userId,
      category_id: categoryId,
    };

    if (value.favorite == false) {
      allData[index].favorite = !allData[index].favorite;
      this.setState({ selectedIndex: true });
      this.props.dispatch(startAddFavSubCategory(formData));
      this.props.dispatch(setResourceSubCategory(allData))
    } else {
      allData[index].favorite = !allData[index].favorite;
      this.setState({ selectedIndex: false });
      this.props.dispatch(startDeleteFavSubcategory(subcategoryId));
      this.props.dispatch(setResourceSubCategory(allData))
    }

  };

  onSearch = (val) => {

    const data = this.props.user?.subCategory
    const arr = []

    data.forEach((item)=>{
      if(item.subcategory_name.toLowerCase().includes(val.toLowerCase())){
         arr.push(item)
      }
    })

    this.setState({ data: arr})
    // const id = this.props.navigation?.getParam('data')?.id;
    // const value = await AsyncStorage.getItem('token')
    // const token = JSON.parse(value)

    // fetch(`http://instabytes.in/api/search-subCategory/${id}/${val}`, {
    //   method: 'GET',
    //   headers: {
    //     'accept': 'application/json',
    //     'content-type': 'application/json',
    //     'Authorization': `Bearer ${token}`
    //   }
    // }).then((res) => res.json())
    //   .then((json) => {
    //     console.log(json)
    //     if (json.status == "1") {
    //       this.props.dispatch(setResourceSubCategory(json.data))
    //     } else {
    //       Toast(json.message)
    //     }
    //   })
  }

  render() {
    const { data } = this.state;
    return (
      <>
        <View style={Styles.safeViewStyle}>
        <Header
            navigation={this.props.navigation}
            headerName={this.props.navigation?.getParam('data')?.name || 'Sub Category'}
            screen={"LearningCategories"}
            isSearch={true}
            isShare={false}
            isBackBtn={true}
            onSearch={this.onSearch}
          />
          {
            this.state.loader ? <ActivityIndicator color={Colors.appHeaderColor} style={Styles.activityIndicator} /> :

              <ScrollView>
                <View style={Styles.mainContainer}>
                  {data?.length > 0 ?
                    data.map((value, index) => {
                      return (
                        <>
                          <TouchableOpacity
                            key={value.id.toString()}
                            onPress={() => {
                              this.handleIdChange(value);
                            }}
                          >
                            <View style={Styles.listWrapper}>
                              <View style={Styles.innerList}>
                                <BaseImageBackground
                                  path={value.subcategory_image}
                                  imageStyle={{ borderRadius: 12 }}
                                  style={Styles.headerContentInner}
                                >
                                  <View style={{ height: 120, width: 120, alignItems: "center", justifyContent: "center", backgroundColor: "rgba(0, 0, 0, 0.2)", borderRadius:12 }}>
                                    <TouchableOpacity onPress={() => {
                                      this.handleIdChange(value);
                                    }}>
                                      <Image
                                        source={Images.playIcon}
                                        style={Styles.playIconStyle}
                                      />
                                    </TouchableOpacity>
                                  </View>
                                </BaseImageBackground>
                                <Text
                                  style={[
                                    GlobalStyles.mediumText,
                                    Styles.titleText,
                                  ]}
                                >
                                  {value.subcategory_name}
                                </Text>
                              </View>
                              <TouchableOpacity
                                onPress={() => {
                                  this.handleIndexChange(index, value);
                                }}
                              >
                                <Image
                                  source={Images.heart}
                                  style={{
                                    width: 23,
                                    height: 20,
                                    marginTop: 15,
                                    marginRight: 15,
                                    tintColor: value.favorite == true
                                      ? "#F33636"
                                      : "#BDBDBD",
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                          </TouchableOpacity>
                        </>
                      );
                    }) :
                    <Text style={[GlobalStyles.boldText, {color: colors.main, position:"absolute", top:height * 0.35, textAlign:"center", width:"100%"}]}>No Data</Text>}
                </View>
              </ScrollView>
          }
        </View>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps)(ResourceSubCategory);
