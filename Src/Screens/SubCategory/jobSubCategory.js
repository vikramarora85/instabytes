import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Dimensions,
  ActivityIndicator
} from "react-native";
import Colors from '../../Styles/Colors';
import Styles from "./Styles";
import Images from "../../Styles/Images";
import GlobalStyles from "../../Styles/globalStyles";
import Header from "../../Components/Header";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { connect, useDispatch } from "react-redux";
import {
  startAddFavJobSubCategory,
  startSubCategory,
  startDeleteJobSubFav,
  startJobSubCategory,
  setJobSubCategory,
} from "../../actions/userAction";
import { BaseImageBackground } from "../../Components/ImageUrl";
import BackHeader from "../../Components/BackHeader";
import Toast from "../../Components/Toast";
import colors from "../../Styles/Colors";

const height = Dimensions.get('window').height

class JobSubCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: null,
      data:[],
      loader: true
    };
  }

  async componentDidMount() {
    await this.props.dispatch(startJobSubCategory(this.props.navigation?.getParam('data')?.id));
    this.setSubCat()
  }

  setSubCat = () => {
    this.setState({ data: this.props.user.subJobCategory, loader:false })
  }

  handleIdChange = async (value) => {

    try {
      await AsyncStorage.setItem("subCategoryId", value.id.toString());
    } catch (e) {
      console.log(e)
    }

    this.props.navigation.navigate("JobSlider", {
      id: value.id,
      data: value,
      shareable: this.props.navigation?.getParam('data')?.sharable == "1" ? true : false
    });
  };

  handleIndexChange = async (index, value) => {

    const userId = await AsyncStorage.getItem("userId");
    //const categoryId1 = await AsyncStorage.getItem("categoryId");
    const categoryId = this.props.navigation?.getParam('data').id;
    let allData = this.props.user.subJobCategory;
    const jobSubcategoryId = {
      job_id: this.props.navigation?.getParam('data').id,
      job_subcategory_id: value.id,
    }
    const formData = {
      job_id: this.props.navigation?.getParam('data').id,
      customer_id: userId,
      job_subcategory_id: value.id,
    };
    if (!value.favorite) {
      allData[index].favorite = !allData[index].favorite;
      this.setState({ selectedIndex: true });
      this.props.dispatch(startAddFavJobSubCategory(formData));
      this.props.dispatch(setJobSubCategory(allData))
    } else {
      //console.log('oo', value)
      allData[index].favorite = !allData[index].favorite;
      this.setState({ selectedIndex: false });
      this.props.dispatch(startDeleteJobSubFav(jobSubcategoryId));
      this.props.dispatch(setJobSubCategory(allData))
    }

  };

  onSearch = async (val) => {
    const data = this.props.user?.subJobCategory
    const arr = []

    data.forEach((item)=>{
      if(item.job_subname.toLowerCase().includes(val.toLowerCase())){
         arr.push(item)
      }
    })

    this.setState({ data: arr})
    // const id = this.props.navigation?.getParam('data')?.id;
    // const value = await AsyncStorage.getItem('token')
    // const token = JSON.parse(value)

    // fetch(`http://instabytes.in/api/search-jobSubCategory/${id}/${val}`, {
    //   method: 'GET',
    //   headers: {
    //     'accept': 'application/json',
    //     'content-type': 'application/json',
    //     'Authorization': `Bearer ${token}`
    //   }
    // }).then((res) => res.json())
    //   .then((json) => {
    //     console.log(json)
    //     if (json.status == "1") {
    //       this.props.dispatch(setJobSubCategory(json.data))
    //     } else {
    //       Toast(json.message)
    //     }
    //   })
  }

  render() {
    const { data } = this.state;

    return (
      <>
        <View style={Styles.safeViewStyle}>
          <Header
            navigation={this.props.navigation}
            headerName={this.props.navigation?.getParam('data')?.name || 'Jobs'}
            screen={"LearningCategories"}
            isSearch={true}
            isShare={false}
            isBackBtn={true}
            onSearch={this.onSearch}
          />
          {
            this.state.loader ? <ActivityIndicator color={Colors.appHeaderColor} style={Styles.activityIndicator} /> :

              <ScrollView>
                <View style={Styles.mainContainer}>
                  {data?.length > 0 ?
                    data.map((value, index) => {
                      return (
                        <>
                          <TouchableOpacity
                            onPress={() => {
                              this.handleIdChange(value);
                            }}
                          >
                            <View style={Styles.listWrapper}>
                              <View style={Styles.innerList}>
                                <BaseImageBackground
                                  path={value.job_subimage}
                                  imageStyle={{ borderRadius: 12 }}
                                  style={Styles.headerContentInner}
                                >
                                  <View style={{ height: 120, width: 120, alignItems: "center", justifyContent: "center", backgroundColor: "rgba(0, 0, 0, 0.2)", borderRadius:12 }}>
                                    <TouchableOpacity onPress={() => {
                                      this.handleIdChange(value);
                                    }}>
                                      <Image
                                        source={Images.playIcon}
                                        style={Styles.playIconStyle}
                                      />
                                    </TouchableOpacity>
                                  </View>
                                </BaseImageBackground>
                                <Text
                                  style={[
                                    GlobalStyles.mediumText,
                                    Styles.titleText,
                                  ]}
                                >
                                  {value.job_subname}
                                </Text>
                              </View>
                              <TouchableOpacity
                                onPress={() => {
                                  this.handleIndexChange(index, value);
                                }}
                              >
                                <Image
                                  source={Images.heart}
                                  style={{
                                    width: 23,
                                    height: 20,
                                    marginTop: 15,
                                    marginRight: 15,
                                    tintColor: value.favorite
                                      ? "#F33636"
                                      : "#BDBDBD",
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                          </TouchableOpacity>
                        </>
                      );
                    }) : <Text style={[GlobalStyles.boldText, { color: colors.main, position: "absolute", top: height * 0.35, textAlign: "center", width: "100%" }]}>No Data</Text>}
                </View>
              </ScrollView>
          }
        </View>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps)(JobSubCategory);
