import { StyleSheet } from "react-native";
import colors from "../../Styles/Colors";

export const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "white"
    },

    mailBox: {
        height: 60,
        width: 60,
        borderRadius: 10,
        backgroundColor: colors.main,
        justifyContent: "center",
        alignItems: "center"
    },

    headerText:{
        fontFamily:"DMSerifDisplay_400Regular",
        fontSize:34,
        marginTop:10
    },

    paraText:{
      fontSize:16,
      color:'#718096',
      marginTop:25,
      textAlign:'center',
      width:'90%',
      marginHorizontal:"5%",
    },

    buttonContainer:{
        height:48,
        width:'90%',
        marginHorizontal:"5%",
        backgroundColor:colors.main,
        alignItems:"center",
        justifyContent:"center",
        marginTop:55,
        borderRadius:10
    },

    buttonText:{
       fontSize:14,
       fontFamily:"DMSans_700Bold",
       color:'white'
    }
})