import React from 'react';
import { View, Text, TouchableOpacity, SafeAreaView } from 'react-native';
import { styles } from './style';
import Ionicons from '@expo/vector-icons/Ionicons'

function VerificationScreen({ navigation }) {

    const email = navigation?.getParam('email');

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.mailBox}>
                <Ionicons name='md-mail' color='white' size={26} />
            </View>
            <Text style={styles.headerText}>Email Sent</Text>
            <Text style={styles.paraText}>We have sent an email to verify your account to {email}</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Login')} style={styles.buttonContainer}>
                <Text style={styles.buttonText}>Continue</Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

export default VerificationScreen;