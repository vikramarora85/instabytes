import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../Styles/Colors';
import Colors from '../../Styles/Colors';
const widthScreen = Dimensions.get('window').width;
const styles = StyleSheet.create({
  safeViewStyle: {
    flex: 1,
    backgroundColor: Colors.White,
    height:"100%",
    width:"100%"
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
 },
  gratitudeText: {
    marginTop: 15,
    fontSize: 24,
    width: widthScreen / 1.2,
    alignSelf: 'center',
    textAlign: 'center',
    color: Colors.black
  },
  description: {
    marginTop: 10,
    fontSize: 16,
    width: widthScreen / 1.2,
    alignSelf: 'center',
    textAlign: 'center',
    color: "#6C6C6C"
  },
  buttonContainerGradient: {
    width: widthScreen / 1.2,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 25,
    marginBottom: 30,
    backgroundColor:colors.main
  },
  buttonContainer: {
    width: widthScreen / 1.2,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.appHeaderColor,
    alignSelf: 'center',
    borderRadius: 25,
    position:"absolute",
    bottom:30
  },
  buttonText: {
    color: Colors.White,
    fontSize: 16,
  },
  headerContainer: {
    width: widthScreen / 1.2,
    alignSelf: "center",
    marginTop: 20,
    marginBottom: 20
  },
});
export default styles;
