import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ActivityIndicator,
  Alert
} from 'react-native';
import Styles from './Styles';
import Images from '../../Styles/Images';
import GlobalStyles from '../../Styles/globalStyles';
import BackHeader from '../../Components/BackHeader';
import NotificationList from '../../Components/NotificationList';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Header from '../../Components/Header';
import colors from '../../Styles/Colors';
import Toast from '../../Components/Toast';

class Notifications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggleNotification: false,
      showGradient: false,
      notificationData: [],
      loading: true
    };
  }

  async componentDidMount() {
    const token = await AsyncStorage.getItem('token')
    fetch('http://instabytes.in/api/notifications', {
      method: "GET",
      headers: {
        accept: 'application/json',
        "content-type": 'application/json',
        "Authorization": `Bearer ${JSON.parse(token)}`
      }
    }).then((res) => res.json())
      .then((json) => {
        console.log(json)
        if (json.status == "1") {
          this.setState({ notificationData: json.data, toggleNotification: true, loading: false })
        } else {
          this.setState({ loading: false })
        }
      })
  }

  onchangeToggle = () => {
    this.setState({ toggleNotification: !this.state.toggleNotification })
  }
  getStartedPress = () => {
    this.setState({ showGradient: !this.state.showgradient })
    this.setTime()
  }
  setTime = () => {
    setTimeout(() => {
      this.props.navigation.navigate('LearningCategories')
    }, 10);
  }

  clearData = async () => {
    Alert.alert(
      "Hey!",
      "Please press on ok to clear notifications",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => this.clearDataCnfrm()}
      ]
    );
  }

  clearDataCnfrm = async () => {
    const token = await AsyncStorage.getItem('token')
    fetch('http://instabytes.in/api/clear-notifications', {
      method: "POST",
      headers: {
        accept: 'application/json',
        "content-type": 'application/json',
        "Authorization": `Bearer ${JSON.parse(token)}`
      }
    }).then((res) => res.json())
      .then((json) => {
        console.log(json)
        Toast(json.message)
      })
  }

  render() {
    const { notificationData, loading } = this.state

    if (loading) {
      return (
        <View style={{ height: "100%", width: "100%" }}>
          <BackHeader
            navigation={this.props.navigation}
            headerName={'Notifications'}
            screen={'LearningCategories'}
            nextShow={false}
            notificationHidden={true}
          />
          <ActivityIndicator color={colors.main} size='small' style={{ marginTop: '75%' }} />
          <TouchableOpacity style={Styles.buttonContainer} onPress={this.getStartedPress}>
            <Text style={[GlobalStyles.boldText, Styles.buttonText]}>{'Back to Home'}</Text>
          </TouchableOpacity>
        </View>
      )
    }
    return (
      <>
        <View style={Styles.safeViewStyle}>
          <BackHeader
            navigation={this.props.navigation}
            headerName={'Notifications'}
            screen={'LearningCategories'}
            notificationHidden={true}
            nextShow={false}
            deleteShow={true}
            deletePress={this.clearData}
          />
          {notificationData.length == 0 ? <View style={{ flex: 1, justifyContent: 'center' }}>
            <TouchableOpacity
              onPress={this.onchangeToggle}
            >
              <Image source={Images.mainViewNotification} style={{ width: 70, height: 70, alignSelf: 'center' }} />
            </TouchableOpacity>
            <Text style={[GlobalStyles.boldText, Styles.gratitudeText]}>{'No Notifications yet!'}</Text>
            <Text style={[GlobalStyles.lightText, Styles.description]}>{' Simply browse and Explore Instabyte and create a Favourite. '}</Text>

            {/* Simply browse and Explore Instabyte and create a Favourites */}
          </View> :
            <ScrollView>
              <View style={Styles.headerContainer}>
                {
                  notificationData.length > 0 &&
                  notificationData.map((value) => {
                    return (
                      <>
                        <NotificationList
                          key={value.id.toString()}
                          navigation={this.props.navigation}
                          time={value.time}
                          action={value.action}
                          title={value.title}
                          value={value}
                          description={value.description}
                        />
                      </>
                    )
                  })
                }
              </View>
              <View style={{backgroundColor:"#ffffff", height:100}} />
            </ScrollView>
          }
          <TouchableOpacity style={Styles.buttonContainer} onPress={this.getStartedPress}>
            <Text style={[GlobalStyles.boldText, Styles.buttonText]}>{'Back to Home'}</Text>
          </TouchableOpacity>
        </View>
      </>
    );
  }
}
export default Notifications;
