import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground,
  ActivityIndicator
} from 'react-native';
import Styles from './Styles';
import Colors from '../../Styles/Colors';
import Images from '../../Styles/Images';
import GlobalStyles from '../../Styles/globalStyles';
import BackHeader from "../../Components/BackHeader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { connect } from 'react-redux'
import { setFreeJob, startAddFavJobCategory, startRemoveFavJobCategory } from '../../actions/userAction'
import { BaseImageBackground } from '../../Components/ImageUrl'
import Header from '../../Components/Header';




class FreeJobs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: null,
      loader: true
    };
  }
  handlePress = (value) => {
    // console.log(value.id)
    this.props.navigation.navigate('JobSubCategory',{
      index: 1,
      data:value
    })
  }
  handleIndexChange = async (index, value) => {
    const userId = await AsyncStorage.getItem('userId')
    const data = this.props.user.jobs

    const categoryId = {
      job_id: value.id
    }

    console.log(value, 'jhk')


    const formData = {
      job_id: value.id,
      customer_id: userId
    }
    if (value.favorite) {
      for (var i in data) {
        if (data[i].id == value.id) {
          data[i].favorite = false;
          break; //Stop this loop, we found it!
        }
      }
      this.setState({ selectedIndex: false });
      this.props.dispatch(startRemoveFavJobCategory(categoryId));
      this.props.dispatch(setFreeJob(data))
    } else {
      for (var i in data) {
        if (data[i].id == value.id) {
          data[i].favorite = true;
          break; //Stop this loop, we found it!
        }
      }
      this.setState({ selectedIndex: true });
      this.props.dispatch(startAddFavJobCategory(formData));
      this.props.dispatch(setFreeJob(data))
    }

  };
  componentDidMount() {
    this.setState({ loader: false })
  }
  render() {
    // console.log(this.props)
    const { jobs } = this.props.user
    
    return (
      <>
        {
          this.state.loader ? <ActivityIndicator color={Colors.appHeaderColor} style={Styles.activityIndicator} /> :
            <View style={Styles.safeViewStyle}>
              <Header
                navigation={this.props.navigation}
                headerName={"Free Jobs"}
                screen={"LearningCategories"}
                isBackBtn={true}
                isSearch={false}
              />

              <ScrollView>
                <View style={Styles.mainContainer}>
                  {
                    jobs.length > 0 && jobs.map((value, index) => {

                      return (
                        <>
                          <TouchableOpacity onPress={() => { this.handlePress(value) }}>
                            <View style={Styles.listWrapper}>
                              <View style={Styles.innerList}>
                                <BaseImageBackground
                                  path={value.category_image}
                                  imageStyle={{ borderRadius: 12 }}
                                  style={Styles.headerContentInner}>
                                     <View style={{height:"100%", width:'100%', backgroundColor:"rgba(0, 0, 0, 0.2)", borderRadius:12}}/>
                                </BaseImageBackground>
                                <Text style={[GlobalStyles.mediumText, Styles.titleText]}>
                                  {value.name}
                                </Text>
                              </View>
                              <TouchableOpacity onPress={() => { this.handleIndexChange(index, value) }}>
                                <Image source={Images.heart} style={{
                                  width: 23, height: 20, marginTop: 15, marginRight: 15,
                                  tintColor: value.favorite ? '#F33636' : '#BDBDBD'
                                }} />
                              </TouchableOpacity>
                            </View>
                          </TouchableOpacity>
                        </>
                      )
                    })
                  }
                </View>
              </ScrollView>
            </View>
        }
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user

  }

}
export default connect(mapStateToProps)(FreeJobs);
