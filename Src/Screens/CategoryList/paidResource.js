import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground,
  ActivityIndicator,
  Modal
} from 'react-native';
import Styles from './Styles';
import Colors from '../../Styles/Colors';
import Images from '../../Styles/Images';
import GlobalStyles from '../../Styles/globalStyles';
import Header from '../../Components/Header';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { connect } from 'react-redux'
import { setPaidResource, startAddFavCategory, startAddFavSubCategory, startRemoveFavCategory } from '../../actions/userAction'
import { BaseImageBackground } from '../../Components/ImageUrl'
import BuyCourse from "../BuyCourse";
import BackHeader from '../../Components/BackHeader';


class PaidResource extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: null,
      loader: true,
      modal_visible: false,
      visible_data: {}
      //   freeData: [
      //     {
      //       id: false,
      //       name: 'Title of Sub Category',
      //       image: Images.Mask,
      //       favorite: false,
      //     },
      //     {
      //       id: false,
      //       name: 'Title of Sub Category',
      //       image: Images.Mask_1,
      //       favorite: false,
      //     },
      //     {
      //       id: false,
      //       name: 'Title of Sub Category',
      //       image: Images.Mask,
      //       favorite: false,
      //     },
      //     {
      //       id: false,
      //       name: 'Title of Sub Category',
      //       image: Images.Mask_1,
      //       favorite: false,
      //     },
      //     {
      //       id: false,
      //       name: 'Title of Sub Category',
      //       image: Images.Mask_1,
      //       favorite: false,
      //     },
      //     {
      //       id: false,
      //       name: 'Title of Sub Category',
      //       image: Images.Mask,
      //       favorite: false,
      //     },
      //     {
      //       id: false,
      //       name: 'Title of Sub Category',
      //       image: Images.Mask,
      //       favorite: false,
      //     },
      //     {
      //       id: false,
      //       name: 'Title of Sub Category',
      //       image: Images.Mask_1,
      //       favorite: false,
      //     },
      //   ]
    };
  }


  setModalFalse = () => {
    this.setState({
      modal_visible: false,
      visible_data: {}
    })
  }


  handlePress = (value) => {
    if (value.isPurchased) {
      AsyncStorage.setItem('categoryId', JSON.stringify(value.id))
      this.props.navigation.navigate("ResourceSubCategory", {
        index: this.state.selectedIndex,
        data: value
      });
    } else {
      this.setState({
        modal_visible: true,
        visible_data: value
      })
    }
  }
  handleIndexChange = async (value, index) => {
    const userId = await AsyncStorage.getItem('userId')
    let allData = this.props.user.paidResource
    this.setState({ freeData: allData })

    const formData = {
      category_id: value.id,
      user_id: userId
    }
    if (!value.favorite) {
      allData[index].favorite = !allData[index].favorite
      this.props.dispatch(startAddFavCategory(formData))
      this.props.dispatch(setPaidResource(allData))
    } else {
      allData[index].favorite = !allData[index].favorite
      this.props.dispatch(startRemoveFavCategory(formData))
      this.props.dispatch(setPaidResource(allData))
    }
  };
  async componentDidMount() {
    this.setState({ loader: false })
  }
  render() {
    // console.log(this.props)
    const { paidResource } = this.props.user
    return (
      <>
        {
          this.state.loader ? <ActivityIndicator color={Colors.appHeaderColor} /> :

            <View style={Styles.safeViewStyle}>
              <Header
                isBackBtn={true}
                screen={'LearningCategories'}
                navigation={this.props.navigation}
                headerName={'Premium Learnings'}
                isSearch={false}
              />
              <ScrollView>
                <View style={Styles.mainContainer}>
                  {
                    paidResource?.length > 0 && paidResource.map((value, index) => {
                      console.log(value)
                      return (
                        <>
                          <TouchableOpacity onPress={() => { this.handlePress(value) }}>
                            <View style={Styles.listWrapper}>
                              <View style={Styles.innerList}>
                                <BaseImageBackground
                                  path={value.category_image}
                                  imageStyle={{ borderRadius: 12 }}
                                  style={Styles.headerContentInner}>
                                  <View style={{height:'100%', width:"100%", borderRadius:12, backgroundColor:"rgba(0, 0, 0, 0.2)", alignItems:'center', justifyContent:"center"}}>
                                    <TouchableOpacity onPress={() => { this.handlePress(value) }}>
                                      <Image source={!value.isPurchased ? Images.lockPremium : Images.playIcon} style={Styles.playIconStyle} />
                                    </TouchableOpacity>
                                  </View>
                                </BaseImageBackground>
                                <Text style={[GlobalStyles.mediumText, Styles.titleText]}>
                                  {value.name}
                                </Text>
                              </View>
                              <TouchableOpacity onPress={() => { this.handleIndexChange(value, index) }}>
                                <Image source={Images.heart} style={{
                                  width: 23, height: 20, marginTop: 15, marginRight: 15,
                                  tintColor: value.favorite ? '#F33636' : '#BDBDBD'
                                }} />
                              </TouchableOpacity>
                            </View>
                          </TouchableOpacity>
                        </>
                      )
                    })
                  }
                </View>
              </ScrollView>
            </View>
        }
        <Modal onRequestClose={() => this.setModalFalse} transparent animationType='slide' visible={this.state.modal_visible}>
          <View style={{ height: '100%', width: "100%", backgroundColor: 'rgba(0,0,0,0.5)', }}>
            <BuyCourse navigation={this.props.navigation} onCancel={this.setModalFalse} data={this.state.visible_data} />
          </View>
        </Modal>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user

  }

}
export default connect(mapStateToProps)(PaidResource);
