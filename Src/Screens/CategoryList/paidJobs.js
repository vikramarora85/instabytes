import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Modal,
  Image,
  ImageBackground,
  ActivityIndicator
} from 'react-native';
import Styles from './Styles';
import Images from '../../Styles/Images';
import Colors from '../../Styles/Colors';
import GlobalStyles from '../../Styles/globalStyles';
import Header from '../../Components/Header';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { connect } from 'react-redux'
import { setFavJobs, setPaidJob, startAddFavCategory, startAddFavJobCategory, startAddFavSubCategory, startRemoveFavCategory, startRemoveFavJobCategory } from '../../actions/userAction'
import { BaseImageBackground } from '../../Components/ImageUrl';
import Subscription from "../SubscriptionModal";
import BackHeader from '../../Components/BackHeader';



class PaidJobs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: null,
      loader: true,
      user: {},
      modal_visible:false,
      visible_data:{}
    };
  }

  handlePress = async (value) => {
    if (this.state.user.is_subscribed == "0") {
      this.setState({modal_visible:true, visible_data:value})
    } else {
      await AsyncStorage.setItem('categoryId', JSON.stringify(value.id))
      this.props.navigation.navigate('CategoryList',{ data : value })
    }
  }

  
  handleIndexChange = async (index, value) => {
    const userId = await AsyncStorage.getItem('userId')
    let allData = this.props.user.paidjobs
    console.log(value)

    const formData = {
      job_id: value.id,
      user_id: userId
    }

    if(value.favorite){
      allData[index].favorite = !allData[index].favorite
      this.props.dispatch(startRemoveFavJobCategory(formData))
      this.props.dispatch(setPaidJob(allData))
    } else {
      allData[index].favorite = !allData[index].favorite
      this.props.dispatch(startAddFavJobCategory(formData))
      this.props.dispatch(setPaidJob(allData))
    }
    // console.log(formData)
  };


  async componentDidMount() {
    const user = await AsyncStorage.getItem('user')
    this.setState({ user: JSON.parse(user) })
    this.setState({ loader: false })
  }

  setModalJobFalse = () =>{
    this.setState({
      modal_visible:false,
      visible_data:{}
    })
  }

  navigate = (val) =>{
    this.setState({
      modal_visible:false,
      visible_data:{}
    })
    this.props.navigation.navigate(val)
  }


  render() {
    const { paidjobs } = this.props.user
    return (
      <>
        {
          this.state.loader ? <ActivityIndicator color={Colors.appHeaderColor} /> :

            <View style={Styles.safeViewStyle}>
              <Header
                isBackBtn={true}
                navigation={this.props.navigation}
                headerName={'Premium Jobs'}
                screen={'LearningCategories'}
                isSearch={false}
              />
              <ScrollView>
                <View style={Styles.mainContainer}>
                  {
                    paidjobs.length > 0 && paidjobs.map((value, index) => {
                      return (
                        <>
                          <TouchableOpacity onPress={() => { this.handlePress(value) }}>
                            <View style={Styles.listWrapper}>
                              <View style={Styles.innerList}>
                                <BaseImageBackground
                                  path={value.category_image}
                                  imageStyle={{ borderRadius: 12 }}
                                  style={Styles.headerContentInner}>
                                  <View style={{height:'100%', width:"100%", borderRadius:12, backgroundColor:"rgba(0, 0, 0, 0.2)", alignItems:'center', justifyContent:"center"}}>
                                  <TouchableOpacity onPress={() => this.handlePress(value)} >
                                    <Image source={this.state.user?.is_subscribed == "0" ? Images.lockPremium : Images.playIcon} style={Styles.playIconStyle} />
                                  </TouchableOpacity>
                                  </View>
                                </BaseImageBackground>
                                <Text style={[GlobalStyles.mediumText, Styles.titleText]}>
                                  {value.name}
                                </Text>
                              </View>
                              <TouchableOpacity onPress={() => { this.handleIndexChange(index, value) }}>
                                <Image source={Images.heart} style={{
                                  width: 23, height: 20, marginTop: 15, marginRight: 15,
                                  tintColor: value.favorite ? '#F33636' : '#BDBDBD'
                                }} />
                              </TouchableOpacity>
                            </View>
                          </TouchableOpacity>
                        </>
                      )
                    })
                  }
                </View>
              </ScrollView>
            </View>
        }
        <Modal onRequestClose={() => this.setModalJobFalse} transparent animationType='slide' visible={this.state.modal_visible}>
          <View style={{ height: '100%', width: "100%", backgroundColor: 'rgba(0,0,0,0.5)', }}>
            <Subscription navigate={this.navigate} onCancel={this.setModalJobFalse} data={this.state.visible_data} />
          </View>
        </Modal>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user

  }

}
export default connect(mapStateToProps)(PaidJobs);
