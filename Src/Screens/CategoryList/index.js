import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground,
} from "react-native";
import Styles from "./Styles";
import Images from "../../Styles/Images";
import GlobalStyles from "../../Styles/globalStyles";
import Header from "../../Components/Header";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { connect } from "react-redux";
import {
  startAddFavSubCategory,
  startSubCategory,
} from "../../actions/userAction";

class CategoryList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: null,
      // freeData: [
      //   {
      //     id: false,
      //     name: 'Title of Sub Category',
      //     image: Images.Mask,
      //     favorite: false,
      //   },
      //   {
      //     id: false,
      //     name: 'Title of Sub Category',
      //     image: Images.Mask_1,
      //     favorite: false,
      //   },
      //   {
      //     id: false,
      //     name: 'Title of Sub Category',
      //     image: Images.Mask,
      //     favorite: false,
      //   },
      //   {
      //     id: false,
      //     name: 'Title of Sub Category',
      //     image: Images.Mask_1,
      //     favorite: false,
      //   },
      //   {
      //     id: false,
      //     name: 'Title of Sub Category',
      //     image: Images.Mask_1,
      //     favorite: false,
      //   },
      //   {
      //     id: false,
      //     name: 'Title of Sub Category',
      //     image: Images.Mask,
      //     favorite: false,
      //   },
      //   {
      //     id: false,
      //     name: 'Title of Sub Category',
      //     image: Images.Mask,
      //     favorite: false,
      //   },
      //   {
      //     id: false,
      //     name: 'Title of Sub Category',
      //     image: Images.Mask_1,
      //     favorite: false,
      //   },
      // ]
    };
  }

  componentDidMount(){
    // console.log(this.props.navigation.state.params.index,'in comp')
    // if(this.props.navigation.state.params.index===0){
    //   this.props.navigation.navigate("ResourceSlider")
    // }else{
    //   this.props.navigation.navigate("JobSlider")
      
    // }
    this.props.dispatch(startSubCategory());
  }

  handleIdChange=(value)=>{
    // console.log(value.id)
    // console.log(this.props.navigation.state.params.inde,'in handle')
    AsyncStorage.setItem('subCategoryId',value.id)


  }
  
  handleIndexChange = async (index,value) => {
    const userId = await AsyncStorage.getItem("userId");
    const categoryId1 = await AsyncStorage.getItem("categoryId")
    const categoryId=JSON.parse(categoryId1)
    let allData = this.props.user.subCategory;
    allData[index].favorite = !allData[index].favorite;
    this.setState({ freeData: allData });

    const formData = {
      subcategory_id: value.id,
      user_id: userId,
      category_id: categoryId,
      
    };
    console.log(formData)
    this.props.dispatch(startAddFavSubCategory(formData));

  };
  render() {
    // console.log(this.props.navigation.state.params.index,'index')
    console.log(this.props.user.subCategory)
    const { subCategory } = this.props.user
    return (
      <>
        <View style={Styles.safeViewStyle}>
          <Header
            navigation={this.props.navigation}
            headerName={"Categories"}
            isSearch={false}
          />
          <ScrollView>
            <View style={Styles.mainContainer}>
              {subCategory?.length > 0 &&
                subCategory.map((value, index) => {
                  return (
                    <>
                    {console.log(value)}
                      <TouchableOpacity
                        onPress={() => {
                          this.handleIdChange(value)
                        }}
                      >
                        <View style={Styles.listWrapper}>
                          <View style={Styles.innerList}>
                            <ImageBackground
                              source={value.subcategory_image}
                              imageStyle={{ borderRadius: 12 }}
                              style={Styles.headerContentInner}
                            >
                              <TouchableOpacity>
                                <Image
                                  source={Images.playIcon}
                                  style={Styles.playIconStyle}
                                />
                              </TouchableOpacity>
                            </ImageBackground>
                            <Text
                              style={[
                                GlobalStyles.mediumText,
                                Styles.titleText,
                              ]}
                            >
                              {value.subcategory_name}
                            </Text>
                          </View>
                          <TouchableOpacity
                            onPress={() => {
                              this.handleIndexChange(index,value);
                            }}
                          >
                            <Image
                              source={Images.heart}
                              style={{
                                width: 23,
                                height: 20,
                                marginTop: 15,
                                marginRight: 15,
                                tintColor: value.favorite
                                  ? "#F33636"
                                  : "#BDBDBD",
                              }}
                            />
                          </TouchableOpacity>
                        </View>
                      </TouchableOpacity>
                    </>
                  );
                })}
            </View>
          </ScrollView>
        </View>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps)(CategoryList);
