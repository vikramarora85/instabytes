import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ActivityIndicator,
} from "react-native";
import Styles from "./Styles";
import Images from "../../Styles/Images";
import Colors from '../../Styles/Colors';
import GlobalStyles from "../../Styles/globalStyles";
import BackHeader from "../../Components/BackHeader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { connect } from "react-redux";
import { setFreeResoure, startAddFavCategory, startRemoveFavCategory } from "../../actions/userAction";
import { BaseImageBackground } from "../../Components/ImageUrl";
import Header from "../../Components/Header";

class FreeResource extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: null,
      fav: null,
      loader: true
    };
  }

  handlePress = (value) => {
    // console.log(value)
    this.props.navigation.navigate("ResourceSubCategory", {
      index: 0,
      data: value
    });
  };

  handleIndexChange = async (index, value) => {
    console.log(index);
    const userId = await AsyncStorage.getItem("userId");
    let allData = this.props.user.freeResource;
    this.setState({ fav: allData })
    // console.log(allData);
    // allData[index].favourite_count= !allData[index].favourite_count;
    if (this.state.fav[index].favourite_count == 1) {
      this.setState({ selectedIndex: false })

    } else {
      this.setState({ selectedIndex: true })

    }
    const categoryId = {
      category_id: value.id
    }
    const formData = {
      category_id: value.id,
      customer_id: userId,
    };

    const data = this.props.user.freeResource

    if (value.favorite) {
      for (var i in data) {
        if (data[i].id == value.id) {
          data[i].favorite = false;
          break; //Stop this loop, we found it!
        }
      }
      await this.props.dispatch(startRemoveFavCategory(formData))
      await this.props.dispatch(setFreeResoure(data))
    } else {
      for (var i in data) {
        if (data[i].id == value.id) {
          data[i].favorite = true;
          break; //Stop this loop, we found it!
        }
      }
      await this.props.dispatch(startAddFavCategory(formData))
      await this.props.dispatch(setFreeResoure(data))
    }
    // console.log(value.favourite.length)
    // if (this.state.fav==0) {

    //   this.setState({ selectedIndex: true });
    //   // this.props.dispatch(startAddFavCategory(formData));

    // } else {

    //   this.setState({ selectedIndex: false });
    //   // this.props.dispatch(startRemoveFavCategory(categoryId));

    // }
    // console.log(formData)

  };
  componentDidMount() {
    this.setState({ loader: false })
  }

  render() {
    // console.log(this.props)
    const { freeResource } = this.props.user;
    console.log(freeResource)
    return (
      <>
        {
          this.state.loader ? <ActivityIndicator color={Colors.appHeaderColor} style={Styles.activityIndicator} /> :

            <View style={Styles.safeViewStyle}>
              <Header
                navigation={this.props.navigation}
                headerName={"Free Resources"}
                screen={"LearningCategories"}
                isBackBtn={true}
                isSearch={false}
              />
              <ScrollView>
                <View style={Styles.mainContainer}>
                  {freeResource.length > 0 &&
                    freeResource.map((value, index) => {
                      return (
                        <>
                          <TouchableOpacity
                            onPress={() => {
                              this.handlePress(value);
                            }}
                          >
                            <View style={Styles.listWrapper}>
                              <View style={Styles.innerList}>
                                <BaseImageBackground
                                  path={value.category_image}
                                  imageStyle={{ borderRadius: 12 }}
                                  style={Styles.headerContentInner}
                                >
                                  <View style={{height:"100%", width:'100%', backgroundColor:"rgba(0, 0, 0, 0.2)", borderRadius:12}}/>
                                </BaseImageBackground>
                                <Text
                                  style={[
                                    GlobalStyles.mediumText,
                                    Styles.titleText,
                                  ]}
                                >
                                  {value.name}
                                </Text>
                              </View>
                              <TouchableOpacity
                                onPress={() => {
                                  this.handleIndexChange(index, value);
                                }}
                              >
                                <Image
                                  source={Images.heart}
                                  style={{
                                    width: 23,
                                    height: 20,
                                    marginTop: 15,
                                    marginRight: 15,
                                    tintColor: value.favorite
                                      ? "#F33636"
                                      : "#BDBDBD",
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                          </TouchableOpacity>
                        </>
                      );
                    })}
                </View>
              </ScrollView>
            </View>
        }
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps)(FreeResource);
