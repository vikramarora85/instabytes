import React, { Component } from "react";
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Image,
} from "react-native";
import Styles from "./Styles";
import Images from "../../Styles/Images";
import * as Constants from "../../AllConstants";
import GlobalStyles from "../../Styles/globalStyles";
import AsyncStorage from "@react-native-async-storage/async-storage";

class GetStarted extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showgradient: false,
    };
    this.moved = false;
  }
  async componentDidMount() {

    //this.props.navigation.navigate('resourceSlide', {id:25, slide:5})

            const Token = await AsyncStorage.getItem("token");
		
            if (Token == null) {
				 this.props.navigation.navigate("Login");
             } 
			else {
              this.props.navigation.navigate("LearningCategories");
            }
          
  }

  set_linking = async () => {
    await AsyncStorage.removeItem("is_linking")
  }


  getStartedPress = async () => {
    //this.setState({ showgradient: !this.state.showgradient });
    try {
      await AsyncStorage.setItem('getStarted', 'done')
      this.setTime();
    } catch (e) {
      console.log(e)
    }
    this.setTime();
  };

  setTime = () => {
    setTimeout(() => {
      this.props.navigation.navigate("Signup");
    }, 10);
  };

  render() {
    const { showgradient } = this.state;
    return (
      <>
        <SafeAreaView style={Styles.safeViewStyle}>
          <View style={Styles.mainContainer}>
            <Image source={Images.getStarted} style={Styles.learnLogo} />
          </View>

          <View style={Styles.mainWrapper}>
            <Text style={[GlobalStyles.boldText, Styles.headingText]}>
              {Constants.GETSTARTED_TITLE}
            </Text>
          </View>

          {showgradient ? (
            <TouchableOpacity>
              <View
                style={Styles.buttonContainerGradient}
              >
                <Text style={[GlobalStyles.boldText, Styles.buttonText]}>
                  {Constants.GETSTARTED_BUTTON}
                </Text>
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={Styles.buttonContainer}
              onPress={this.getStartedPress}
            >
              <Text style={[GlobalStyles.boldText, Styles.buttonText]}>
                {Constants.GETSTARTED_BUTTON}
              </Text>
            </TouchableOpacity>
          )}
        </SafeAreaView>
      </>
    );
  }
}
export default GetStarted;
