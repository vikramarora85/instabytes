import React, { useRef, useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, BackHandler, ActivityIndicator, Platform } from 'react-native';
import { WebView } from 'react-native-webview';
import AntDesign from '@expo/vector-icons/AntDesign'
import MaterialIcons from '@expo/vector-icons/MaterialIcons'
import colors from '../../Styles/Colors';

function WebViewScreen({ navigation, route }) {

    const url = navigation.getParam('link');
    const [isLoading, setIsLoading] = useState(true)
    const [canGoBack, setCanGoBack] = useState(true)
    const [canGoForw, setCanGoForw] = useState(true)
    const webViewRef = useRef(null)

    useEffect(() => {
        BackHandler.addEventListener("hardwareBackPress", backButtonHandler);

        return () => {
            BackHandler.removeEventListener("hardwareBackPress", backButtonHandler);
        };
    }, [backButtonHandler, BackHandler, canGoBack]);


    const backButtonHandler = () => {
        if (canGoBack) {
            goBack()
            return true;
        }
        return false;
    }

    const goBack = () => {
        webViewRef.current.goBack();
    };

    const goForward = () => {
        webViewRef.current.goForward();
    };

    const reload = () => {
        setIsLoading(true)
        webViewRef.current.reload();
    };

    const stopLoading = () => {
        setIsLoading(false)
        webViewRef.current.stopLoading();
    };

    return (
        <View style={{ height: "100%", width: "100%" }}>
            <View style={{ position: "absolute", top: '50%', left: '47%', zIndex: 10 }}>
                {isLoading ? <ActivityIndicator color={colors.main} size='large' /> : null}
            </View>
            <View style={{ height: Platform.OS =='android'? 100 : 100 , backgroundColor: colors.main, flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>
                <View style={{ flexDirection: "row", alignItems: "center", marginTop:25 }}>
                    <AntDesign name='arrowleft' onPress={canGoBack ? goBack : null} color={canGoBack ? 'white' : '#CACACA'} size={25} style={{ marginLeft: 10 }} />
                    <AntDesign name='arrowright' onPress={canGoForw ? goForward : null} color={canGoForw ? 'white' : '#CACACA'} size={25} style={{ marginLeft: 10 }} />
                    {isLoading ? <AntDesign onPress={stopLoading} name='close' color='white' size={25} style={{ marginLeft: 15,  }} /> : <AntDesign onPress={reload} name='reload1' color='white' size={23} style={{ marginLeft: 15 }} />}
                </View>
                <MaterialIcons name='exit-to-app' onPress={() => navigation.goBack(null)} color={'white'} size={30} style={{ marginRight: 10, marginTop:25 }} />
            </View>
            <WebView
                ref={webViewRef}
                onLoadEnd={(syntheticEvent) => {
                    // update component to be aware of loading status
                    const { nativeEvent } = syntheticEvent;
                    setIsLoading(nativeEvent.loading);
                    setCanGoBack(nativeEvent.canGoBack)
                    setCanGoForw(nativeEvent.canGoForward)
                }}
                onLoadStart={(syntheticEvent) => {
                    // update component to be aware of loading status
                    const { nativeEvent } = syntheticEvent;
                    setIsLoading(nativeEvent.loading);
                    setCanGoBack(nativeEvent.canGoBack)
                    setCanGoForw(nativeEvent.canGoForward)
                }}
                containerStyle={{ flex: 1, height: '93%' }} source={{ uri: url }} />
        </View>
    )
}

export default WebViewScreen;