import React, { Component } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Image
} from 'react-native';
import Styles from './Styles';
import Images from '../../Styles/Images';
import GlobalStyles from '../../Styles/globalStyles';
import PhoneInput from "react-native-phone-number-input";
import * as Constants from '../../AllConstants';
import FloatingLabel from "../../Components/FloatingLabel";
import { connect } from 'react-redux'
import { startRegisterUser } from '../../actions/userAction';
import * as Google from 'expo-google-app-auth';
import * as Facebook from 'expo-facebook';
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ActivityIndicator } from 'react-native';
import colors from '../../Styles/Colors';
import Toast from '../../Components/Toast';
import { updateExpoToken } from '../../Utils/update_expo_token';

class Signup extends Component {
  constructor(props) {
    super(props);
    this.phoneInput = React.createRef();
    this.state = {
      name: '',
      nameError: '',
      email: '',
      emailError: '',
      password: '',
      passError: '',
      value: '',
      valueError: '',
      showgradient: false,
      phoneFocus: false,
      passwordfocus: false,
      validated: false,
      loading: false,
      googleLoading: false,
      fbLogin: false,
      passEyeIconVisible:true
    };
  }

  handleSubmit = () => {

    let reg = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
    let isValid = reg.test(this.state.email);

    let regPass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/
    let isValidPass = regPass.test(this.state.password)


    if (this.state.name === '') {
      this.setState({ nameError: "Field Required" })
    } else if (!isValid) {
      this.setState({ emailError: "Not a valid email" })

    } else if (!isValidPass) {
      this.setState({ passError: "Password should contain one  uppercase, lowercase, number and symbol" })

    } else if (this.state.value === '') {
      this.setState({ valueError: "Field Required" })
    } else {
      this.setState({ nameError: '', emailError: "", passError: '', valueError: '', loading: true })
      const formData = {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        phone: this.state.value,
        country: 'India'
      }
      console.log(formData)
      const redirect = () => {
        return this.getStartedPress()
      }

      const onError = () => {
        return this.setState({ loading: false })
      }
      this.props.dispatch(startRegisterUser(formData, redirect, onError))
    }
  }

  handleGoogleLogin = () => {
    this.setState({ googleLoading: true })
    const config = {
      iosClientId: `678626351526-q6k64i8ouhq93r3ee0583jm6hnk0k7r9.apps.googleusercontent.com`,
      androidStandaloneAppClientId: '637174475769-mlcgb5uli2s8liosk6teb7pqv98jqf1a.apps.googleusercontent.com',
      androidClientId: `678626351526-f4k7e87nj2a13hi63fr3n27qvngpijdv.apps.googleusercontent.com`,
      scopes: ['profile', 'email']
    }
    Google.logInAsync(config)
      .then((response) => {
        const { type, user } = response
        console.log(response)
        if (type === 'success') {
          this.update({ access_token: response.accessToken, provider: "google" })
        } else {
          this.setState({ googleLoading: false })
        }
      })
      .catch((err) => {
        this.setState({ googleLoading: false })
        console.log(err)
        Toast('An error occured ,Check your network and try again')
      })

  }


  handleFacebookLogin = async () => {
    this.setState({ fbLogin: true })
    try {
      await Facebook.initializeAsync({
        appId: '345570947302726',
      });
      const val = await Facebook.logInWithReadPermissionsAsync({
        permissions: ['public_profile', 'email'],
      });
      console.log(val)
      if (val.type === 'success') {
        this.update({ access_token: val.token, provider: "facebook" })
      } else {
        this.setState({ fbLogin: false })
      }
    } catch ({ message }) {
      console.log(message)
      this.setState({ fbLogin: false })
      Toast(`Facebook Login Error: ${message}`);
    }
  }

  update = (value) => {
    fetch(`http://instabytes.in/api/auth/social/${value.provider}`, {
      method: "POST",
      headers: {
        "accept": "application/json",
        "content-type": "application/json"
      },
      body: JSON.stringify({
        access_token: value.access_token
      })
    }).then((res) => res.json())
      .then((json) => {
        this.setState({ fbLogin: false, googleLoading: false })
        if (json.status == "1") {
          Toast("Success")
          this.storeUser(json.data)
        } else {
          Toast(json.message)
        }
      }).catch((err) => {
        console.log(err)
        this.setState({ fbLogin: false, googleLoading: false, })
      })
  }

  storeUser = async (val) => {
    const token = JSON.stringify(val.api_token)
    const data = JSON.stringify(val)
    try {
      await AsyncStorage.setItem('token', token)
      await AsyncStorage.setItem('user', data)
      this.props.navigation.navigate('DrawerNavigation')
      updateExpoToken(val.api_token)
    } catch (err) {
      Toast("An error occured please try again")
      console.log(err)
    }
  }


  getStartedPress = () => {
    this.setState({ showgradient: !this.state.showgradient })
    this.setTime()
  }

  setTime = () => {
    this.setState({ loading: false })
    setTimeout(() => {
      this.props.navigation.navigate('Verify', { email: this.state.email })
    }, 10);
  }
  render() {
    const { name, email, password, value,
      showgradient,
      loading,
      googleLoading,
      fbLogin,
      passEyeIconVisible
    } = this.state
    return (
      <>
        <SafeAreaView style={Styles.safeViewStyle}>
          <ScrollView>
            <View style={Styles.headerContainer}>
              <View style={Styles.loginSignupContainer}>
                <Text style={[GlobalStyles.mediumText, Styles.signupText]}>{Constants.SIGNUP}</Text>
                <TouchableOpacity onPress={() => { this.props.navigation.navigate('Login') }}>
                  <Text style={[GlobalStyles.lightText, Styles.loginText]}>{Constants.LOGIN}</Text>
                </TouchableOpacity>
              </View>

              <Text style={[GlobalStyles.boldText, Styles.WelcomeText]}>{Constants.WELOCME}</Text>
              <Text style={[GlobalStyles.lightText, Styles.wlcomeDescription]}>{Constants.WELOCME_DESCRIPTION}</Text>

              {/* FullName */}
              <FloatingLabel
                value={name}
                label={'Name'}
                autoCapitalize='none'
                onChange={this.handleChange}
                onChangeText={(value) => {
                  this.setState({ name: value })
                }}
              />
              <Text style={{ color: 'red' }}>{this.state.nameError}</Text>

              {/* Email */}
              <FloatingLabel
                value={email}
                label={"Email"}
                autoCapitalize='none'
                onChange={this.handleChange}
                onChangeText={(value) => {
                  this.setState({ email: value })
                }}
              />
              <Text style={{ color: 'red' }}>{this.state.emailError}</Text>

              {/* Phone Number */}
              <View style={Styles.phoneWrapper}>
                <PhoneInput
                  ref={this.phoneInput}
                  defaultValue={value}
                  defaultCode="IN"
                  layout="second"
                  keyboardType='numeric'
                  onChange={this.handleChange}
                  onChangeText={(text) => {
                    this.setState({
                      value: text,

                      // phoneFocus: true
                    });
                  }}

                  onChangeFormattedText={(text) => {
                    this.setState({ formattedValue: text });
                  }}
                  textContainerStyle={{ backgroundColor: 'transparent', }}
                  textInputStyle={[GlobalStyles.lightText, { width: '70%', backgroundColor: 'transparent', borderColor: "transparent" , height:45, marginLeft:-10}]}
                  containerStyle={{ width: '90%', backgroundColor: 'transparent', borderColor: "transparent" }}
                />
              </View>
              <Text style={{ color: 'red' }}>{this.state.valueError}</Text>

              {/* Password */}
              <FloatingLabel
                value={password}
                label={"Password"}
                eyeIcon={true}
                onEyePress={()=> this.setState({ passEyeIconVisible: !passEyeIconVisible})}
                secureTextEntry={passEyeIconVisible}
                autoCapitalize='none'
                onChange={this.handleChange}
                onChangeText={(value) => {
                  this.setState({ password: value })
                }}
              />
              <Text style={{ color: 'red' }}>{this.state.passError}</Text>

              {
                showgradient ?
                  <TouchableOpacity
                  // onPress={() => { this.props.navigation.navigate("Login") }}
                  >
                    <View
                      style={Styles.buttonContainerGradient}
                    >
                      <Text style={[GlobalStyles.boldText, Styles.buttonText]}>{Constants.SIGNUP}</Text>
                    </View>
                  </TouchableOpacity>
                  :
                  <TouchableOpacity disabled={loading} style={Styles.buttonContainer}
                    onPress={this.handleSubmit}
                  >
                    {loading ? <ActivityIndicator color='white' size='small' /> : <Text style={[GlobalStyles.boldText, Styles.buttonText]}>{Constants.SIGNUP}</Text>}
                  </TouchableOpacity>
              }

              <View style={Styles.seperatorContainer}>
                <View style={Styles.seperatorWrapper} />
                <Text style={[GlobalStyles.lightText, Styles.textContainer]}>{Constants.OR}</Text>
                <View style={Styles.textwrapper} />
              </View>

              <Text style={[GlobalStyles.lightText, Styles.loginText1]}>{'Signup with'}</Text>

              <View style={Styles.socialContainer}>

                {/* Google */}
                <TouchableOpacity disabled={googleLoading} onPress={this.handleGoogleLogin} style={Styles.googleButton} >
                {googleLoading ? <ActivityIndicator color={colors.main} size='small'/> : <View style={Styles.innerContainer}>
                    <Image source={Images.Google} style={Styles.socialImage} />
                    <Text style={[GlobalStyles.boldText, Styles.socialText1]}>{Constants.GOOGLE}</Text>
                  </View> }
                </TouchableOpacity>
                {/* FaceBook */}
                <TouchableOpacity disabled={fbLogin} onPress={this.handleFacebookLogin} style={Styles.facebookButton}>
                {fbLogin ? <ActivityIndicator color='white' size='small' /> :
                  <View style={Styles.innerContainer}>
                    <Image source={Images.Facebook} style={Styles.socialImage} />
                    <Text style={[GlobalStyles.boldText, Styles.socialText]}>{Constants.FACEBOOK}</Text>
                  </View>}
                </TouchableOpacity>
              </View>

              <View style={Styles.bottomWrapper}>
                <Text style={[GlobalStyles.lightText, Styles.alreadyAccountText]}>{Constants.HAVE_ACCOUNT}</Text>
                <TouchableOpacity onPress={() => { this.props.navigation.navigate('Login') }}>
                  <Text style={[GlobalStyles.lightText, Styles.loginTextBottom]}>{Constants.LOGIN}</Text>
                </TouchableOpacity>
              </View>

            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}
export default connect()(Signup);
