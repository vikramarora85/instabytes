import React, { Component } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Image
} from 'react-native';
import Styles from './Styles';
import Images from '../../Styles/Images';
import GlobalStyles from '../../Styles/globalStyles';
import PhoneInput from "react-native-phone-number-input";
import * as Constants from '../../AllConstants';
import FloatingLabel from "../../Components/FloatingLabel";
import axios from '../../Components/config/axios'

class Signup extends Component {
  constructor(props) {
    super(props);
    this.phoneInput = React.createRef();
    this.state = {
      name: '',
      nameError:'',
      email: '',
      emailError:'',
      password: '',
      passError:'',
      value: '',
      valueError:'',
      showgradient: false,
      phoneFocus: false,
      passwordfocus: false,
      validated:false
    };
  }
  handleSubmit=async()=>{
    try{

    
    const formData={
      name:this.state.name,
      email:this.state.email,
      password:this.state.password,
      phone:this.state.value,
      country:'India'
    }
    console.log(formData)
  
  if(this.state.name==='' ){
      this.setState({nameError:"Field Required"})
  }else{
      this.setState({nameError:" "})     
  }



  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  let isValid = reg.test(this.state.email);
  if(!isValid ) {
      this.setState({ emailError: "Not a valid email"})
   }else {
      this.setState({ emailError: ""})
   }


   const regPass=/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/
   let isValidPass=regPass.test(this.state.password)
   if(!isValidPass){
    this.setState({passError:"Password should contain one  uppercase, lowercase, number and symbol"})
    }else{
        this.setState({passError:''})
    }
    if(this.state.value==='' ){
      this.setState({valueError:"Field Required"})
  }else{
      this.setState({valueError:" "})     
  }
  const result=await axios.post('/auth/register',formData)
  if(result.data.data.status!==0){
    if(this.state.name!=='' && isValid && isValidPass && this.state.value!=='' ){

      this.getStartedPress()
    }

  }
  

}
catch(error){
  console.log(error)
  
}

    

  }


  getStartedPress = () => {
    this.setState({ showgradient: !this.state.showgradient })
    this.setTime()
  }
  // focusPhone = () => {
  //   this.setState({
  //     phoneFocus: false,
  //     passwordfocus: !this.state.passwordfocus
  //   })
  // }
  setTime = () => {
    setTimeout(() => {
      this.props.navigation.navigate('Login')
    }, 10);
  }
  render() {
    const { name, email, password, value,
      showgradient,
      phoneFocus,
    } = this.state
    return (
      <>
        <SafeAreaView style={Styles.safeViewStyle}>
          <ScrollView>
            <View style={Styles.headerContainer}>
              <View style={Styles.loginSignupContainer}>
                <Text style={[GlobalStyles.mediumText, Styles.signupText]}>{Constants.SIGNUP}</Text>
                <TouchableOpacity onPress={() => { this.props.navigation.navigate('Login') }}>
                  <Text style={[GlobalStyles.lightText, Styles.loginText]}>{Constants.LOGIN}</Text>
                </TouchableOpacity>
              </View>

              <Text style={[GlobalStyles.boldText, Styles.WelcomeText]}>{Constants.WELOCME}</Text>
              <Text style={[GlobalStyles.lightText, Styles.wlcomeDescription]}>{Constants.WELOCME_DESCRIPTION}</Text>

              {/* FullName */}
              <FloatingLabel
                value={name}
                label={'Name'}
                autoCapitalize='none'
                onChange={this.handleChange}
                onChangeText={(value) => {
                  this.setState({ name: value })
                }}
              />
             <Text style={{color: 'red'}}>{this.state.nameError}</Text>

              {/* Email */}
              <FloatingLabel
                value={email}
                label={"Email"}
                autoCapitalize='none'
                onChange={this.handleChange}
                onChangeText={(value) => {
                  this.setState({ email: value })
                }}
              />
              <Text style={{color: 'red'}}>{this.state.emailError}</Text>

              {/* Phone Number */}
              <View style={Styles.phoneWrapper}>
                <PhoneInput
                  ref={this.phoneInput}
                  defaultValue={value}
                  defaultCode="IN"
                  layout="second"
                  keyboardType='numeric'
                  onChange={this.handleChange}
                  onChangeText={(text) => {
                    this.setState({
                      value: text,

                      // phoneFocus: true
                    });
                  }}

                  onChangeFormattedText={(text) => {
                    this.setState({ formattedValue: text });
                  }}
                  textContainerStyle={{ backgroundColor: 'transparent', }}
                  textInputStyle={[GlobalStyles.lightText, { width: '70%', backgroundColor: 'transparent', borderColor: "transparent" }]}
                  containerStyle={{ width: '90%', backgroundColor: 'transparent', borderColor: "transparent" }}
                />
              </View>
              <Text style={{color: 'red'}}>{this.state.valueError}</Text>

              {/* Password */}
              <FloatingLabel
                value={password}
                label={"Password"}
                secureTextEntry={true}
                autoCapitalize='none'
                onChange={this.handleChange}
                onChangeText={(value) => {
                  this.setState({ password: value })
                }}
              />
              <Text style={{color: 'red'}}>{this.state.passError}</Text>

              {
                showgradient ?
                  <TouchableOpacity
                  // onPress={() => { this.props.navigation.navigate("Login") }}
                  >
                    <View
                      style={Styles.buttonContainerGradient}
                    >
                      <Text style={[GlobalStyles.boldText, Styles.buttonText]}>{Constants.SIGNUP}</Text>
                    </View>
                  </TouchableOpacity>
                  :
                  <TouchableOpacity style={Styles.buttonContainer}
                    onPress={this.handleSubmit}
                  >
                    <Text style={[GlobalStyles.boldText, Styles.buttonText]}>{Constants.SIGNUP}</Text>
                  </TouchableOpacity>
              }

              <View style={Styles.seperatorContainer}>
                <View style={Styles.seperatorWrapper} />
                <Text style={[GlobalStyles.lightText, Styles.textContainer]}>{Constants.OR}</Text>
                <View style={Styles.textwrapper} />
              </View>

              <Text style={[GlobalStyles.lightText, Styles.loginText1]}>{'Signup with'}</Text>

              <View style={Styles.socialContainer}>

                {/* Google */}
                <TouchableOpacity style={Styles.googleButton}>
                  <View style={Styles.innerContainer}>
                    <Image source={Images.Google} style={Styles.socialImage} />
                    <Text style={[GlobalStyles.boldText, Styles.socialText1]}>{Constants.GOOGLE}</Text>
                  </View>
                </TouchableOpacity>
                {/* FaceBook */}
                <TouchableOpacity style={Styles.facebookButton}>
                  <View style={Styles.innerContainer}>
                    <Image source={Images.Facebook} style={Styles.socialImage} />
                    <Text style={[GlobalStyles.boldText, Styles.socialText]}>{Constants.FACEBOOK}</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={Styles.bottomWrapper}>
                <Text style={[GlobalStyles.lightText, Styles.alreadyAccountText]}>{Constants.HAVE_ACCOUNT}</Text>
                <TouchableOpacity onPress={() => { this.props.navigation.navigate('Login') }}>
                  <Text style={[GlobalStyles.lightText, Styles.loginTextBottom]}>{Constants.LOGIN}</Text>
                </TouchableOpacity>
              </View>

            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}
export default Signup;
