import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
  ImageBackground,
  ActivityIndicator,
} from "react-native";
import Styles from "./Styles";
import Images from "../../Styles/Images";
import Colors from "../../Styles/Colors";
import GlobalStyles from "../../Styles/globalStyles";
import Header from "../../Components/Header";
import SegmentedControlTab from "react-native-segmented-control-tab";
import PremiumLearning from "./paidResource";
import PremiumJob from "./paidJob";
import { connect } from "react-redux";
import { setProfile, startFetchData} from "../../actions/userAction";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { IMAGE_BASE_URL } from "../../AllConstants";
import colors from "../../Styles/Colors";

class LearningCategories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      loader:true
    };
  }

  handlePress = async (value) => {
    console.log(value, 'hh')
    try{
      await AsyncStorage.setItem("categoryId", JSON.stringify(value.id));
    } catch(e){
      console.log(e)
    }
    if (this.state.selectedIndex === 0) {
      this.props.navigation.navigate("ResourceSubCategory", {
        index: this.state.selectedIndex,
        data:value,
      });
    } else {
      this.props.navigation.navigate("JobSubCategory", {
        index: this.state.selectedIndex,
        data:value,
      });
    }
  };

  async componentDidMount() {
    await this.props.dispatch(startFetchData());
    this.setState({loader:false})
    this.fetchProfile()
  }

  fetchProfile = async () => {
   const token = await AsyncStorage.getItem("token")
   fetch('http://instabytes.in/api/profile',{
     method:"GET",
     headers:{
       "accept":"application/json",
       "content-type":"application/json",
       "Authorization":`Bearer ${JSON.parse(token)}`
     }
   }).then((res)=> res.json())
   .then((json)=> {
     if(json.status == "1"){
       this.storeData(json.data)
     } else {
       if(json.message == "Unauthenticated."){
        AsyncStorage.clear();
        AsyncStorage.setItem('getStarted', 'done')
        this.props.navigation.navigate("Login")
       }
     }
   }).catch((err)=>{
     console.log(err)
   })

  }

  storeData = async (val) =>{

    this.props.setProfile(val)
    const data = JSON.stringify(val)

    try {
      await AsyncStorage.setItem('user', data)
    } catch (err) {
      console.log(err)
    }
  }

  handleIndexChange = (index) => {
    this.setState({ selectedIndex: index });
  }

  showProfileWrapper = (value) => {

    return (
      <>
        <ImageBackground
          key={value.id}
          imageStyle={Styles.imageBorderRadius}
          source={{
            uri: IMAGE_BASE_URL + value.category_image,
          }}
          style={Styles.headerContentInner}
        >
          <TouchableOpacity
            style={Styles.centerContent}
            onPress={() => this.handlePress(value)}
          >
            <View style={Styles.centerContent}>
              <Text style={[(GlobalStyles.lightText, Styles.textCategory1)]}>
                {value.name}
              </Text>
            </View>
          </TouchableOpacity>
        </ImageBackground>
      </>
    );
  };

  render() {
    const selectedIndex = this.state.selectedIndex;
    return (
      <>

        <View style={Styles.safeViewStyle}>
          <Header
            navigation={this.props.navigation}
            headerName={"Categories"}
            isSearch={false}
          />
          {
      this.state.loader ?
        <ActivityIndicator color={Colors.appHeaderColor} style={Styles.activityIndicator}/>:
          <View>
          <View style={Styles.segmentMainContainer}>
            <View style={Styles.segmentWrapper}>
              {selectedIndex === 1 ? (
                <SegmentedControlTab
                  values={["Learning Resources", "Job Updates"]}
                  selectedIndex={this.state.selectedIndex}
                  onTabPress={this.handleIndexChange}
                  borderWidth={0}
                  borderLeftWidth={0}
                  borderLeftColor={Colors.White}
                  borderRightColor={Colors.White}
                  activeTabStyle={{
                    backgroundColor: "transparent",
                    borderBottomWidth: 4,
                    borderWidth: 0,
                    borderRightColor: Colors.White,
                    borderRightWidth: 0,
                    borderLeftColor: Colors.White,
                    borderBottomColor: colors.main,
                    borderColor: Colors.White,
                  }}
                  activeTabTextStyle={{
                    color: Colors.appHeaderColor,
                    fontSize: 17,
                    fontWeight: "600",
                    marginVertical: 5,
                    fontFamily: GlobalStyles.mediumText.fontFamily,
                  }}
                  tabStyle={{
                    backgroundColor: "transparent",
                    borderBottomColor: "#454647",
                    borderBottomWidth: 1,
                    borderRightColor: Colors.White,
                    borderLeftColor: Colors.White,
                    borderLeftWidth: 0,
                    borderWidth: 0,
                    borderColor: Colors.White,
                  }}
                  tabTextStyle={{
                    color: Colors.ok,
                    fontSize: 17,
                    fontWeight: "600",
                    marginVertical: 5,
                    fontFamily: GlobalStyles.mediumText.fontFamily,
                  }}
                />
              ) : (
                <SegmentedControlTab
                  values={["Learning Resources", "Job Updates"]}
                  selectedIndex={this.state.selectedIndex}
                  onTabPress={this.handleIndexChange}
                  borderWidth={0}
                  borderLeftWidth={0}
                  borderLeftColor={Colors.White}
                  borderRightColor={Colors.White}
                  activeTabStyle={{
                    backgroundColor: "transparent",
                    borderBottomWidth: 4,
                    borderWidth: 0,
                    borderRightColor: Colors.White,
                    borderRightWidth: 0,
                    borderLeftColor: Colors.White,
                    borderBottomColor: colors.main,
                    borderColor: Colors.White,
                  }}
                  activeTabTextStyle={{
                    color: Colors.appHeaderColor,
                    fontSize: 18,
                    fontWeight: "600",
                    marginVertical: 5,
                    fontFamily: GlobalStyles.mediumText.fontFamily,
                  }}
                  tabStyle={{
                    backgroundColor: "transparent",
                    borderBottomColor: "#454647",
                    borderBottomWidth: 1,
                    borderRightColor: Colors.White,
                    borderLeftColor: Colors.White,
                    borderLeftWidth: 0,
                    borderWidth: 0,
                    borderColor: Colors.White,
                  }}
                  tabTextStyle={{
                    color: Colors.ok,
                    fontSize: 16,
                    fontWeight: "600",
                    marginVertical: 5,
                    fontFamily: GlobalStyles.mediumText.fontFamily,
                  }}
                />
              )}
            </View>
          </View>


          {/* Ends */}
          {this.props.hasOwnProperty("user") && (
            <ScrollView>
              {selectedIndex === 0 ? (
                <View style={Styles.headerWrapper}>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                    }}
                  >
                    <Text
                      style={[GlobalStyles.boldText, Styles.mainHeaderName]}
                    >
                      {"Free Resources"}
                    </Text>

                    <TouchableOpacity
                      onPress={() => {
                        this.props.navigation.navigate("FreeResource");
                      }}
                    >
                      <Text style={[GlobalStyles.lightText, Styles.seeAllText]}>
                        {"See All"}
                      </Text>
                    </TouchableOpacity>
                  </View>

                  {/* free learning */}
                  <View style={Styles.headerContentWrapper}>
                    <View style={Styles.headerContent}>
                      <FlatList
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                        data={this.props.user.freeResource}
                        keyExtractor={(item) => item.id.toString()}
                        renderItem={({ item }) => this.showProfileWrapper(item)}
                      />
                    </View>
                  </View>
                </View>
              ) : (
                <View style={Styles.headerWrapper}>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                    }}
                  >
                    <Text
                      style={[GlobalStyles.boldText, Styles.mainHeaderName]}
                    >
                      {"Free Jobs"}
                    </Text>

                    <TouchableOpacity
                      onPress={() => {
                        this.props.navigation.navigate("FreeJobs");
                      }}
                    >
                      <Text style={[GlobalStyles.lightText, Styles.seeAllText]}>
                        {"See All"}
                      </Text>
                    </TouchableOpacity>
                  </View>

                  {/* free job */}
                  <View style={Styles.headerContentWrapper}>
                    <View style={Styles.headerContent}>
                      <FlatList
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                        keyExtractor={(item) => item.id.toString()}
                        data={this.props.user.jobs}
                        renderItem={({ item }) => this.showProfileWrapper(item)}
                      />
                    </View>
                  </View>
                </View>
              )}
              {selectedIndex === 0 ? (
                <View style={Styles.headerWrapperPaid}>
                  <Text style={[GlobalStyles.boldText, Styles.mainHeaderName]}>
                    {"Premium Learning's"}
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("PaidResource");
                    }}
                  >
                    <Text style={[GlobalStyles.lightText, Styles.seeAllText]}>
                      {"See All"}
                    </Text>
                  </TouchableOpacity>
                </View>
              ) : (
                <View style={Styles.headerWrapperPaid}>
                  <Text style={[GlobalStyles.boldText, Styles.mainHeaderName]}>
                    {"Premium Jobs"}
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("PaidJobs");
                    }}
                  >
                    <Text style={[GlobalStyles.lightText, Styles.seeAllText]}>
                      {"See All"}
                    </Text>
                  </TouchableOpacity>
                </View>
              )}

              <View style={Styles.headerContentWrapper1}>
                {selectedIndex === 0 ? (
                  <PremiumLearning navigation={this.props.navigation} data={this.props} />
                ) : (
                  <PremiumJob navigation={this.props.navigation} data={this.props} />
                )}
              </View>
            </ScrollView>
          )}
          </View>
      }
        </View>

      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};

const mapDispatchToProps = (dispatch) => ({
  setProfile: (data) => dispatch(setProfile(data)),
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(LearningCategories);
