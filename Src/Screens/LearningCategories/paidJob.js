import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  Modal
} from "react-native";
import Styles from "./Styles";
import Images from "../../Styles/Images";
import GlobalStyles from "../../Styles/globalStyles";
import { connect } from "react-redux";
import { IMAGE_BASE_URL } from "../../AllConstants";
import Subscription from "../SubscriptionModal";
import AsyncStorage from "@react-native-async-storage/async-storage";

class PremiumJob extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal_visible:false,
      visible_data:{},
      user:{}
    };
  }

  async componentDidMount(){
    const user = await AsyncStorage.getItem('user');
    this.setState({
      user:JSON.parse(user)
    })

  }

  handelPress = (value) =>{
    if(this.state.user.is_subscribed == "0"){
      this.setState({modal_visible:true, visible_data:value})
    } else {
      this.props.navigation.navigate("JobSubCategory", {
        index: 1,
        data:value
      })
    }
  }

  setModalJobFalse = () =>{
    this.setState({
      modal_visible:false,
      visible_data:{}
    })
  }

  navigate = (val) =>{
    this.setState({
      modal_visible:false,
      visible_data:{}
    })
    this.props.navigation.navigate(val, {data:{type: 'subscription'}})
  }

  render() {
    // console.log(this.props)
    const { paidjobs } = this.props.user;
    return (
      <>
        <View style={Styles.headerContentWrapper1}>
          {paidjobs.length > 0 &&
            paidjobs.map((value) => {
              return (
                <ImageBackground
                  key={value.id}
                  source={{
                    uri: IMAGE_BASE_URL + value.category_image,
                  }}
                  imageStyle={Styles.imageBorderRadius}
                  style={Styles.headerPremiumContentInner}
                >
                  <TouchableOpacity
                    onPress={() => {
                      this.handelPress()
                    }}
                  >
                    <View style={Styles.innerPremium}>
                      <Image
                        source={Images.premium_quality}
                        style={Styles.playIconStyle2}
                      />

                      <TouchableOpacity
                        onPress={() => {
                          this.handelPress(value)
                        }}
                      >
                        <Image
                          source={this.state.user.is_subscribed == "0" ? Images.lockPremium : Images.playIcon}
                          style={Styles.playIconStyle1}
                        />
                      </TouchableOpacity>
                      <Text
                        style={[GlobalStyles.lightText, Styles.textCategory]}
                      >
                        {value.name}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </ImageBackground>
              );
            })
          }
        </View>
        <Modal onRequestClose={()=> this.setModalJobFalse} transparent animationType='fade' visible={this.state.modal_visible}>
           <View style={{height:'100%', width:"100%", backgroundColor:'rgba(0,0,0,0.5)',}}>
              <Subscription navigate={this.navigate} onCancel={this.setModalJobFalse} data={this.state.visible_data}/>
           </View>
        </Modal>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps)(PremiumJob);
