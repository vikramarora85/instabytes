import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  Modal
} from "react-native";
import Styles from "./Styles";
import Images from "../../Styles/Images";
import GlobalStyles from "../../Styles/globalStyles";
import { connect } from "react-redux";
import { IMAGE_BASE_URL } from "../../AllConstants";
import BuyCourse from "../BuyCourse";
import AsyncStorage from "@react-native-async-storage/async-storage";

class PremiumLearning extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal_visible:false,
      visible_data:{}
    };
  }

  setModalFalse = () =>{
    this.setState({
      modal_visible:false,
      visible_data:{}
    })
  }

  handelPress = async (val) =>{
    try{
      await AsyncStorage.setItem("categoryId", JSON.stringify(val.id));
    } catch(e){
      console.log(e)
    }
    if(!val.isPurchased){
      this.setState({visible_data:val, modal_visible:true, })
    } else {
      this.props.navigation.navigate("ResourceSubCategory", {
        index: 0,
        data:val
      })
    }
  }

  render() {
    // console.log(this.props)
    const { paidResource } = this.props.user;
    return (
      <>
        <View style={Styles.headerContentWrapper1}>
          {paidResource?.length > 0 &&
            paidResource.map((value) => {

              return (
                <ImageBackground
                  key={value.id}
                  source={{
                    uri: IMAGE_BASE_URL + value.category_image,
                  }}
                  imageStyle={Styles.imageBorderRadius}
                  style={Styles.headerPremiumContentInner}
                >
                  <View style={Styles.innerPremium}>
                    <TouchableOpacity
                      onPress={() => {
                        this.handelPress(value)
                      }}
                    >
                      <Image
                        source={Images.premium_quality}
                        style={Styles.playIconStyle2}
                      />

                      {!value.isPurchased? <TouchableOpacity
                        onPress={() => {
                          this.setState({visible_data:value, modal_visible:true})
                        }}
                      >
                        <Image
                          source={Images.lockPremium}
                          style={Styles.playIconStyle1}
                        />
                      </TouchableOpacity> : null}
                      <Text
                        style={[GlobalStyles.lightText, Styles.textCategory]}
                      >
                        {value.name}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </ImageBackground>
              );
            })}
        </View>
        <Modal onRequestClose={()=> this.setModalFalse} transparent animationType='fade' visible={this.state.modal_visible}>
           <View style={{height:'100%', width:"100%", backgroundColor:'rgba(0,0,0,0.5)',}}>
              <BuyCourse navigation={this.props.navigation} onCancel={this.setModalFalse} data={this.state.visible_data}/>
           </View>
        </Modal>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps)(PremiumLearning);
