import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    Image,
    FlatList,
    ImageBackground,
    ActivityIndicator,
} from "react-native";
import Styles from "./Styles";
import Images from "../../Styles/Images";
import Colors from "../../Styles/Colors";
import GlobalStyles from "../../Styles/globalStyles";
import Header from "../../Components/Header";
import SegmentedControlTab from "react-native-segmented-control-tab";
import { connect } from "react-redux";
import { startFetchData } from "../../actions/userAction";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { IMAGE_BASE_URL } from "../../AllConstants";
import BackHeader from "../../Components/BackHeader";

class PurchasedCat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: 0,
            loader: true,
            resources: [],
            jobs: []
        };
    }

    handlePress = (val) => {

        this.props.navigation.navigate("ResourceSubCategory", {
            index: 0,
            data: val
        })

    }

    componentDidMount() {
        this.fetchResources()
    }

    handleIndexChange = (index) => {
        this.setState({ selectedIndex: index });
    };


    fetchResources = async () => {

        const token = await AsyncStorage.getItem('token')
        fetch('http://instabytes.in/api/category-purchased', {
            method: "GET",
            headers: {
                "accept": "application/json",
                "content-type": "application/json",
                "Authorization": `Bearer ${JSON.parse(token)}`
            }
        }).then((res) => res.json())
            .then((json) => {
                if (json.status == "1") {
                    this.setState({
                        resources: json.data,
                        loader: false
                    });
                } else {
                    this.setState({
                        loader: false
                    });
                    console.log(json.message)
                }
            })
    }


    render() {
        const { resources } = this.state
        return (
            <>

                <View style={Styles.safeViewStyle}>
                    <Header
                        navigation={this.props.navigation}
                        headerName={"Purchased Items"}
                        isSearch={true}
                        isBackBtn={true}
                        screen='LearningCategories'
                    />
                    {
                        this.state.loader ?
                            <ActivityIndicator color={Colors.appHeaderColor} style={Styles.activityIndicator} /> :
                            <View>
                                {resources.length != 0 ?
                                    <FlatList
                                        data={resources}
                                        keyExtractor={item => item.id}
                                        renderItem={({ item }) => (
                                            <ImageBackground
                                                source={{
                                                    uri: IMAGE_BASE_URL + item.category_image,
                                                }}
                                                imageStyle={Styles.imageBorderRadius}
                                                style={Styles.headerPremiumContentInner}
                                            >
                                                <TouchableOpacity onPress={() => {
                                                    this.handlePress(item)
                                                }} style={Styles.innerPremium}>
                                                    <TouchableOpacity
                                                        onPress={() => {
                                                            this.handlePress(item)
                                                        }}
                                                    >
                                                        <Text
                                                            style={[GlobalStyles.lightText, Styles.textCategory]}
                                                        >
                                                            {item.name}
                                                        </Text>
                                                    </TouchableOpacity>
                                                    <Image
                                                        source={Images.premium_quality}
                                                        style={Styles.playIconStyle2}
                                                    />
                                                </TouchableOpacity>
                                            </ImageBackground>
                                        )}
                                    /> : <Text style={{ fontSize: 18, alignSelf: "center", fontFamily: "DMSans_700Bold", color: 'black', marginTop: '80%' }}>There are no premium resources.</Text>}
                            </View>
                    }
                </View>

            </>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        user: state.user,
    };
};

/*const mapDispatchToProps = (dispatch) => ({
  changeSignupStatus: () => dispatch(startFetchData()),
});*/
export default connect(mapStateToProps)(PurchasedCat);
