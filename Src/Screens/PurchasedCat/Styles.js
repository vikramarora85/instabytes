import { StyleSheet, Dimensions, Platform } from "react-native";
import Colors from "../../Styles/Colors";
const widthScreen = Dimensions.get("window").width;
const styles = StyleSheet.create({
  safeViewStyle1: {
    backgroundColor: Colors.appHeaderColor,
    height:'100%',
    width:'100%'
  },
  safeViewStyle: {
    flex: 1,
    backgroundColor: Colors.White,
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
 },
  segmentMainContainer: {
    // paddingVertical: 30,
    justifyContent: "center",
    alignItems: "center",
  },
  segmentWrapper: {
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  headerWrapper: {
    width: widthScreen / 1.1,
    alignSelf: "center",
    flexDirection: "column",
    marginTop: 10,
    justifyContent: "space-between",
  },
  headerWrapperPaid: {
    width: widthScreen / 1.1,
    alignSelf: "center",
    flexDirection: "row",
    marginTop: 10,
    justifyContent: "space-between",
  },
  mainHeaderName: {
    fontSize: 18,
    color: Colors.ok,
  },
  seeAllText: {
    fontSize: 14,
    fontWeight: "400",
    color: Colors.ok,
    marginTop: 5,
  },
  headerContentWrapper: {
    marginTop: 10,
  },
  headerContentWrapper1: {
    marginBottom: 5,
  },
  headerContent: {
    flexDirection: "row",
  },
  headerContentInner: {
    width: 130,
    height: 180,
    borderRadius: 15,
    justifyContent: "center",
    alignItems: "center",
    marginRight: 15,
    marginBottom: "10%",
  },
  centerContent: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  playIconStyle: {
    width: 30,
    height: 30,
  },
  playIconStyle1: {
    alignSelf: "center",
    width: 45,
    height: 45,
  },
  playIconStyle2: {
    width: 30,
    height: 30,
    position:"absolute", 
    right:10,
     top:10
  },
  innerPremium: {
    alignSelf: "center",
    backgroundColor:"rgba(0, 0, 0, 0.3)",
    height:"100%",
    width:"100%",
    borderRadius:15,
    alignItems:"center",
    justifyContent:'center'
  },
  headerPremiumContentInner: {
    width: widthScreen / 1.1,
    height: 150,
    alignSelf: "center",
    marginTop: 20,
  },
  textCategory: {
    fontSize: 19,
    fontWeight: "500",
    textAlign: "center",
    color: Colors.White,
  },
  textCategory1: {
    fontSize: 16,
    textAlign: "center",
    color: Colors.White,
    position: "absolute",
    bottom: 0,
    marginBottom: 10,
  },
  imageBorderRadius: {
    borderRadius: 15,
  },
});
export default styles;
