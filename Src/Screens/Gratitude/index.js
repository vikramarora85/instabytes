import React, { Component } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Image
} from 'react-native';
import Styles from './Styles'
import Images from '../../Styles/Images';
import Colors from '../../Styles/Colors';
import * as Constants from '../../AllConstants';
import GlobalStyles from "../../Styles/globalStyles";

class Gratitude extends Component {
  constructor(props) {
    super(props);
  }
  getStartedPress = () => {
    this.setTime()
  }
  setTime = () => {
    setTimeout(() => {
      this.props.navigation.navigate('LearningCategories')
    }, 50);
  }

  render() {
    return (
      <>
        <SafeAreaView style={Styles.safeViewStyle}>
          <View style={Styles.mainWrapper}>

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PaymentForm');
              }}
            >
              <Image source={Images.back} style={Styles.menuStyle} />
            </TouchableOpacity>
            {/*
                  onPress={() => { this.props.navigation.navigate("LearningCategories") }}

          */}


          </View>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <Text style={[Styles.gratitudeText, GlobalStyles.mediumText]}>{'Thankyou for Your Subscription Enjoy Your Benefits'}</Text>
          </View>
          {
            <TouchableOpacity style={[GlobalStyles.mediumText, Styles.buttonContainer]}
                              onPress={this.getStartedPress}
            >
              <Text style={Styles.buttonText}>{'Go to home screen'}</Text>
            </TouchableOpacity>
          }


        </SafeAreaView>
      </>
    );
  }
}
export default Gratitude;
