import React, { Component, createRef } from "react";
import { View, Image, TouchableOpacity, Text, ActivityIndicator, SafeAreaView, FlatList, Dimensions, Platform, Share } from "react-native";
import Styles from "./Styles";
import BackHeader from "../../Components/BackHeader";
import Colors from "../../Styles/Colors";
import MaterialCommunityIcons from '@expo/vector-icons/MaterialCommunityIcons'
import { Video } from "expo-av";
import Images from "../../Styles/Images";
import Swiper from "react-native-swiper/src";
import { connect } from "react-redux";
import { startGetSlide, startAddFavSlider, startDeleteFavSlider } from "../../actions/userAction";
import { IMAGE_BASE_URL } from "../../AllConstants";
import GlobalStyles from "../../Styles/globalStyles";
import AsyncStorage from "@react-native-async-storage/async-storage";
import InViewPort from "@coffeebeanslabs/react-native-inviewport";
import colors from "../../Styles/Colors";
import Toast from "../../Components/Toast";
import * as FileSystem from 'expo-file-system';
import * as Sharing from 'expo-sharing'; 


const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const images = ["jpg", "gif", "png", "PNG", "jpeg", "JPEG", "JPG", "webp", "WEBP", "webP"];
const videos = ["mp4", "3gp", "ogg"];

class ResourceSlider extends Component {
  constructor(props) {
    super(props);
    this.videoRef = null;
    this.state = {
      selectedIndex: null,
      favorite: null,
      visibleIndex: null,
      status: null,
      loader: true
    };
  }

  videoRefs = [];

  setRef = (ref) => {
    this.videoRefs.push(ref);
  };

  onSharePress = async (item) => {
    if (!item.slide) {
      return;
    }

    try {
      const { uri } = await FileSystem.downloadAsync(IMAGE_BASE_URL + item.slide, `${FileSystem.cacheDirectory}${new Date().toString()}.${item.file_type}`)
      await Sharing.shareAsync(uri)
    } catch (e) {
      console.log(e)
    }
  }


  async componentDidMount() {
    await this.props.dispatch(startGetSlide(this.props.navigation.getParam('data').id));
    /*console.log(this.props.navigation.state.params.subcategory_id);*/
    this.setState({ loader: false })
  }


  handleIndexChange = async (index, value) => {
    const userId = await AsyncStorage.getItem("userId");
    const categoryId1 = await AsyncStorage.getItem("categoryId");
    const categoryId = JSON.parse(categoryId1);
    const subcategoryId = this.props.navigation.state.params.subcategory_id
    const formData = {
      post_id: value.id,
      user_id: userId,
      subcategory_id: subcategoryId,
      category_id: categoryId
    }
    const postId = {
      category_id: categoryId,
      subcategory_id: subcategoryId,
      post_id: value.id
    }

    let allData = this.props.user.resourceSlider;
    allData[index].favorite = !allData[index].favorite;

    if (value.favorite) {
      this.setState({ favorite: true });
      this.props.dispatch(startAddFavSlider(formData));
    } else {
      this.setState({ favorite: false });
      this.props.dispatch(startDeleteFavSlider(postId));
    }


  };


  handlePageChange = (page) => {
    this.setState({
      selectedIndex: page,
    });
  };

  renderPagination = (index, total) => {
    return (
      <View style={Styles.paginationStyle}>
        <Text style={{ color: "grey" }}>
          <Text style={Styles.paginationText}>{index + 1}</Text>/{total}
        </Text>
      </View>
    );
  };

  onViewableItemsChanged = props => {
    const changed = props.changed;
    changed.forEach(item => {
      console.log(item.item);
      const video = this.videoRefs[item.index];
      if (video) {
        if (!item.isViewable && videos.includes(item.item.file_type)) {
          video.pauseAsync();
        }
      }
    })
    /*console.log(this.videoRefs[firstViewableItem]);
    if(this.videoRefs[firstViewableItem - 1]){
      this.videoRefs[firstViewableItem].pauseAsync()
    }*/
  };

  render() {
    const { resourceSlider } = this.props.user;

    return (
      <>
        <View style={{ height: '100%', width: "100%" }}>
          <BackHeader
            navigation={this.props.navigation}
            headerName={this.props?.navigation?.getParam('data').subcategory_name}
            screen={"ResourceSubCategory"}
            onSharePress={this.onSharePress}
            nextShow={false}
          />
          {
            this.state.loader ? <View style={{ height: "90%", width: "100%", alignItems: "center", justifyContent: "center" }}><ActivityIndicator color={Colors.appHeaderColor} /></View> :

              resourceSlider?.length != 0 ?
                <FlatList
                  data={resourceSlider}
                  horizontal
                  viewabilityConfig={{
                    viewAreaCoveragePercentThreshold: 50
                  }}
                  onViewableItemsChanged={this.onViewableItemsChanged}
                  pagingEnabled
                  keyExtractor={item => item?.id?.toString()}
                  renderItem={({ item, index }) => {
                    return (
                      <View style={{ height: "100%", width: width }}>
                        {
                          images.includes(item.file_type) ?
                            <View style={{ height: '100%', width: '100%', }}>
                              <View style={{ position: "absolute", top: 10, right: 10, width: this.props.navigation.getParam('shareable') == true ? 150 : 110, zIndex: 5, flexDirection: 'row', alignItems: 'center' }}>
                                <TouchableOpacity
                                  onPress={() => {
                                    this.handleIndexChange(index, item);
                                  }}
                                  style={{ ...Styles.close, marginRight: 10, elevation: 5 }}
                                >
                                  <Image
                                    source={Images.heart}
                                    style={{
                                      width: 23,
                                      height: 20,
                                      tintColor: item.favorite ? "#F33636" : "#BDBDBD",
                                    }}
                                  />
                                </TouchableOpacity>
                                <TouchableOpacity
                                  onPress={() => {
                                    item.link != null ? item.link.length != 0 ? this.props.navigation.navigate('WebViewScreen', { link: item.link }) : Toast('No Link provided for this slide') : Toast('No Link provided for this slide')
                                  }}
                                  style={{ ...Styles.close, elevation: 5 }}
                                >
                                  <MaterialCommunityIcons name='web' size={25} color={colors.main} />
                                </TouchableOpacity>
                                {this.props?.navigation?.getParam('shareable') == true ? <TouchableOpacity
                                  onPress={() => {
                                    this.onSharePress(item);
                                  }}
                                  style={{ ...Styles.close, marginRight: 10, elevation: 5 }}
                                >
                                  <Image
                                    source={Images.share}
                                    style={{
                                      width: 23,
                                      height: 20,
                                      tintColor: "grey"
                                    }}
                                  />
                                </TouchableOpacity> : null}
                              </View>
                              <Image
                                ref={this.setRef}
                                resizeMode="cover"
                                style={{ height: "100%", width: "100%" }}
                                source={{ uri: IMAGE_BASE_URL + item.slide }} />
                              <View style={[GlobalStyles.mediumText, Styles.paginationStyle]}>
                                <Text style={[GlobalStyles.mediumText, Styles.paginationText]}>
                                  {index + 1}/{resourceSlider.length}
                                </Text>
                              </View>
                            </View> :
                            <View style={{ height: '100%', width: '100%', }}>
                              {videos.includes(item.file_type) ?
                                <View style={{ height: '100%', width: '100%', }}>
                                  <View style={{ position: "absolute", top: 10, right: 10, width: this.props.navigation.getParam('shareable') == true ? 150 : 110, zIndex: 5, flexDirection: 'row', alignItems: 'center' }}>
                                    <TouchableOpacity
                                      onPress={() => {
                                        this.handleIndexChange(index, item);
                                      }}
                                      style={{ ...Styles.close, marginRight: 10 }}
                                    >
                                      <Image
                                        source={Images.heart}
                                        style={{
                                          width: 23,
                                          height: 20,
                                          tintColor: item.favorite ? "#F33636" : "#BDBDBD",
                                        }}
                                      />
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      onPress={() => {
                                        item.link != null ? item.link.length != 0 ? this.props.navigation.navigate('WebViewScreen', { link: item.link }) : Toast('No Link provided for this slide') : Toast('No Link provided for this slide')
                                      }}
                                      style={Styles.close}
                                    >
                                      <MaterialCommunityIcons name='web' size={25} color={colors.main} />
                                    </TouchableOpacity>
                                    {this.props?.navigation?.getParam('shareable') == true ? <TouchableOpacity
                                      onPress={() => {
                                        this.onSharePress(item);
                                      }}
                                      style={{ ...Styles.close, marginRight: 10 }}
                                    >
                                      <Image
                                        source={Images.share}
                                        style={{
                                          width: 23,
                                          height: 20,
                                          tintColor: "grey"
                                        }}
                                      />
                                    </TouchableOpacity> : null}
                                  </View>
                                  <Video
                                    ref={this.setRef}
                                    style={[
                                      Styles.video,
                                      { position: "absolute", top: 0 },
                                    ]}
                                    resizeMode={"contain"}
                                    source={{ uri: IMAGE_BASE_URL + item.slide }}
                                    useNativeControls
                                    isLooping
                                    onPlaybackStatusUpdate={(status) => {
                                      this.setState({ status: status });
                                    }}
                                  /></View> : null}
                              <View style={[GlobalStyles.mediumText, Styles.paginationStyle]}>
                                <Text style={[GlobalStyles.mediumText, Styles.paginationText]}>
                                  {index + 1}/{resourceSlider.length}
                                </Text>
                              </View>
                              {/* <View style={[GlobalStyles.mediumText, Styles.paginationStyle]}>
                              <Text style={[GlobalStyles.mediumText, Styles.paginationText]}>
                                {index + 1}/{jobSlider.length}
                              </Text>
                            </View> */}
                            </View>
                        }
                      </View>
                    )
                  }}
                /> : <View style={{ height: "90%", width: "100%", alignItems: "center", justifyContent: "center" }}><Text style={{ ...GlobalStyles.boldText, color: colors.main }}>No Data</Text></View>

          }
        </View>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps)(ResourceSlider);
