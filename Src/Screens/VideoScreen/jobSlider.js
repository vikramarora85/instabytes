import React, { Component, createRef } from "react";
import { View, Image, Text, TouchableOpacity, ActivityIndicator, FlatList, Dimensions, Platform, Share } from "react-native";
import Styles from "./Styles";
import BackHeader from "../../Components/BackHeader";
import Images from "../../Styles/Images";
import { Video } from "expo-av";
import Colors from "../../Styles/Colors";
import Swiper from "react-native-swiper/src";
import Carousel from "react-native-banner-carousel";
import { connect } from "react-redux";
import { startGetSlide, startAddFavJobSlider, startDeleteFavJobSlider } from "../../actions/userAction";
import { IMAGE_BASE_URL } from "../../AllConstants";
import GlobalStyles from "../../Styles/globalStyles";
import AsyncStorage from "@react-native-async-storage/async-storage";
import MaterialCommunityIcons from '@expo/vector-icons/MaterialCommunityIcons'
import colors from "../../Styles/Colors";
import Toast from "../../Components/Toast";
import * as FileSystem from 'expo-file-system';
import * as Sharing from 'expo-sharing'; 


const images = ["jpg", "gif", "png", "PNG", "jpeg", "JPEG", "JPG", "webp", "WEBP", "webP"];
const videos = ["mp4", "3gp", "ogg"];
const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;


class JobSlider extends Component {
  constructor(props) {
    super(props);
    this.videoRef = { }
    this.state = {
      selectedIndex: null,
      favorite: null,
      status: null,
      loader: true
    };
  }

  async componentDidMount() {
    await this.props.dispatch(startGetSlide(this.props.navigation.getParam('id')));
    this.setState({ loader: false })
  }

  handleIndexChange = async (index, value) => {
    const userId = await AsyncStorage.getItem("userId");
    const categoryId1 = await AsyncStorage.getItem("categoryId");
    const categoryId = JSON.parse(categoryId1);
    const jobsubcategoryId = this.props.navigation.state.params.job_subcategory_id

    console.log(value)
    const formData = {
      job_id: this.props.navigation?.getParam('data').job_category_id,
      job_subcategory_id: this.props.navigation?.getParam('data').id,
      post_id: value.id,
    }
    const postId = {
      job_id: categoryId,
      job_subcategory_id: value.job_subcategory_id,
      post_id: value.id
    }

    if (!value.favorite) {
      let allData = this.props.user.jobSlider;
      allData[index].favorite = !allData[index].favorite;
      this.setState({ favorite: true });
      this.props.dispatch(startAddFavJobSlider(formData));
    } else {
      let allData = this.props.user.jobSlider;
      allData[index].favorite = !allData[index].favorite;
      this.setState({ favorite: false });
      this.props.dispatch(startDeleteFavJobSlider(formData));
    }

  };

  onSharePress = async (item) => {
    if (!item.slide) {
      return;
    }

    try {
      const { uri } = await FileSystem.downloadAsync(IMAGE_BASE_URL + item.slide, `${FileSystem.cacheDirectory}${new Date().toString()}.${item.file_type}`)
      await Sharing.shareAsync(uri)
    } catch (e) {
      console.log(e)
    }
  }

  videoRefs = [];

  setRef = (ref) => {
    this.videoRefs.push(ref);
  };

  onViewableItemsChanged = props => {
    const changed = props.changed;
    changed.forEach(item => {
      console.log(item.item);
      const video = this.videoRefs[item.index];
      if (video) {
        if (!item.isViewable && videos.includes(item.item.file_type)) {
          video.pauseAsync();
        }
      }
    })
  };

  handlePageChange = (page) => {
    this.setState({
      selectedIndex: page,
    });
  };

  renderPagination = (index, total) => {
    return (
      <View style={Styles.paginationStyle}>
        <Text style={{ color: "grey" }}>
          <Text style={Styles.paginationText}>{index + 1}</Text>/{total}
        </Text>
      </View>
    );
  };

  onSharePress = async (item) => {
    if (!item.job_slides) {
      return;
    }

    try {
      const { uri } = await FileSystem.downloadAsync(IMAGE_BASE_URL + item.job_slides, `${FileSystem.cacheDirectory}${new Date().toString()}.${item.file_type}`)
      await Sharing.shareAsync(uri)
    } catch (e) {
      console.log(e)
    }
  }

  render() {

    const { jobSlider } = this.props.user;

    return (
      <>
        <View style={{ height: '100%', width: "100%" }}>
          <BackHeader
            navigation={this.props.navigation}
            headerName={this.props.navigation.getParam('data').job_subname}
            screen={"JobSubCategory"}
            onSharePress={this.onSharePress}
            nextShow={false}
          />
          {
            this.state.loader ? <View style={{ height: "90%", width: "100%", alignItems: "center", justifyContent: "center" }}><ActivityIndicator color={Colors.appHeaderColor} /></View> :

              jobSlider?.length != 0 ?
                <FlatList
                  data={jobSlider}
                  horizontal
                  viewabilityConfig={{
                    viewAreaCoveragePercentThreshold: 90
                  }}
                  onViewableItemsChanged={this.onViewableItemsChanged}
                  pagingEnabled
                  keyExtractor={item => item?.id?.toString()}
                  renderItem={({ item, index }) => {
                    return (
                      <View style={{ height: "100%", width: width }}>
                        {
                          images.includes(item.file_type) ?
                            <View style={{ height: '100%', width: '100%', }}>
                              <View style={{ position: "absolute", top: 10, right: 10, width: this.props.navigation?.getParam('shareable') == true ?  150 : 110, zIndex: 5, flexDirection: 'row', alignItems: 'center' }}>
                                <TouchableOpacity
                                  onPress={() => {
                                    this.handleIndexChange(index, item);
                                  }}
                                  style={{ ...Styles.close, marginRight: 10,  elevation:5 }}
                                >
                                  <Image
                                    source={Images.heart}
                                    style={{
                                      width: 23,
                                      height: 20,
                                      tintColor: item.favorite ? "#F33636" : "#BDBDBD",
                                    }}
                                  />
                                </TouchableOpacity>
                                <TouchableOpacity
                                  onPress={() => {
                                    item.link != null ? item.link.length != 0 ? this.props.navigation.navigate('WebViewScreen', { link: item.link }) : Toast('No Link provided for this slide') : Toast('No Link provided for this slide')
                                  }}
                                  style={Styles.close}
                                >
                                  <MaterialCommunityIcons name='web' size={25} color={colors.main} />
                                </TouchableOpacity>
                                {this.props.navigation?.getParam('shareable') == true ? <TouchableOpacity
                                  onPress={() => {
                                    this.onSharePress(item);
                                  }}
                                  style={{ ...Styles.close, marginRight: 10 }}
                                >
                                  <Image
                                    source={Images.share}
                                    style={{
                                      width: 23,
                                      height: 20,
                                      tintColor: "grey"
                                    }}
                                  />
                                </TouchableOpacity> : null}
                              </View>
                              <Image
                                ref={this.setRef}
                                resizeMode="cover"
                                style={{ height: "100%", width: "100%" }}
                                source={{ uri: IMAGE_BASE_URL + item.job_slides }} />
                              <View style={[GlobalStyles.mediumText, Styles.paginationStyle]}>
                                <Text style={[GlobalStyles.mediumText, Styles.paginationText]}>
                                  {index + 1}/{jobSlider.length}
                                </Text>
                              </View>
                            </View> :
                            <View style={{ height: '100%', width: '100%', }}>
                              {videos.includes(item.file_type) ?
                                <View style={{ height: '100%', width: '100%', }}>
                                  <View style={{ position: "absolute", top: 10, right: 10, width: this.props.navigation.getParam('shareable') == true ? 150 : 110, zIndex: 5, flexDirection: 'row', alignItems: 'center' }}>
                                    <TouchableOpacity
                                      onPress={() => {
                                        this.handleIndexChange(index, item);
                                      }}
                                      style={{ ...Styles.close, marginRight: 10 }}
                                    >
                                      <Image
                                        source={Images.heart}
                                        style={{
                                          width: 23,
                                          height: 20,
                                          tintColor: item.favorite ? "#F33636" : "#BDBDBD",
                                        }}
                                      />
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      onPress={() => {
                                        item.link != null ? item.link.length != 0 ? this.props.navigation.navigate('WebViewScreen', { link: item.link }) : Toast('No Link provided for this slide') : Toast('No Link provided for this slide')
                                      }}
                                      style={Styles.close}
                                    >
                                      <MaterialCommunityIcons name='web' size={25} color={colors.main} />
                                    </TouchableOpacity>
                                    {this.props?.navigation?.getParam('shareable') == true ? <TouchableOpacity
                                      onPress={() => {
                                        this.onSharePress(item);
                                      }}
                                      style={{ ...Styles.close, marginRight: 10 }}
                                    >
                                      <Image
                                        source={Images.share}
                                        style={{
                                          width: 23,
                                          height: 20,
                                          tintColor: "grey"
                                        }}
                                      />
                                    </TouchableOpacity> : null}
                                  </View>
                                  <Video
                                    ref={this.setRef}
                                    style={[
                                      Styles.video,
                                      { position: "absolute", top: 0 },
                                    ]}
                                    resizeMode={"contain"}
                                    source={{ uri: IMAGE_BASE_URL + item.job_slides }}
                                    useNativeControls
                                    isLooping
                                    onPlaybackStatusUpdate={(status) => {
                                      this.setState({ status: status });
                                    }}
                                  /></View> : null}
                              <View style={[GlobalStyles.mediumText, Styles.paginationStyle]}>
                                <Text style={[GlobalStyles.mediumText, Styles.paginationText]}>
                                  {index + 1}/{jobSlider.length}
                                </Text>
                              </View>
                              {/* <View style={[GlobalStyles.mediumText, Styles.paginationStyle]}>
                                <Text style={[GlobalStyles.mediumText, Styles.paginationText]}>
                                  {index + 1}/{jobSlider.length}
                                </Text>
                              </View> */}
                            </View>
                        }
                      </View>
                    )
                  }}
                /> : <View style={{ height: "90%", width: "100%", alignItems: "center", justifyContent: "center" }}><Text style={{ ...GlobalStyles.boldText, color: colors.main }}>No Data</Text></View>

          }
        </View>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps)(JobSlider);
