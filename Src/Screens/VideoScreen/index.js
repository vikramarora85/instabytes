import React, { Component, createRef } from 'react';
import { View ,Image} from 'react-native';
import Styles from './Styles';
import BackHeader from '../../Components/BackHeader';
import { Video } from 'expo-av';
import Carousel from 'react-native-banner-carousel';

class VideoScreen extends Component {
  constructor(props) {
    super(props);
    this.video = createRef(null)
    this.state = {
      selectedIndex: null,
      status: null,
      data:[
        {
          title: "Aenean leo",
          body: "Ut tincidunt tincidunt erat. Sed cursus turpis vitae tortor. Quisque malesuada placerat nisl. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.",
          imgUrl: "https://picsum.photos/id/11/200/300"
        },
        {
          title: "In turpis",
          body: "Aenean ut eros et nisl sagittis vestibulum. Donec posuere vulputate arcu. Proin faucibus arcu quis ante. Curabitur at lacus ac velit ornare lobortis. ",
          imgUrl: "https://picsum.photos/id/10/200/300"
        },
        {
          title: "Lorem Ipsum",
          body: "Phasellus ullamcorper ipsum rutrum nunc. Nullam quis ante. Etiam ultricies nisi vel augue. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc.",
          imgUrl: "https://picsum.photos/id/12/200/300"
        }
      ]
    };
  }
  // componentDidMount(){
  //   const userId = await AsyncStorage.getItem("userId");
  //   const categoryId1 = await AsyncStorage.getItem("categoryId")
  //   const categoryId=JSON.parse(categoryId1)
  // }
  handleIndexChange = (index) => {
    // console.log(index)
    this.setState({ selectedIndex: index })
  };
  render() {
    const { freeData, selectedIndex,data } = this.state
    return (
      <>
        <View style={Styles.safeViewStyle}>
          <BackHeader
            navigation={this.props.navigation}
            headerName={'Title of subcategory'}
            screen={'CategoryList'}
            nextShow={true}
          />
          <View style={Styles.container}>
            <Carousel>
            {
                  data.map((ele)=>{
                    console.log(ele.imgUrl)
                    return <Image source={{uri:ele.imgUrl}} style={Styles.video}/> 
                  })
                }
            <Video
              ref={this.video}
              style={Styles.video}
              resizeMode={'contain'}
              source={{
                uri: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
              }}
              useNativeControls
              isLooping
              onPlaybackStatusUpdate={(status) => { this.setState({ status: status }) }}
            />
            </Carousel>
          </View>
        </View>
      </>
    );
  }
}
export default VideoScreen;
