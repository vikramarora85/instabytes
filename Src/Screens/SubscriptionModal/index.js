import React, { Component, useRef } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Animated
} from 'react-native';
import Styles from './Styles';
import { startGetPlans } from '../../actions/userAction'
import { connect } from 'react-redux';
import Entypo from '@expo/vector-icons/Entypo'


class Subscription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      showgradient: false,
      isModalVisible: true,
    };
  }

  componentDidMount() {
    this.props.dispatch(startGetPlans());
    this.fadeAnim = new Animated.Value(0);
  }
  getStartedPress = () => {
    this.setState({ showgradient: !this.state.showgradient })
    this.setTime()
  }
  setTime = () => {
    setTimeout(() => {
      this.props.navigate('LearningCategories')
    }, 50);
  }
  valueChangeOne = () => {
    console.log('I ran')
    this.setState({ isModalVisible: !this.state.isModalVisible })
    this.fadeIn()
  }

  valueChangeTwo = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible })
  }

  valueChange1 = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible })
    this.props.navigate("SelectPayment")
  }

  fadeIn = () => {
    Animated.timing(this.fadeAnim, {
      toValue: 1,
      duration: 800,
      useNativeDriver: false
    }).start();
  };


  modalContent = () => {
    return (
      <Animated.View style={{ ...Styles.mainWrapper, opacity: this.fadeAnim }}>
        <View style={Styles.courseDescription}>
          <Entypo onPress={() => this.props.onCancel()} style={{ position: 'absolute', top: 10, right: 10 }} name='cross' color='black' size={25} />
          <Text style={Styles.headingText}>
            {'Bytes Subscription'}
          </Text>
          <Text style={Styles.headingdescription}>
            {'Upgrade to Premium Membership and increase your options'}
          </Text>
          <TouchableOpacity style={Styles.buyContainer} onPress={this.valueChange1}>
            <Text style={Styles.buttonText}>{'$99 Upgrade to Premium'}</Text>
          </TouchableOpacity>
        </View>
      </Animated.View>
    )
  }


  render() {
    console.log(this.props.user.subscriptionPlans)
    const { showgradient, isModalVisible } = this.state
    return (
      <>
        <View style={Styles.safeViewStyle}>
          {
            isModalVisible ?
              <View style={{ ...Styles.mainWrapper }}>

                <View style={Styles.courseDescription}>
                  <Text style={Styles.headingText}>
                    {'Subscription'}
                  </Text>
                  <Text style={Styles.headingdescription1}>
                    {'Please make a subscription to access this content'}
                  </Text>

                  <View style={{
                    flexDirection: 'row',
                    marginTop: 20, justifyContent: 'flex-end'
                  }}>

                    <TouchableOpacity style={Styles.cancelButton1}
                      onPress={() => this.props.onCancel()}
                    >
                      <Text style={Styles.buttonText1}>{'Cancel'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={Styles.cancelButton2}
                      onPress={this.valueChangeOne}
                    >
                      <Text style={Styles.buttonText2}>{'Subscribe'}</Text>
                    </TouchableOpacity>

                  </View>
                </View>
              </View>
              : this.modalContent()
          }
        </View>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user
  };
}
export default connect(mapStateToProps)(Subscription);