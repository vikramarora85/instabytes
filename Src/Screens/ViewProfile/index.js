import React, { Component } from "react";
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Image,
} from "react-native";
import Styles from "./Styles";
import Images from "../../Styles/Images";
import Colors from "../../Styles/Colors";
import * as Constants from "../../AllConstants";
import { LinearGradient } from "expo-linear-gradient";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { setProfile } from "../../actions/userAction";
import { connect } from 'react-redux'

class ViewProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showgradient: false,
      user:null
    };
  }
  async componentDidMount(){
    const user=await AsyncStorage.getItem('user')
    this.setState({user:JSON.parse(user)})
    console.log(user)

  }
  handleClickLogout = () => {
    // const Name=await AsyncStorage.getItem('name')
    AsyncStorage.clear();
    this.getStartedPress();
  };
  getStartedPress = () => {
    this.setState({ showgradient: !this.state.showgradient });
    this.setTime();
  };
  setTime = () => {
    setTimeout(() => {
      this.props.navigation.navigate("Login");
    }, 50);
  };

  render() {

    const { user } = this.props;
    const { showgradient } = this.state;
    console.log(user, 'kjkjkjkj')
    return (
      <>
        <SafeAreaView style={Styles.safeViewStyle}>
          <View style={Styles.mainWrapper}>
            <View style={Styles.headerWrapper}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("LearningCategories");
                }}
              >
                <Image source={Images.back} style={Styles.menuStyle} />
              </TouchableOpacity>

              <Text style={Styles.headerTitle}>{"Profile"}</Text>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("ProfileScreen");
                }}
              >
                <Text style={Styles.headerTitle}>{"Edit"}</Text>
              </TouchableOpacity>
            </View>

            <Image
              source={user?.profile_pic?.length > 0 ? {uri:user?.profile_pic} : Images.Avatar}
              style={{
                width: 110,
                height: 110,
                borderRadius: 110,
                alignSelf: "center",
                marginTop: 20,
              }}
            />
            <Text
              style={{
                fontSize: 20,
                fontWeight: "600",
                color: Colors.ok,
                textAlign: "center",
                marginTop: 10,
              }}
            >
              {user === null ? null :user.name}
            </Text>
            <View style={Styles.listMainWrapper}>
              {/* 1 */}
              <Text style={Styles.listText}>{"Email Id"}</Text>
              <Text style={Styles.listTextDescription}>
                {user === null ? null :user.email}
              </Text>
              {/* 2 */}
              <Text style={Styles.listText}>{"Phone"}</Text>
              <Text style={Styles.listTextDescription}>{user === null ? null :user.phone}</Text>
              {/* 3 */}
              <Text style={Styles.listText}>{"Gender"}</Text>
              <Text style={Styles.listTextDescription}>{user === null ? null :user.gender}</Text>
              {/* 4 */}
              <Text style={Styles.listText}>{"Country"}</Text>
              <Text style={Styles.listTextDescription}>{user === null ? null :user.country}</Text>
              {/* 5 */}
              <Text style={Styles.listText}>{"Professional Domain"}</Text>
              <Text style={Styles.listTextDescription}>{user === null ? null :user.professional_domain}</Text>
            </View>
          </View>
          <ScrollView></ScrollView>
          {showgradient ? (
            <TouchableOpacity
            // onPress={() => { this.props.navigation.navigate("LearningCategories") }}
            >
              <View
                style={Styles.buttonContainerGradient}
              >
                <Text style={Styles.buttonText}>{"Logout"}</Text>
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={Styles.buttonContainer}
              onPress={this.handleClickLogout}
            >
              <Text style={Styles.buttonText}>{"Logout"}</Text>
            </TouchableOpacity>
          )}
        </SafeAreaView>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user.profile,
  };
};

const mapDispatchToProps = (dispatch) => ({
  setProfile: (data) => dispatch(setProfile(data)),
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewProfile);
