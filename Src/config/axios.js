import Axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";

const axios = Axios.create({
  baseURL: "http://instabytes.in/api",
});

axios.interceptors.request.use(
  async (config) => {
    const token1 = await AsyncStorage.getItem("token");
    const token = JSON.parse(token1);
    // console.log(token)
    if (token) {
      config.headers.common = {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: "Bearer " + token,
      };
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default axios;
