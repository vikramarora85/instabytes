import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import DrawerNavigation from "./DrawerNavigation";
import SplashScreen from "../Screens/SplashScreen";
import BuyCourse from "../Screens/BuyCourse";
import GetStarted from "../Screens/GetStarted";
import Signup from "../Screens/Signup";
import Login from "../Screens/Login";
import ForgotPass from "../Screens/ForgotPass";
import VerificationScreen from "../Screens/VerificationScreen";
import JobSlides from "../Screens/jobSlides";
import ResourceSlides from "../Screens/ResourceSlides";
import LearningCategories from "../Screens/LearningCategories";
const {Navigator, Screen} = createStackNavigator();

const AuthStack = () => (
  <Navigator headerMode="none">
    <Screen name="GetStarted" component={GetStarted} />
  	<Screen name="Signup" component={Signup} />
    <Screen name="Login" component={Login} />
    <Screen name="Forgot" component={ForgotPass} />
  	<Screen name="Verify" component={VerificationScreen} />
<Screen name="DrawerNavigation" component={DrawerNavigation} />
   <Screen name="jobSlide" component={JobSlides} path = "job/:id" />
   <Screen name="resourceSlide" component={ResourceSlides} path="resource/:id" />
  </Navigator>
);

const AppNavigator = () => (
  <NavigationContainer>
    <AuthStack />
  </NavigationContainer>
);

export default AppNavigator;

