import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from '@react-navigation/native';
import LearningCategories from "../Screens/LearningCategories";
import CategoryList from "../Screens/CategoryList";
import FreeJobs from "../Screens/CategoryList/freeJobs";
import FreeResource from "../Screens/CategoryList/freeResource";
import PaidJobs from "../Screens/CategoryList/paidJobs";
import PaidResource from "../Screens/CategoryList/paidResource";
import VideoScreen from "../Screens/VideoScreen";
import JobSlider from "../Screens/VideoScreen/jobSlider";
import JobSliderFvrts from "../Screens/VideoScreenFvrts/jobSlider";
import JobSubCategory from "../Screens/SubCategory/jobSubCategory";
import ResourceSubCategory from "../Screens/SubCategory/resourceSubCategory";
import ResourceSlider from "../Screens/VideoScreen/resourceSlider";
import ResourceSliderFvrts from "../Screens/VideoScreenFvrts/resourceSlider";
import BuyCourse from "../Screens/BuyCourse";
import SelectPayment from "../Screens/SelectPayment";
import PaymentForm from "../Screens/PaymentForm";
import Gratitude from "../Screens/Gratitude";
import Settings from "../Screens/Settings";
import ViewProfile from "../Screens/ViewProfile";
import ProfileScreen from "../Screens/Profile";
import Notifications from "../Screens/Notifications";
import Favourites from "../Screens/Favourites";
import FavJobSubcategory from "../Screens/Favourites/favJobSubcategories";
import FavResourceSubcategory from '../Screens/Favourites/favResourceSubcategory';
import FavResourceSlide from "../Screens/Favourites/favResourceSlide";
import FavJobSlide from "../Screens/Favourites/favJobSlide";
import Subscription from "../Screens/Subscription";
import ContactUs from "../Screens/ContactUs";
import ChangePassword from "../Screens/ChangePassword";
import VideoScreenFvrts from "../Screens/VideoScreenFvrts";
import ForgotPass from "../Screens/ForgotPass";
import PurchasedCat from "../Screens/PurchasedCat";
import Stripe from "../Screens/Stripe";
import WebViewScreen from "../Screens/WebView";
import GetStarted from "../Screens/GetStarted";
// import HomeScreen from '../Screens/HomeScreen';
const {Navigator, Screen} = createStackNavigator();
const fade = ({ current }) => ({
  cardStyle: {
    opacity: current.progress,
  },
});


const HomeNavigation = () => (
  <Navigator headerMode="none" screenOptions={{
    headerShown: false
  }}>
     <Screen name="GetStarted" component={GetStarted} />
     <Screen name="LearningCategories" component={LearningCategories} options={{cardStyleInterpolator: fade}} />
     <Screen name="CategoryList" component={CategoryList} options={{cardStyleInterpolator: fade}} />
  	 <Screen name="FreeJobs" component={FreeJobs} options={{cardStyleInterpolator: fade}} />
     <Screen name="FreeResource" component={FreeResource} options={{cardStyleInterpolator: fade}} />
     <Screen name="PaidJobs" component={PaidJobs} options={{cardStyleInterpolator: fade}} />
     <Screen name="PaidResource" component={PaidResource} options={{cardStyleInterpolator: fade}} />
     <Screen name="VideoScreen" component={VideoScreen} options={{cardStyleInterpolator: fade}} />
     <Screen name="VideoScreenFvrts" component={VideoScreenFvrts} options={{cardStyleInterpolator: fade}} />

     <Screen name="JobSlider" component={JobSlider} options={{cardStyleInterpolator: fade}}  path = 'job/:data' />
     <Screen name="FvrtJobs" component={JobSliderFvrts} options={{cardStyleInterpolator: fade}} />
  	 <Screen name="ResourceSlider" component={ResourceSlider} options={{cardStyleInterpolator: fade}}  path = 'resource/:data' />
     <Screen name="FvrtRes" component={ResourceSliderFvrts} options={{cardStyleInterpolator: fade}} />
     <Screen name="JobSubCategory" component={JobSubCategory} options={{cardStyleInterpolator: fade}} />
     <Screen name="ResourceSubCategory" component={ResourceSubCategory} options={{cardStyleInterpolator: fade}} />
     <Screen name="BuyCourse" component={BuyCourse} options={{cardStyleInterpolator: fade}} />
     <Screen name="SelectPayment" component={SelectPayment} options={{cardStyleInterpolator: fade}} />


     <Screen name="PaymentForm" component={PaymentForm} options={{cardStyleInterpolator: fade}}  />
     <Screen name="Gratitude" component={Gratitude} options={{cardStyleInterpolator: fade}} />
  	 <Screen name="Settings" component={Settings} options={{cardStyleInterpolator: fade}} />
     <Screen name="ViewProfile" component={ViewProfile} options={{cardStyleInterpolator: fade}} />
     <Screen name="ProfileScreen" component={ProfileScreen} options={{cardStyleInterpolator: fade}}  path = "profile"  />
     <Screen name="Notifications" component={Notifications} options={{cardStyleInterpolator: fade}} />
     <Screen name="PurchasedCat" component={PurchasedCat} options={{cardStyleInterpolator: fade}} />
     <Screen name="Favourites" component={Favourites} options={{cardStyleInterpolator: fade}} />

     <Screen name="FavJobSubcategory" component={FavJobSubcategory} options={{cardStyleInterpolator: fade}}  />
     <Screen name="FavResourceSubcategory" component={FavResourceSubcategory} options={{cardStyleInterpolator: fade}} />
  	 <Screen name="FavResourceSlide" component={FavResourceSlide} options={{cardStyleInterpolator: fade}} />
     <Screen name="FavJobSlide" component={FavJobSlide} options={{cardStyleInterpolator: fade}} />
     <Screen name="Subscription" component={Subscription} options={{cardStyleInterpolator: fade}}  path = "profile"  />
     <Screen name="ContactUs" component={ContactUs} options={{cardStyleInterpolator: fade}} />
     <Screen name="ChangePassword" component={ChangePassword} options={{cardStyleInterpolator: fade}} />
     <Screen name="Stripe" component={Stripe} options={{cardStyleInterpolator: fade}} />
     <Screen name="WebViewScreen" component={WebViewScreen} options={{cardStyleInterpolator: fade}} />

  </Navigator>
);


export default HomeNavigation;
