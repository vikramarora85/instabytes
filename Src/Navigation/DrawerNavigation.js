import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import React from "react";
import Colors from "../Styles/Colors";
import Metrics from "../Styles/Metrices";
import DrawerContent from "../Components/DrawerContent";
import HomeNavigation from "./HomeNavigation";
import LearningCategories from "../Screens/LearningCategories";

const navigatorConfig = {
  drawerWidth: Metrics.screenWidth * 0.8,
  // drawerWidth: Metrics.screenWidth,
  initialRouteName: "main",
  contentComponent: ({ navigation }) => (
    <DrawerContent navigation={navigation} />
  ),
  contentOptions: {
    activeTintColor: Colors.White,
    inactiveTintColor: Colors.primary,
    // labelStyle: {  },
    indicatorStyle: { height: 0 },
    scrollEnabled: false,
  },
  drawerOpenRoute: "DrawerOpen",
  drawerCloseRoute: "DrawerClose",
  drawerToggleRoute: "DrawerToggle",
};
const Drawer = createDrawerNavigator();

const  DrawerNavigation  = () => (
  <Drawer.Navigator headerMode="none" screenOptions={{
    headerShown: false
  }}
   drawerContent={props => <DrawerContent {...props} />}>
    <Drawer.Screen name="main" component={HomeNavigation} />
  </Drawer.Navigator>
)
// const MainDrawerNavigator = createDrawerNavigator({
//    main: {
//      screen: HomeNavigation,
//      path:'drawer'
//    }
// },
//   navigatorConfig,
// );

export default DrawerNavigation;
