import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  header: {
    fontFamily: "DMSans_400Regular",
  },
  boldText: {
    fontFamily: "DMSans_700Bold",
  },
  mediumText: {
    fontFamily: "DMSans_500Medium",
  },
  lightText: {
    fontFamily: "DMSans_400Regular",
  },
  textBox: {
    fontFamily: "DMSans_400Regular",
  },
});

export default styles;
