import AsyncStorage from "@react-native-async-storage/async-storage";
import * as Notifications from 'expo-notifications';

export const updateExpoToken = async (jwt) =>{
    const token = (await Notifications.getExpoPushTokenAsync()).data;

      console.log(token)
      fetch('http://instabytes.in/api/auth/update-device-token',{
        method:"POST",
       headers:{
         accept:"application/json",
         "content-type":"application/json",
         "Authorization": `Bearer ${jwt}`
       },
       body:JSON.stringify({
         device_token:token
       })
     }).then((res)=> res.json())
     .then((json)=>{
       console.log(json)
     })
  }
