import axios from "../config/axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import * as Notifications from 'expo-notifications';
import Toast from "../Components/Toast";
import { updateExpoToken } from "../Utils/update_expo_token";

export const setFreeResoure = (user) => {
  return { type: "SET_FREE_RESOURCE", payload: user };
};

export const setFreeJob = (user) => {
  return { type: "SET_FREE_JOB", payload: user };
};

export const setPaidJob = (user) => {
  return { type: "SET_PAID_JOB", payload: user };
};

export const setPaidResource = (user) => {
  return { type: "SET_PAID_RESOURCE", payload: user };
};

export const setProfile = (user) => {
  return { type: "SET_PROFILE", payload: user}
}

export const setUser = (user) => {
  return { type: "SET_USER", payload: user };
};
export const setResourceSubCategory = (user) => {
  return { type: "SET_SUB_CATEGORY", payload: user };
};
export const setJobSubCategory = (user) => {
  return { type: "SET_JOB_SUB_CATEGORY", payload: user };
};
export const setSubscriptionPlans = (user) => {
  return { type: "SET_SUBSCRIPTION_PLANS", payload: user };
};
export const setResourceSlider = (user) => {
  return { type: "SET_RESOURCE_SLIDER", payload: user };
};
export const setJobSlider = (user) => {
  return { type: "SET_JOB_SLIDER", payload: user };
};
export const setFavResource = (user) => {
  return { type: "SET_FAV_RESOURCE", payload: user };
};
export const setFavJobs = (user) => {
  return { type: "SET_FAV_JOBS", payload: user };
};
export const setFavJobSubcategory = (user) => {
  return { type: "SET_FAV_JOB_SUB_CAT", payload: user };
};
export const setFavResourceSubcategory = (user) => {
  return { type: "SET_FAV_RES_SUB_CAT", payload: user };
};

// Register user
export const startRegisterUser = (formData, redirect, onError) => {
  return (dispatch) => {
    axios
      .post("/auth/register", formData)
      .then((response) => {
        if (response.data.status == '0') {
          Toast(response.data.message)
          onError()
          return;
        }
          redirect();
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

// Login user
export const startLoginUser =  (formData, redirect, onError) => {
  return (dispatch) => {
    axios
      .post("/auth/login", formData)
      .then(async (response) => {
        if (response.data.status == '0') {
          Toast(response.data.message)
          onError()
          return;
        }
        axios.defaults.headers.Authorization = `Bearer ${response.data.data.api_token}`;
        await AsyncStorage.setItem(
          "token",
          JSON.stringify(response.data.data.api_token)
        );
        await AsyncStorage.setItem("userId", JSON.stringify(response.data.data.id));
        await AsyncStorage.setItem("user", JSON.stringify(response.data.data));
        Toast("you have logged in succesfully");
        dispatch(setUser(response.data.data));
        updateExpoToken(response.data.data.api_token)
        redirect();
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

// const updateExpoToken = async (jwt) =>{
//   const token = (await Notifications.getExpoPushTokenAsync()).data;

//     console.log(token)
//     fetch('http://instabytes.in/api/auth/update-device-token',{
//       method:"POST",
//      headers:{
//        accept:"application/json",
//        "content-type":"application/json",
//        "Authorization": `Bearer ${jwt}`
//      },
//      body:JSON.stringify({
//        device_token:token
//      })
//    }).then((res)=> res.json())
//    .then((json)=>{
//      console.log(json)
//    })
// }

// Getting all categories
export const startFetchData = () => {
  return async (dispatch) => {
    try {
      const freeLearningData = await axios.get("/category-free");
      dispatch(setFreeResoure(freeLearningData.data.data));

      const freeJobData = await axios.get("/job-free");
      dispatch(setFreeJob(freeJobData.data.data));

      const premiumLearningData = await axios.get("/category-paid");
      dispatch(setPaidResource(premiumLearningData.data.data));

      const premiumJobData = await axios.get("/job-paid");
      dispatch(setPaidJob(premiumJobData.data.data));
    } catch (error) {
      console.log(error);
    }
  };
};



// adding free resource categories to favs
export const startAddFavCategory = (formData) => {
  console.log(formData)
  return (dispatch) => {
    axios
      .post("/addCategoryFav", formData)
      .then((response) => {
        if (response.data.status === "1") {
          Toast(response.data.message)

        } else {
          Toast(response.data.message)
        }

      })
      .catch((err) => {
        console.log(err);
      });
  };
};


// removing fav free resource categories from favs
export const startRemoveFavCategory = (categoryId) => {
  return (dispatch) => {
    axios
      .post("/removeFavouriteCategory", categoryId)
      .then((response) => {

        if (response.data.status === "1") {
          Toast(response.data.message)
        } else {
          Toast(response.data.message)
        }

      })
      .catch((err) => {
        console.log(err);
      });
  };
};


// adding free job categories to favs
export const startAddFavJobCategory = (formData) => {
  console.log(formData)
  return (dispatch) => {
    axios
      .post("/addjobCategory", formData)
      .then((response) => {
        if (response.data.status === 1) {
          Toast(response.data.message)
        } else {
          Toast(response.data.message)
        }

      })
      .catch((err) => {
        console.log(err);
      });
  };
};
// removing  fav free job categories from favs
export const startRemoveFavJobCategory = (categoryId) => {
  return (dispatch) => {
    console.log(categoryId)
    axios
      .post("/removeFavouriteJobCategory", categoryId)
      .then((response) => {
        console.log(response)
        if (response.data.status === 1) {
          Toast(response.data.message)
        } else {
          Toast(response.data.message)
        }

      })
      .catch((err) => {
        console.log(err);
      });
  };
};




// Adding subcategory favorite
export const startAddFavSubCategory = (formData) => {
  return (dispatch) => {
    axios
      .post("/addsubCategoryFav", formData)
      .then((response) => {
        if (response.data.status) {
          Toast(response.data.message);
        } else {
          Toast(response.data.message);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

//removing fav subcategory from favs
export const startDeleteFavSubcategory = (formData) => {
  return (dispatch) => {
    axios
      .post("/removeFavouriteSubCategory", formData)
      .then((response) => {
        if (response.data.status) {
          Toast(response.data.message);
        } else {
          Toast(response.data.message);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
};


// adding fav job sub category
export const startAddFavJobSubCategory = (formData) => {
  return (dispatch) => {
    axios
      .post("/addjobsubCategory", formData)
      .then((response) => {
        console.log(response.data)
        if (response.data.status === 1) {
          Toast(response.data.message);
        } else {
          Toast(response.data.message);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

//removing fav job sub category from favs
export const startDeleteJobSubFav = (jobSubcategoryId) => {
  return (dispatch) => {
    axios
      .post("/removeFavouriteJobSubCategory", jobSubcategoryId)
      .then((response) => {
        console.log(response.data)
        if (response.data.status === 1) {
          Toast(response.data.message);
        } else {
          Toast(response.data.message);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

//adding fav sliders
export const startAddFavSlider = (formData) => {
  return async (dispatch) => {
    try {
      const favSlider = await axios.post("/addsubcategorySlideFav", formData);
      console.log(favSlider);
      if (favSlider.data.status === 1) {
        Toast(favSlider.data.message);
      } else {
        Toast(favSlider.data.message);
      }
    } catch (e) {
      console.log(e);
    }
  };
};

// remove fav from resouce slider
export const startDeleteFavSlider = (formData) => {
  return async (dispatch) => {
    try {
      const favSliderDel = await axios.post("/removeFavouriteSubSlide", formData);
      console.log(favSliderDel);
      if (favSliderDel.data.status === 1) {
        Toast(favSliderDel.data.message);
      } else {
        Toast(favSliderDel.data.message);
      }
    } catch (e) {
      console.log(e);
    }
  };
};

//adding fav job slider
export const startAddFavJobSlider = (formData) => {
  return async (dispatch) => {
    try {
      const favSlider = await axios.post("/addjobslide", formData);
      if (favSlider.data.status === 1) {
        Toast(favSlider.data.message);
      } else {
        Toast(favSlider.data.message);
      }
    } catch (e) {
      console.log(e.response.data);
    }
  };
};

// remove fav from job slider
export const startDeleteFavJobSlider = (formData) => {
  return async (dispatch) => {
    try {
      const favSliderDel = await axios.post("/removeFavouriteJobSlide", formData);
      console.log(favSliderDel);
      if (favSliderDel.data.status === 1) {
        Toast(favSliderDel.data.message);
      } else {
        Toast(favSliderDel.data.message);
      }
    } catch (e) {
      console.log(e);
    }
  };
};

// Sub categories
export const startSubCategory = (id) => {
  return async (dispatch) => {
    try {
      console.log(id)
      // const categoryId1 = await AsyncStorage.getItem("categoryId");
      // const categoryId = JSON.parse(categoryId1);
      const category = await axios.get("/subcategory", {
        params: {
          category_id: id,
        },
      });
      dispatch(setResourceSubCategory(category.data.data));
      // const jobSubCategory = await axios.get(`/jobsubcategory/${categoryId}`);
      // dispatch(setJobSubCategory(jobSubCategory.data.data));
    } catch (err) {
      console.log(err);
    }
  };
};

export const startJobSubCategory = (id) => {
  return async (dispatch) => {
    try {
      const jobSubCategory = await axios.get(`/jobsubcategory/${id}`);
      dispatch(setJobSubCategory(jobSubCategory.data.data));
    } catch (err) {
      console.log(err);
    }
  };
};
// changing password

export const startChangePassword = (formData, redirect, onError) => {
  return async () => {
    try {
      const changePass = await axios.post("/auth/change-password", formData);

      if(changePass.data.status == "1"){
        Toast("Success")
        redirect()
      } else {
        onError()
        Toast(changePass.data.message)
      }
    } catch (err) {
      console.log(err);
    }
  };
};

// Slider
export const startGetSlide = (id) => {
  return async (dispatch) => {
    try {
      // const subCategoryId1 = await AsyncStorage.getItem("subCategoryId");
      // const subCategoryId = JSON.parse(subCategoryId1);
      const resourceSlide = await axios.get(
        `/subcategorySlide/${id}`
      );
      dispatch(setResourceSlider(resourceSlide.data.data));

      const jobSlide = await axios.get(`/jobsubcategorySlide/${id}`);
      dispatch(setJobSlider(jobSlide.data.data));
      console.log(jobSlide.data)
    } catch (err) {
      console.log(err);
    }
  };
};

// Subscription plans

export const startGetPlans = () => {
  return async (dispatch) => {
    try {
      const plans = await axios.get("/plans");
      dispatch(setSubscriptionPlans(plans.data.data));
    } catch (err) {
      console.log(err);
    }
  };
};

// getting fav category list
export const startGetFav = () => {
  return async (dispatch) => {
    try {
      const resourceFav = await axios.get("/favourite-list");
      dispatch(setFavResource(resourceFav.data.data));
      const jobFav = await axios.get("/jobfavourite-list");
      console.log(jobFav.data)

      if (jobFav.data.status === "1") {
        dispatch(setFavJobs(jobFav.data.data));
      } else {
        Toast(jobFav.data.message);
      }
    } catch (err) {
      console.log(err);
    }
  };
};

// getting fav Job Sub category list

export const startGetFavSubcategory = (jobId) => {
  return async (dispatch) => {
    try {
      const jobSubCategory = await axios.get("/favourite-subjoblist", {
        params: {
          job_id: jobId,
        },
      });

      if (jobSubCategory.data.message === "Success") {
        dispatch(setFavJobSubcategory(jobSubCategory.data.data));
      } else {
        dispatch(setFavJobSubcategory([]));
        Toast(jobSubCategory.data.message);
      }
    } catch (err) {
      console.log(err);
    }
  };
};

// fav resource sub cat
export const startGetFavResuorceSubcategory = (catId) => {
  return async (dispatch) => {
    try {
      const resourceSubCategory = await axios.get("/favourite-sublist", {
        params: {
          category_id: catId,
        },
      });

      console.log(resourceSubCategory.data, 'kkll')

      if (resourceSubCategory.data.status == "1") {
        dispatch(setFavResourceSubcategory(resourceSubCategory.data.data));
      } else {
        dispatch(setFavResourceSubcategory([]));
        Toast(resourceSubCategory.data.message);
      }
    } catch (err) {
      console.log(err);
    }
  };
};

//fav resource slider
export const startGetFavResuorceSlider = (id) => {
  return async () => {
    try {
      const favResourceSlider = await axios.get("/favourite-slidelist", {
        params: {
          subcategory_id: id,
        },
      });
      console.log(favResourceSlider);
    } catch (e) {
      console.log(e);
    }
  };
};
//fav job slider
export const startGetFavJobSlider = () => {
  return async () => {
    try {
      const favJobSlider = await axios.get("/favourite-slidejoblist", {
        params: {
          job_subcategory_id: 2,
        },
      });
      console.log(favJobSlider);
    } catch (e) {
      console.log(e);
    }
  };
};

// // deleting category from fav list
// export const startDeleteFav = (subcategoryId, jobSubcategoryId) => {
//   return async (dispatch) => {
//     try {
//       const favId = await AsyncStorage.getItem("FavouriteId");
//       const favoriteId = JSON.parse(favId);

//       const resourceDeleteFav = await axios.get(`/removefav/${favoriteId}`);

//       // dispatch(setFavResource(resourceFav.data.data))

//       const jobDeleteFav = await axios.get(`/removeJobfav/${favoriteId}`);
//       // dispatch(setFavJobs(jobFav.data.data))

//       const resourceSubcatDelete = await axios.get(`/remove-subcatgoryfav/${subcategoryId}`);
//       console.log(resourceSubcatDelete)

//       const jobSubcatDelete = await axios.get(`/remove-subcatgoryjobfav/${jobSubcategoryId}`);
//       console.log(jobSubcatDelete.data,jobSubcategoryId)

//     } catch (err) {
//       console.log(err);
//     }
//   };
// };

// //delete job subcategory
// export const startDeleteJobSubFav = (jobSubcategoryId) => {
//   return async (dispatch) => {
//     try {

//       const jobSubcatDelete = await axios.get(`/remove-subcatgoryjobfav/${jobSubcategoryId}`);
//       console.log(jobSubcatDelete.data,jobSubcategoryId)

//     } catch (err) {
//       console.log(err);
//     }
//   };
// };

// update profile
export const startUpdateProfile = () => {
  return async () => {
    try {
      const profile = await axios.post("");
    } catch (err) {
      console.log(err);
    }
  };
};
