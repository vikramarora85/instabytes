import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import Styles from "./Styles";
import Images from "../../Styles/Images";
import GlobalStyles from "../../Styles/globalStyles";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

class BackHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTrue: false,
      search: "",
    };
  }
  searchValue = () => {
    this.setState({ searchTrue: !this.state.searchTrue });
  };

  favValue = () => { };

  render() {
    const { headerName, screen, nextShow, deleteShow, deletePress } = this.props;
    return (
      <>
        <View
          style={Styles.headerWrapper}
        >
          {
            <View style={Styles.mainWrapper}>
              <View style={Styles.innerWrapper}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate(screen);
                  }}
                >
                  <Image source={Images.back} style={Styles.menuStyle} />
                </TouchableOpacity>
                <Text style={[GlobalStyles.boldText, Styles.headerTitle]}>
                  {headerName}
                </Text>
              </View>
              {nextShow ? (
                <View style={Styles.innerWrapper}>
                  <TouchableOpacity onPress={() => this.props.onSharePress()}>
                    <Image
                      source={Images.share}
                      style={[Styles.menuStyle, { marginLeft: 15 }]}
                    />
                  </TouchableOpacity>
                </View>
              ) : deleteShow ? <View style={Styles.innerWrapper}>
                <TouchableOpacity onPress={() => deletePress()}>
                  <MaterialCommunityIcons size={30} color='white' name={'delete-outline'}/>
                </TouchableOpacity>
              </View> : null}
            </View>
          }
        </View>
      </>
    );
  }
}
export default BackHeader;
