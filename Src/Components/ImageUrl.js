import React from "react";
import { Image, ImageBackground, View } from "react-native";

export const BaseImage = ({ path, style }) => {
  let baseImageUrl = `http://instabytes.in/uploads${path}`;
  return (
    <View>
      <Image source={{ uri: baseImageUrl }} style={style} />
    </View>
  );
};

export const BaseImageBackground = ({ path, style, ...props}) => {
  let baseImageUrl = `http://instabytes.in/uploads${path}`;
  return (
    <ImageBackground
      imageStyle={{ borderRadius: 15 }}
      source={{ uri: baseImageUrl }}
      style={style}
    >{props.children}</ImageBackground>
  );
};
