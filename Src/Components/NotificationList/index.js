import React, { Component } from 'react';
import {
  View,
  Text,
  Linking,
  Platform
} from 'react-native';
import Styles from './Styles';
import GlobalStyles from '../../Styles/globalStyles';
import { TouchableOpacity } from 'react-native';

class NotificationList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTrue: false,
      search: ''
    };
  }
  searchValue = () => {
    this.setState({ searchTrue: !this.state.searchTrue })
  }

  navigateSlide = (val) => {
    console.log(val)
    if (Platform.OS != 'android') {
      if (val.includes('job')) {

        const txt = val.slice(0, 13)
        this.props.navigation.navigate('jobSlide', { id: txt })
      } else {

        const txt = val.slice(0, 17)
        this.props.navigation.navigate('resourceSlide', { id: txt })
      }
    } else {
      Linking.openURL(val)
    }
  }

  render() {
    const { time, title, description, action } = this.props

    return (
      <>
        <TouchableOpacity onPress={() => this.navigateSlide(action)} style={Styles.headerWrapperSelected}>
          <View style={Styles.headerContainer}>
            <Text style={[GlobalStyles.mediumText, Styles.textHeader]}>{title}</Text>
            <Text style={[GlobalStyles.lightText, Styles.textHeaderTime]}>{time}</Text>
          </View>
          <View style={Styles.bottomWrapper}>
            <Text numberOfLines={2} style={GlobalStyles.lightText}>{description}</Text>
          </View>
        </TouchableOpacity>
      </>
    );
  }
}
export default NotificationList;
