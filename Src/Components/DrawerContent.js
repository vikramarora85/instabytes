import React, { Component } from "react";
import {
  Image,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
  Platform,
  Linking
} from "react-native";
import Images from '../Styles/Images';
import Colors from '../Styles/Colors';
import Metrics from '../Styles/Metrices';
import GlobalStyles from '../Styles/globalStyles';
import { connect } from 'react-redux'
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "../config/axios";
import * as Constants from "../AllConstants";

class DrawerContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      switchValue: true,
      user: null
    };
    this.titles =
      [
        {
          name: "Favourite",
          image: Images.favouriteDrawer
        },
        {
          name: "Subscription",
          image: Images.subscribe
        },
        {
          name: "My Resources",
          image: Images.bag_shop
        },
        {
          name: "Notification",
          image: Images.drawerNotification
        },
        {
          name: "Change Password",
          image: Images.lock
        },
        {
          name: 'Privacy Policy',
          image: Images.settingSheild
        },
        {
          name: 'Terms and Conditions',
          image: Images.settingTerm
        },
        {
          name: "Help",
          image: Images.help
        },
        {
          name: "Logout",
          image: Images.lock
        },
      ];
    this.navigates =
      [
        "Favourites",
        "Subscription",
        "PurchasedCat",
        "Notifications",
        "ChangePassword",
        "ContactUs",
        "ContactUs",
        "ContactUs",
        "Login"

      ]
  }
  async componentDidMount() {
    const user = await AsyncStorage.getItem('user')
    this.setState({ user: JSON.parse(user) })
    // console.log(user,'12')

  }
  handleClickLogout = async () => {
    // const Name=await AsyncStorage.getItem('name')
    try {
      await AsyncStorage.clear();
      this.props.navigation.navigate("Login")
      await AsyncStorage.setItem('getStarted', 'done')
      this.setTime();
    } catch (e) {
      console.log(e)
    }
  };
  setTime = () => {
    setTimeout(() => {
      this.props.navigation.navigate("Login");
    }, 50);
  };

  _onPress = async (value, index) => {
    // this.props.navigation.navigate(this.navigates[index]);
    if (index === 8) {
      this.handleClickLogout()
    } else if (index === 5) {
      Linking.openURL('https://instabyte.microntec.in/privacy.html')
    } else if (index === 6) {
      Linking.openURL('https://instabyte.microntec.in/terms-conditions.html')
    } else {
      this.props.navigation.navigate(this.navigates[index])
    }
    this.props.navigation.closeDrawer();
  };


  _renderItem = (value, index) => {
    return (
      <View key={index} style={{ height: 50, width: '95%', alignSelf: 'center' }}>
        {/* {index !== 0 && this._renderSeparator()} */}
        <TouchableOpacity
          onPress={() => {
            this._onPress(value, index);
          }}
          style={styles.menu}
        >
          <View style={{ flexDirection: 'row' }}>
            <Image source={value.image} style={{
              width: 22,
              height: 22,
            }} />
            <Text style={[GlobalStyles.lightText, { marginLeft: 10, fontSize: 16, color: Colors.ok }]}>{value.name}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const { user } = this.props;
    return (

      <SafeAreaView style={{ flex: 1, backgroundColor: Colors.White }}>
        <View style={styles.container}>

          <View
            style={{
              backgroundColor: Colors.White,
              width: "100%",
              paddingVertical: 20,
              paddingHorizontal: Metrics.padding,
              alignItems: "center"
            }}
          >
            <View style={{ flexDirection: 'row', width: "95%", alignSelf: 'center', justifyContent: 'space-between' }}>

              <View style={{ flexDirection: 'row', }}>
                <Image source={user?.profile_pic?.length != 0 ? { uri: user?.profile_pic } : Images.Avatar} style={{
                  width: 70,
                  height: 70,
                  borderRadius: 100,
                }} />

                <View style={{ marginLeft: 15 }}>
                  <Text style={[GlobalStyles.boldText, {
                    fontSize: 20, color: Colors.ok, marginTop: 10,
                  }]}>
                    {user === null ? null : user.name}
                  </Text>
                  <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate('ViewProfile') }}>
                    <Text style={[GlobalStyles.lightText, {
                      fontSize: 14, color: Colors.appHeaderColor, marginTop: 5
                    }]}>
                      {'View Profile'}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            {/* 2nd */}
          </View>
          {
            <View style={[styles.container1, { paddingTop: 2 }]}>
              {this.titles.map(this._renderItem)}
            </View>
          }
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: Platform.OS === 'ios' ? 20 : 40,
    flex: 0.9,
    backgroundColor: "white"
  },
  container1: {
    marginTop: 5,
    flex: 0.9,
    backgroundColor: "white"
  },
  menu: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 20,
    alignItems: "center"
  },
  text: {
    fontSize: 20,
    fontWeight: "700",
    color: "black"
  }
});

const mapStateToProps = (state) => {
  return {
    user: state.user.profile
  }
}


export default connect(mapStateToProps)(DrawerContent);
