import React, { Component } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Image,
  TextInput
} from 'react-native';
import Styles from './Styles';
import Images from '../../Styles/Images';
import GlobalStyles from '../../Styles/globalStyles';

class HomeHeaderList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTrue: false,
      search: ''
    };
  }
  searchValue = () => {
    this.setState({ searchTrue: !this.state.searchTrue })
  }

  render() {
    const { headerName,headerName1 } = this.props
    const { searchTrue, search } = this.state
    return (
      <>
        <View
          style={Styles.headerWrapper}
        >
          <View style={Styles.mainWrapper}>
            <View>
              {/* <TouchableOpacity
                onPress={() => {
                  this.props.navigation.toggleDrawer();
                }}
              >
                <Image source={Images.menu} style={Styles.menuStyle} />
              </TouchableOpacity> */}
              <Text style={[GlobalStyles.boldText, Styles.headerTitle]}>{headerName}</Text>
              <Text style={[GlobalStyles.boldText, Styles.headerTitle1]}>{headerName1}</Text>
            </View>

            <View style={Styles.innerWrapper}>
              <TouchableOpacity>
                <Image source={Images.search} style={Styles.menuStyle} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => { this.props.navigation.navigate('Notifications') }}>
                <Image source={Images.notification} style={[Styles.menuStyle, { marginLeft: 15 }]} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </>
    );
  }
}
export default HomeHeaderList;
