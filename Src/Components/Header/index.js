import React, { Component } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Image,
  TextInput
} from 'react-native';
import Styles from './Styles';
import Images from '../../Styles/Images';
import Colors from '../../Styles/Colors';
import GlobalStyles from '../../Styles/globalStyles';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTrue: false,
      search: ''
    };
  }
  searchValue = () => {
    this.setState({ searchTrue: !this.state.searchTrue })
  }

  handleBtn = () => {
    if (this.props.isBackBtn) {
      this.props.navigation.navigate(this.props.screen)
    } else {
      this.props.navigation.toggleDrawer()
    }
  }

  searchToggle = () => {
    this.setState({ searchTrue: !this.state.searchTrue })
  }

  handleShare = () => {

  }

  render() {
    const { headerName, isBackBtn, isSearch, isShare, notificationHidden } = this.props
    const { searchTrue, search } = this.state
    return (
      <View style={Styles.headerWrapper}>
        {
          searchTrue ?
            <View style={Styles.mainWrapper1}>
              <View style={Styles.innerWrapper}>
                <TouchableOpacity
                  onPress={() => {
                    this.handleBtn()
                  }}
                >
                  {isBackBtn ? <Image source={Images.back} style={Styles.menuStyle} /> : <Image source={Images.menu} style={Styles.menuStyle} />}
                </TouchableOpacity>
                {/* Search */}
                <View style={Styles.emailWrapper}>
                  <TextInput
                    style={[GlobalStyles.lightText, Styles.emailInput]}
                    value={search}
                    autoFocus={true}
                    placeholder={'Search'}
                    placeholderTextColor={Colors.textInputColor}
                    autoCapitalize='none'
                    onChangeText={(value) => {
                      this.setState({ search: value })
                      this.props.onSearch(value)
                    }}
                  />
                  <TouchableOpacity onPress={this.searchValue}>
                    <Image source={Images.search} style={[Styles.inputImageLock, {
                      tintColor: Colors.appHeaderColor
                    }]} />
                  </TouchableOpacity>
                </View>
                {isShare ? <TouchableOpacity onPress={() => { handleShare() }}>
                  <Image source={Images.share} style={[Styles.menuStyle, { marginLeft: 15 }]} />
                </TouchableOpacity> : <TouchableOpacity onPress={() => { this.props.navigation.navigate('Notifications') }}>
                  <Image source={Images.notification} style={[Styles.menuStyle, { marginLeft: 15 }]} />
                </TouchableOpacity>}
                {/* End */}
              </View>
            </View>
            :
            <View style={Styles.mainWrapper}>
              <View style={Styles.innerWrapper}>
                <TouchableOpacity
                  onPress={() => {
                    this.handleBtn()
                  }}
                >
                  {isBackBtn ? <Image source={Images.back} style={Styles.menuStyle} /> : <Image source={Images.menu} style={Styles.menuStyle} />}
                </TouchableOpacity>
                <Text style={[GlobalStyles.boldText, Styles.headerTitle]}>{headerName}</Text>
              </View>

              <View style={Styles.innerWrapper}>
                {isSearch ? <TouchableOpacity onPress={this.searchToggle}>
                  <Image source={Images.search} style={Styles.menuStyle} />
                </TouchableOpacity> : null}
                {isShare ? <TouchableOpacity onPress={() => { handleShare() }}>
                  <Image source={Images.share} style={[Styles.menuStyle, { marginLeft: 15 }]} />
                </TouchableOpacity> : !notificationHidden? <TouchableOpacity onPress={() => { this.props.navigation.navigate('Notifications') }}>
                  <Image source={Images.notification} style={[Styles.menuStyle, { marginLeft: 15 }]} />
                </TouchableOpacity> : null }
              </View>
            </View>
        }
      </View>
    );
  }
}
export default Header;
