const userInitialState = {
  freeResource: [],
  jobs: [],
  paidResource: [],
  paidjobs: [],
  userData: [],
  subCategory: [],
  subJobCategory: [],
  subscriptionPlans: [],
  resourceSlider: [],
  jobSlider: [],
  favResource: [],
  favJobs: [],
  favJobSubcat: [],
  favResSubcat: [],
  profile: {}
};
const userReducer = (state = userInitialState, action) => {
  switch (action.type) {
    case "SET_FREE_RESOURCE": {
      state.freeResource = action.payload;
      return { ...state };
    }
    case "SET_USER": {
      state.userData = action.payload;
      return { ...state };
    }
    case "SET_FREE_JOB": {
      state.jobs = action.payload;
      return { ...state };
    }
    case "SET_PAID_JOB": {
      state.paidjobs = action.payload;
      return { ...state };
    }
    case "SET_PAID_RESOURCE": {
      state.paidResource = action.payload;
      return { ...state };
    }
    case "SET_SUB_CATEGORY": {
      state.subCategory = action.payload;
      return { ...state };
    }
    case "SET_JOB_SUB_CATEGORY": {
      state.subJobCategory = action.payload;
      return { ...state };
    }
    case "SET_SUBSCRIPTION_PLANS": {
      state.subscriptionPlans = action.payload;
      return { ...state };
    }
    case "SET_RESOURCE_SLIDER": {
      state.resourceSlider = action.payload;
      return { ...state };
    }
    case "SET_JOB_SLIDER": {
      state.jobSlider = action.payload;
      return { ...state };
    }
    case "SET_FAV_RESOURCE": {
      state.favResource = action.payload;
      return { ...state };
    }
    case "SET_FAV_JOBS": {
      state.favJobs = action.payload;
      return { ...state };
    }
    case "SET_FAV_JOB_SUB_CAT": {
      state.favJobSubcat = action.payload;
      return { ...state };
    }
    case 'SET_FAV_RES_SUB_CAT': {
      state.favResSubcat = action.payload
      return { ...state }
    }
    case 'SET_PROFILE': {
      state.profile = action.payload
      return { ...state }
    }

    default: {
      return { ...state };
    }
  }
};
export default userReducer;
