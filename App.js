import React, { useState, useEffect, useRef } from "react";
import Root from "./Src/setup";
import Constants from 'expo-constants';
import {
  useFonts,
  DMSans_400Regular,
  DMSans_500Medium,
  DMSans_500Medium_Italic,
  DMSans_700Bold,
} from "@expo-google-fonts/dm-sans";
import { DMSerifDisplay_400Regular } from "@expo-google-fonts/dm-serif-display";
import AppLoadingPlaceholder from "expo/build/launch/AppLoadingPlaceholder";
import * as Notifications from 'expo-notifications';
import { Provider } from "react-redux";
import configureStore from "./Src/store/configureStore";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Toast from "./Src/Components/Toast";
import { StripeProvider } from '@stripe/stripe-react-native';
import * as Updates from 'expo-updates';
import { StatusBar } from "expo-status-bar";
import { Linking } from 'react-native'

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

const store = configureStore();
store.subscribe(() => {
  // console.log(store.getState(),'store')
  return store.getState();
});

const App = () => {

  const notificationListener = useRef();
  const responseListener = useRef();


  useEffect(() => {
    registerForPushNotificationsAsync().then(token => {
      setToken(token)
    });
    // This listener is fired whenever a notification is received while the app is foregrounded
    notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
      console.log(notification);
    });

    // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
      console.log(response);
    });

    return () => {
      Notifications.removeNotificationSubscription(notificationListener.current);
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, []);

  useEffect(() => {
    const subscription = Notifications.addNotificationResponseReceivedListener(response => {
      const link = response.notification.request.content.data.link;
      Linking.openURL(link);
      set_state()
    });
    return () => subscription.remove();
  }, []);

  const set_state = async () => {
     await AsyncStorage.setItem('is_linking', 'yes')
  }
  
  const lastNotificationResponse = Notifications.useLastNotificationResponse();
  useEffect(() => {
    if (lastNotificationResponse && lastNotificationResponse.actionIdentifier === Notifications.DEFAULT_ACTION_IDENTIFIER) {
      handlePush(lastNotificationResponse);
    }
  }, [lastNotificationResponse]);

  const handlePush = async ({ notification }) => {
    const link = notification.request.content.data.link;
    set_state()
    Linking.openURL(link);
  }

  useEffect(() => {
    checkUpdates()
  }, [])

  const checkUpdates = async () => {
    try {
      const update = await Updates.checkForUpdateAsync();
      if (update.isAvailable) {
        await Updates.fetchUpdateAsync();
        alert('New Update Available')
        await Updates.reloadAsync();
      }
    } catch (e) {
      console.log(e)
    }

  }


  const setToken = async (token) => {
    console.log(token, 'token')
    const newToken = JSON.stringify(token)
    try {
      await AsyncStorage.setItem('expo-token', newToken)
    } catch (e) {
      console.log(e)
    }
  }


  const [fontsLoaded] = useFonts({
    DMSans_400Regular,
    DMSans_500Medium,
    DMSans_500Medium_Italic,
    DMSans_700Bold,
    DMSerifDisplay_400Regular
  });

  if (!fontsLoaded) {
    return <AppLoadingPlaceholder />;
  }
  return (
    <>
      <Provider store={store}>
        <StatusBar style='light' />
        <StripeProvider
          publishableKey={'pk_test_51JKG7ESE9glFk3LnKobfUebmhzePY4IAjIKQjorEYKv1YozRfLK4UC8zvjhetKoE2TDWTVXW66rsMaynNkBQfOch00RVGcaLxe'}
          merchantIdentifier="merchant.identifier"
        >
          <Root />
        </StripeProvider>

      </Provider>
    </>
  );
};
export default App;


async function registerForPushNotificationsAsync() {
  let token;
  if (Constants.isDevice) {
    const { status: existingStatus } = await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      Toast('Failed to get push token for push notification!');
      return;
    }
    token = (await Notifications.getExpoPushTokenAsync()).data;
  } else {
    console.log('Must use physical device for Push Notifications');
  }

  if (Platform.OS === 'android') {
    Notifications.setNotificationChannelAsync('default', {
      name: 'default',
      importance: Notifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: '#FF231F7C',
    });
  }

  return token;
}
